<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Status;

class CategoryController extends Controller
{
    /**
     * Display all categories.
     */
    public function index()
    {
        $categories = Category::with('subCategories')->where('parent_id', 0)->get();
        // return $categories;

        return view('admin.category.index', compact('categories'));
    }
    
    /**
     * Edit the category.
     */
    public function add()
    {   
        $parentCategories = Category::where('parent_id', 0)->get();
        $statuses = Status::all();

        return view('admin.category.add', compact('parentCategories', 'statuses'));
    }

    /**
     * Edit category, once form is submitted.
     */
    public function addSubmit(Request $request)
    {
        $data = $request->all();
        $category = new Category();
        $category->addCategory($data);
        return back()->with('fm_success', 'Category has been added.');
    }

    /**
     * Edit the category.
     */
    public function edit($categoryId = 0)
    {   
        $category = Category::findOrFail($categoryId);
        $parentCategories = Category::where('parent_id', 0)->get();
        $statuses = Status::all();

        return view('admin.category.edit', compact('category', 'parentCategories', 'statuses'));
    }

    /**
     * Edit category, once form is submitted.
     */
    public function editSubmit(Request $request, $categoryId)
    {
        $data = $request->all();
        $category = new Category();
        $category->editCategory($data, $categoryId);
        return back()->with('fm_success', 'Category has been updated.');
    }

    /**
     * Delete category.
     */
    public function delete(Request $request)
    {
        $categoryId = $request->id;
        if (Category::destroy($categoryId)) {
            return back()->with('fm_success', 'Category has been deleted.');
        }
        return back()->with('fm_error', 'Try Again!');
    }
}