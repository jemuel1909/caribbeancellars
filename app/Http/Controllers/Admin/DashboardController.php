<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class DashboardController extends Controller
{
    
    /**
     * Admin dashboard page
     */
    public function index()
    {
        // dd (Auth::user());
        return view('admin.dashboard.index', ['posts'=> 'hello']);
    }

}
