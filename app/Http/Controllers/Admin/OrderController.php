<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display all orders.
     */
    public function index(Request $request)
    {
        if ($request->dt) {
            return Order::orderBy('ordered_at', 'DESC')->get();
        }
        return view('admin.order.index');
    }

    /**
     * View the order.
     */
    public function view(int $orderId)
    {
        $order = Order::with('orderItems')->findOrFail($orderId);
        return view('admin.order.view', compact('order'));    
    }
}