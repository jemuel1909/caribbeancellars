<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductItem;
use App\Models\QuantityType;
use App\Models\Status;

class ProductController extends Controller
{
    /**
     * Display the list of all products.
     */
    public function index(Request $request)
    {
        if (isset($request->dt)) {
            return Product::all();
        }
        return view('admin.product.index');
    }

    /**
     * Add product and display the form.
     */
    public function add()
    {
        return view('admin.product.add');
    }

    /**
     * Add the product once the form is submitted.
     */
    public function addSubmit(Request $request)
    {
        // Get the form.
        $data = $request->all();

        // Check the data.
        $data['slug'] = str_slug($data['name']);
        
        // Add the product.
        $product = new Product();
        $product->addProduct($data);

        return redirect()->route('admin.product.add')->with('fm_success', 'Product is added.');
    }
    
    /**
     * Edit the product and display the edit form.
     */
    public function edit($productId = 0)
    {
        $product       = Product::with('productItems')->findOrFail($productId);
        $quantityTypes = QuantityType::all();
        $statuses      = Status::all();
        
        return view('admin.product.edit', compact('product', 'quantityTypes', 'statuses'));
    }

    /**
     * Edit the product once form is submitted.
     */
    public function editSubmit(Request $request, $productId = 0)
    {
        // Get the form.
        $data = $request->all();
        $quantityTypes = QuantityType::all();

        // Check the data.
        $data['slug'] = str_slug($data['name']);
        
        // Update the product.
        $product = new Product();
        $product->editProduct($data, $productId);

        // foreach ($quantityTypes as $quantityType) {
        //     $quantityTypeId = $quantityType->id;
        //     $srp = $data['quantity_types'][$quantityTypeId] ?? 0;
        //     if ($srp != 0) {
        //         $productQuantityType = ProductQuantityType::updateOrCreate(
        //             ['product_id' => $productId, 'quantity_type_id' => $quantityTypeId],
        //             ['srp' => $srp]
        //         );
        //     }
        // }
            
        return redirect()->back()->with('fm_success', 'Product is updated.');
    }

    /**
     * Search index.
     */
    public function searchIndex()
    {
        $products = Product::select('id', 'sku', 'name')->with(['categories'])->get();
        
        $i = 0;
        foreach($products as $value) {
            
            $cleanName = preg_replace('/[^A-Za-z0-9 ]/', '', $value->name);
            $categories = $value->categories->implode('name', ', ');
            $searchData = $value->sku . ', ' . $value->name . ',' . $cleanName . ', ' . $categories;
            
            $product = Product::find($value->id);
            $product->search_data = $searchData;
            $product->save();
            $i++;
        }

        return redirect()->route('admin.products')->with('fm_success', "$i Products have been indexed.");
    }

    /**
     * Import product.
     */
    public function import(Request $request)
    {
        return view('admin.product.import');
    }
    
    /**
     * Import product, once form submitted.
     */
    public function importSubmit(Request $request)
    {   

        set_time_limit(300);

        // dd($request->all());
        if ($request->has('csv')) {
            $file = $request->file('csv');
            $path = public_path('upload/csv');
            $file->move($path, 'product.csv');

            $file = public_path('upload/csv/product.csv');
            $data = new \SplFileObject($file);
            $data->setFlags(\SplFileObject::READ_CSV);
            
            // Disable all product first.
            \DB::table('products')->update(['status_id' => 2]);

            foreach($data as $key => $value) {
                
                // Check the key, key 0 is usually the header.
                if (isset($value[0]) && $key > 0 ) {
                    
                    $sku        = $value[0];
                    $name       = $value[1];
                    $categories = $value[2];
                    $perCase    = $value[3];
                    $perBottle  = $value[4];
                    $perCarton  = $value[5];
                    $perPiece   = $value[6];

                    $product = new Product();
                    $productId = $product->addOrEditProduct(['sku' => $sku, 'name' => $name, 'status_id' => 1]);
                    
                    // Insert product item.
                    // Remove first the existing.
                    ProductItem::where('product_id', $productId)->delete();

                    if ($perCase != '' || $perCase != 0) {
                        $productItem = new ProductItem();
                        $productItem->product_id = $productId;
                        $productItem->quantity_type_id = 1; // Case
                        $productItem->srp = $perCase;
                        $productItem->save();
                    }

                    if ($perBottle != '' || $perBottle != 0) {
                        $productItem = new ProductItem();
                        $productItem->product_id = $productId;
                        $productItem->quantity_type_id = 2; // Bottle
                        $productItem->srp = $perBottle;
                        $productItem->save();
                    }

                    if ($perCarton != '' || $perCarton != 0) {
                        $productItem = new ProductItem();
                        $productItem->product_id = $productId;
                        $productItem->quantity_type_id = 3; // Box/Carton
                        $productItem->srp = $perCarton;
                        $productItem->save();
                    }

                    if ($perPiece != '' || $perPiece != 0) {
                        $productItem = new ProductItem();
                        $productItem->product_id = $productId;
                        $productItem->quantity_type_id = 4; // Piece
                        $productItem->srp = $perPiece;
                        $productItem->save();
                    }

                    // Insert product category.
                    // Remove first the existing.
                    ProductCategory::where('product_id', $productId)->delete();
                    $categories = explode(',', $categories);

                    // print_r($categories);
                    // exit();

                    if (isset($categories)) {
                        foreach ($categories as $category) {
                            $category = Category::where('name', trim($category))->first();
                            if (isset($category->id) && $productId) {
                                
                                // $productCategory = ProductCategory::firstOrCreate(
                                //     ['product_id' => $productId], ['category_id' => $category->id]
                                // );
                                $productCategory = new ProductCategory();
                                $productCategory->product_id = $productId;
                                $productCategory->category_id = $category->id;                                
                                $productCategory->save();
                            }
                        }
                    }
                }                
            }

            // Count category
            $categories = Category::all();
            foreach($categories as $category) {
                $total = ProductCategory::where('category_id', $category->id)->count();
                $totalCategory = Category::find($category->id);
                $totalCategory->count = $total;
                $totalCategory->save();
            }

            // Search Index.
            $this->searchIndex();

            // exit();
            // dd('test');
            return redirect()->route('admin.products.import')->with('fm_success', 'Products Imported');    
        }

        return redirect()->route('admin.products.import')->with('fm_error', 'Please Try Again!');
    }
    
    /**
     * Remove product(s) from storage.
     */
    public function delete(Request $request)
    {   
        $productId = $request->id;
        if (Product::destroy($productId)) {
            return redirect()->route('admin.products')->with('fm_success', 'Product is successfully deleted.');
        } 
    }
}