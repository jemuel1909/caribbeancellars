<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\QuantityType;

class QuantityTypeController extends Controller
{
    /**
     * Display all quantity types.
     */
    public function index()
    {
        $quantityTypes = QuantityType::all();
        return view('admin.quantity_type.index', compact('quantityTypes'));
    }
    
    /**
     * Edit quantity type.
     */
    public function edit() 
    {
        
    }

}
