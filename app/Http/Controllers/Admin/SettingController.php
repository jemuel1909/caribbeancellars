<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Page;
use App\Models\Setting;

class SettingController extends Controller
{
    /**
     * Edit the setting.
     */
    public function edit()
    {
        $setting = Setting::findOrFail(1);
        $page = Page::findOrFail(1);
        return view('admin.setting.edit', compact('setting', 'page'));
    }

    /**
     * Edit submit setting.
     */
    public function editSubmit(Request $request)
    {
        set_time_limit(300);

        $request->validate([
            'title' => 'required',
        ]);
        
        if ($request->has('pricelist')) {
            $file = $request->file('pricelist');
            $path = public_path('upload/pdf');
            $file->move($path, 'pricelist.pdf');
        }
        
        if ($request->has('pop_up_image')) {
            $file = $request->file('pop_up_image');
            $path = public_path('upload/popup');
            $file->move($path, 'popup.jpg');
        }
        
        if ($request->has('banner_top_image')) {
            $file = $request->file('banner_top_image');
            $path = public_path('upload/banner');
            $file->move($path, 'top.jpg');
        }

        if ($request->has('banner_left_image')) {
            $file = $request->file('banner_left_image');
            $path = public_path('upload/banner');
            $file->move($path, 'left.jpg');
        }

        $setting                    = Setting::findOrFail(1);
        $setting->title             = $request->title;
        $setting->description       = $request->description;
        $setting->email             = $request->email;
        $setting->additional_emails = $request->additional_emails;
        $setting->has_pop_up        = $request->has_pop_up ?? false;
        $setting->pop_up_url        = $request->pop_up_url ?? null;

        $setting->has_banner_top    = $request->has_banner_top ?? false;
        $setting->banner_top_url    = $request->banner_top_url ?? null;

        $setting->has_banner_left   = $request->has_banner_left ?? false;
        $setting->banner_left_url   = $request->banner_left_url ?? null;

        $setting->save();
        
        if ($request->has('page')) {
            $page = Page::findOrFail(1);
            $page->description = $request->page['description'];
            $page->save();
        }

        return redirect()->route('admin.settings')->with('fm_success', 'Setting is updated.');   
    }
}
