<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\State;
use App\Models\User;
use App\Models\UserInfo;

use App\Http\Requests\RegisterForm;

use Validator;

class UserController extends Controller
{
    /**
     * Display all users.
     * 
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.user.index', [
            'users' => $users
        ]);
    }


    /**
     * Add user.
     * 
     * @return Response
     */
    public function add()
    {
        
        $states = State::all();
        return view('admin.user.add', [
            'states' => $states
        ]);    
    
    }

    /**
     * Submit add user.
     * 
     * @param RegisterForm $request
     */
    public function addSubmit(Request $request) 
    {
        $data   = $request->all();

        $user   = new User();
        $userId =  $user->addUser($data);
        
        $userInfo   = new UserInfo();
        $userInfoId = $userInfo->addUserInfo($data, $userId);

        return redirect('admin/user/add')->with('fm_success', 'User is added.');

    }

    /**
     * Edit the user.
     * 
     * @return Response
     */
    public function edit(Request $request, $userId = 0)
    {

        $states = State::all();

        // Get the user info.
        $user     = User::findOrFail($userId);
        $userInfo = UserInfo::where('user_id', $userId)->first();
        
        return view('admin.user.edit', [
            'states'   => $states,
            'user'     => $user,
            'userInfo' => $userInfo
        ]);
    }

    /**
     * Edit the user submit.
     * 
     * @param Request $request
     * @param int $userId
     * @return Response
     */
    public function editSubmit(Request $request, $userId = 0)
    {   
        $data = $request->all();
        
        // Get the user.
        $user = User::findOrFail($userId);
        
        // Set the user id.
        $userId = $user->id;
        
        // Edit the user.
        $user = new User();
        $user->editUser($data, $userId);


        // Then, edit the user info.
        $userInfo = UserInfo::select('id')->where('user_id', $userId)->firstOrFail();
        
        // Set the user info id.
        $userInfoId = $userInfo->id;

        // Edit the user info.
        $userInfo = new UserInfo();
        $userInfo->editUserInfo($data, $userInfoId);

        return redirect()->route('admin.user.edit', ['user_id' => $userId])->with('fm_success', 'User Info has been updated.');
    }

    /**
     * Edit the user password.
     * 
     * @return Response
     */
    public function editPasswordSubmit(Request $request, $userId = 0)
    {   

        // Validate the password.
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        if ($validator->fails()) {
            return redirect("admin/user/edit/$userId")->withErrors($validator)->withInput();
        }

        // Get the user info.
        $user = User::findOrFail($userId);
        $user->password = bcrypt($request->password);
        $user->save();
        
        return redirect("admin/user/edit/$userId")->with('fm_success', 'Password has been updated.');
    }


    /**
     * Soft delete the user.
     * 
     * @param int $puppyId
     */
    public function delete($userId = 0)
    {
        $user = User::findOrFail($userId);
        $user->delete();
        return redirect()->route('admin.user')->with('fm_success', 'User is successfully deleted!');
    }

}
