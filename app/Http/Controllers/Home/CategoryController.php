<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Setting;

class CategoryController extends Controller
{
    /**
     * Display category with products.
     */
    public function index($slug = null)
    {
        if (!isset($_COOKIE['verify_age'])) {
            return redirect()->route('verify');
        }

        // 
        $setting = Setting::find(1);

        // Category.
        $category = Category::where('slug', $slug)->firstOrFail();
        $categoryId = $category->id;
        $categoryParentId = $category->parent_id;

        // Products.
        $products = ProductCategory::with(['product' => function($query) {
            $query->with('productItems');
        }])->where('category_id', $categoryId)->paginate(15);
        
        // All categories.
        $categories = Category::with(['subCategories' => function($query) {
            $query->where(['type' => 'default'])->orderBy('name');
        }])->orderBy('name')->where(['parent_id' => 0, 'type' => 'default'])->get();

        // Region categories
        $categoryRegions = Category::with(['subCategories' => function($query) {
            $query->orderBy('name');
        }])->orderBy('name')->where(['parent_id' => 21, 'type' => 'manufactured'])->get();        

        return view('home.category.index', compact(
            'category', 
            'categoryId', 
            'categoryParentId',
            'categories',
            'categoryRegions',
            'products',
            'setting'
        ));
    }
}