<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;

class IndexController extends Controller
{   
    /**
     * Show application home page.
     */
    public function index()
    {
        if (!isset($_COOKIE['verify_age'])) {
            return redirect()->route('verify');
        }

        if (!isset($_COOKIE['popup_product'])) {
            $name = "popup_product";
            $value = "true";
            setcookie($name, $value, time() + (3600), "/"); // 3600 = 1 hr   
        }

        $products   = Product::select('id', 'sku', 'name')->with('productItems')->active()->paginate(15);
        $categories = Category::orderBy('name')->where(['parent_id' => 0, 'type' => 'default'])->get();
        $setting    = Setting::find(1);
        
        return view('home.index.index', compact(
            'categories', 
            'products', 
            'setting'
        ));
    }

    /**
     * Search submit.
     */
    public function searchSubmit(Request $request)
    {   

        if (!isset($_COOKIE['verify_age'])) {
            return redirect()->route('verify');
        }

        $searchQuery = urlencode($request->search_query);
        return redirect()->route('search', ['search_query' => $searchQuery]);
    }

    /**
     * Show search result.
     */
    public function search(Request $request, $searchQuery = null)
    {
        if (!isset($_COOKIE['verify_age'])) {
            return redirect()->route('verify');
        }

        $searchQuery = urldecode($searchQuery);
        $cleanSearchQuery = preg_replace('/[^A-Za-z0-9 ]/', '', $searchQuery);

        $products = Product::select('id', 'sku', 'name')->with('productItems')->where('search_data', 'like', '%' . $cleanSearchQuery . '%')->active()->paginate(15);
        $categories = Category::orderBy('name')->where(['parent_id' => 0, 'type' => 'default'])->get();
        $setting    = Setting::find(1);
        
        return view('home.index.search', compact('categories', 'products', 'searchQuery', 'setting'));
    }
}