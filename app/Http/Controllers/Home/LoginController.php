<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;

use Auth;
use Mail;

class LoginController extends Controller
{
 
    /**
     * Display the login form.
     */
    public function login(Request $request)
    {

        // If submitted.
        if ($request->isMethod('post')) {

            // Validate
            $request->validate(['email' => 'required', 'password' => 'required']);

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                
                // Authentication passed.
                // Check the role. 1 is admin
                if (Auth::user()->role_id == 1) {
                    return redirect()->intended('admin');
                } else {
                    if (isset($request->redirect_to)) {
                        return redirect()->route($request->redirect_to);
                    } else {
                        return redirect()->route('user.dashboard');
                    }
                }
                
            } else {

                return redirect('login')->with('fm_error', 'Username or password does not match.');
            }
        }

        return view('home.login.login');
    }

    /**
     * Logout user.
     */
    public function logout()
    {
        Auth::logout();
        return redirect('login')->with('fm_success', 'Successfully logout.');
    }

    /**
     * Forgot password.
     */
    public function forgotPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            
            $validator = $request->validate([
                'email' => 'exists:users',
            ]);

            if (!$validator) {
                return redirect()->route('forgot_password')->with('fm_error', 'Your email does not existed on our system');
            }

            $email  = $request->email;
            $user   = User::where(['email' => $email, 'status_id' => 1])->firstOrFail();
            $userId = $user->id;

            // Create email token.
            $emailToken = str_random(75);
            
            // Update the user email token.
            $user = User::find($userId);
            $user->email_token = $emailToken;
            $user->save();

            $user = User::find($userId);
            $data['first_name']  = $user->first_name;
            $data['email_token'] = $emailToken;

            // Send email.
            Mail::to($email)->queue(new \App\Mail\User\ForgotPassword($data));

            return redirect()->route('login')->with('fm_success', 'Verification has been sent. Check your mail box.');
        }

        return view('home.login.forgot_password', []);
    }

    /**
     * Reset password.
     */
    public function resetPassword($emailToken = '')
    {
        
        if ($emailToken != null) {

            $user = User::where('email_token', $emailToken)->firstOrFail();
            return view('home.login.reset_password',[
                'emailToken' => $emailToken
            ]);
       
        } else {

            return redirect()->route('login')->with('fm_error', 'Invalid Email Token.');
        }

    }

    /**
     * Reset submit password.
     */
    public function resetPasswordSubmit(Request $request)
    {
        $validator = $request->validate([
            'email_token' => 'required',
            'password'    => 'required|confirmed'
        ]);
        
        $emailToken = $request->email_token;
        $password   = $request->password;

        if ($emailToken != null) {

            $user = User::where(['email_token' => $emailToken])->firstOrFail();
            $userId = $user->id;

            // Update the status id of the user, also set email token value to null.
            $user              = User::find($userId);
            $user->password    = bcrypt($password);
            $user->email_token = '';
            $user->status_id   = 1;
            $user->save();

            return redirect()->route('login')->with('fm_success', 'Password has been reset. You may now login.');
            
        } else {
            
            return redirect()->route('login');
            
        }   
    }
}