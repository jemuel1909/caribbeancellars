<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Mail;
use Validator;

use App\Models\DeliveryLocation;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductItem;

class OrderController extends Controller
{
    
    /**
     * Put an order to session.
     */
    public function put(Request $request)
    { 
        // Via a request instance...
        $orders = $request->session()->get('orders');
        
        $productId      = $request->product_id;
        $quantity       = $request->quantity;
        $quantityTypeId = $request->quantity_type_id;
        
        // Get the product quantity type.
        $productItem    = ProductItem::where(['product_id' => $productId, 'quantity_type_id' => $quantityTypeId])->firstOrFail();

        // Set the product quantity type info.
        $productId      = $productItem->product_id;
        $name           = $productItem->product->name;
        $sku            = $productItem->product->sku;
        $quantityTypeId = $productItem->quantity_type_id;
        $srp            = $productItem->srp;
        $totalAmount    = $srp * $quantity;
        
        $orders[$productId][$quantityTypeId][] = ['quantity' => $quantity];
        $request->session()->put('orders', $orders);

        return $this->tally();
    }

    /**
     * Update an order in the session.
     */
    public function update(Request $request)
    {
        // Via a request instance...
        $orders = $request->session()->get('orders');

        foreach ($request->prod as $productId => $quantityType) {
            foreach ($quantityType as $quantityTypeId => $quantity) {
                if ($quantity != 0) {
                    $orders[$productId][$quantityTypeId] = array(['quantity' => $quantity]);
                } else {
                    unset($orders[$productId]);
                }
            }
        }
        
        $request->session()->put('orders', $orders);

        return redirect()->route('order.summary')->with('fm_success', 'Your order has been updated.');
    }

    /**
     * Remove an item in the order session.
     */
    public function remove(Request $request)
    {
        // Via a request instance...
        $orders = $request->session()->get('orders');

        $productId = $request->product_id;
        unset($orders[$productId]);
        
        $request->session()->put('orders', $orders);

        return $this->tally();
    }

    /**
     * Put an order to session.
     */
    public function tally()
    {
        // Via a request instance...
        $orders = session('orders');
        $data = [];
        if (isset($orders)) {
            foreach ($orders as $productId => $products) {

                foreach ($products as $quantityTypeId => $productItems) {
                    
                    $quantity = collect($productItems);
                    $totalQuantity = $quantity->sum('quantity');
    
                    // Get the product quantity type.
                    $productItem = ProductItem::where(['product_id' => $productId, 'quantity_type_id' => $quantityTypeId])->firstOrFail();
    
                    // Set the product quantity type info.
                    $srp         = $productItem->srp;
                    $totalAmount = $srp * $totalQuantity;
    
                    $data[] = array(
                        'product_id'       => $productItem->product_id,
                        'name'             => $productItem->product->name,
                        'sku'              => $productItem->product->sku,
                        'quantity_type_id' => $productItem->quantity_type_id,
                        'quantity_name'    => $productItem->quantityType->name,
                        'srp'              => $productItem->srp,
                        'total_quantity'   => $totalQuantity,
                        'total_amount'     => $totalAmount
                    );
                }
            }
        }
        
        return $data;
    }

    /**
     * Order summary.
     */
    public function summary(Request $request)
    {   
        $orders = $this->tally();

        if (session('current_transaction')) {
            $request->session()->forget('current_transaction');
        }

        return view('home.order.summary', compact('orders'));
    }

    /**
     * Order shipping.
     */
    public function shipping(Request $request)
    {   

        // check if we have order item/s to process
        $orders = $this->tally();
        if (empty($orders)) {
            return redirect()->route('home');
        }

        // Get delivery locations.
        $deliveryLocations = DeliveryLocation::get(['id', 'name']);
        $user = Auth::user();
        
        $shippingInfo = [];
        if (session('current_transaction')) {
            $currentTransaction = $request->session()->get('current_transaction');
            $shippingInfo = $currentTransaction['shipping_info'];
        } else {
            if (isset($user->id)) {
                $shippingInfo = array(
                    'phone_no' => $user->userInfo->phone_no
                );
            }
        }

        return view('home.order.shipping', compact('deliveryLocations', 'orders', 'user', 'shippingInfo'));
    }

    /**
     * Order payment.
     */
    public function payment(Request $request)
    {   
        // Check if we have order item/s to process.
        $orders = $this->tally();
        if (empty($orders)) {
            return redirect()->route('home');
        }

        $user = Auth::user();

        if ($request->post()) {

            // Validate Recaptcha.
            $validator = Validator::make($request->all(), [
                'g-recaptcha-response' => ['required', new \App\Rules\ReCaptcha],
            ]);
            
            if ($validator->fails()) {
                return redirect()->route('order.shipping')->withErrors($validator)->withInput();
            }
            
            $shippingInfo = $request->all();
            
            // Check delivery location, save the location name to order.
            $deliveryLocationId                = $shippingInfo['delivery_location_id'];
            $deliveryLocation                  = DeliveryLocation::findOrFail($deliveryLocationId);
            $shippingInfo['delivery_location'] =  $deliveryLocation->name;
            
            // Place in session the current transaction.
            $request->session()->put('current_transaction', ['shipping_info' => $shippingInfo]);
            
        } else {

            if (session('current_transaction')) {
                $currentTransaction = $request->session()->get('current_transaction');
                $shippingInfo = $currentTransaction['shipping_info'];
            } else {
                return redirect()->route('order.shipping');
            }
        }
        
        return view('home.order.payment', compact('user', 'orders', 'shippingInfo'));
    }

    /**
     * Order Checkout.
     */
    public function checkout(Request $request)
    {
        $user = Auth::user();
        $currentTransaction = $request->session()->get('current_transaction');

        if (!isset($user->id)) {

            $userId = null; 
            $orderData = array(
                'user_id'    => $userId,
                'first_name' => $currentTransaction['shipping_info']['first_name'],
                'last_name'  => $currentTransaction['shipping_info']['last_name'],
                'email'      => $currentTransaction['shipping_info']['email']
            );

        } else {

            $orderData = array(
                'user_id'    => $user->id,
                'first_name' => $user->first_name,
                'last_name'  => $user->last_name,
                'email'      => $user->email,
            );
        }
        
        $orderData['phone_no']             = $currentTransaction['shipping_info']['phone_no'];
        $orderData['delivery_location_id'] = $currentTransaction['shipping_info']['delivery_location_id'];
        $orderData['delivery_location']    = $currentTransaction['shipping_info']['delivery_location'];
        $orderData['notes']                = $currentTransaction['shipping_info']['notes'];
        $orderData['ordered_at']           = date('Y-m-d H:i:s');
        $deliveryDate                      = $currentTransaction['shipping_info']['delivery_date'];
        $deliveryTime                      = $currentTransaction['shipping_info']['delivery_time'];
        
        $orderData['delivery_date']        = isset($deliveryDate) ? date('Y-m-d', strtotime($deliveryDate)) : null;
        $orderData['delivery_time']        = isset($deliveryTime) ? date('H:i:s', strtotime($deliveryTime)) : null;

        // Generate Tracking ID.
        $random_str_arr = str_split(str_random(15), 12);
        $orderData['tracking_id'] = strtoupper($random_str_arr[1]) . '-' . $random_str_arr[0];
        
        // Add order.
        $order = Order::create($orderData);
        $currentTransaction['order'] = $order->id;
        
        $request->session()->put('current_transaction', ['current_transaction' => $currentTransaction]);
        
        $overallTotalAmount = 0;
        $orders = $request->session()->get('orders');
        foreach ($orders as $productId => $products) {

            foreach ($products as $quantityTypeId => $productItems) {
                
                $quantity = collect($productItems);
                $totalQuantity = $quantity->sum('quantity');

                // Get the product quantity type.
                $productItem = ProductItem::where(['product_id' => $productId, 'quantity_type_id' => $quantityTypeId])->firstOrFail();

                // Set the product quantity type info.
                $srp            = $productItem->srp;
                $totalAmount    = $srp * $totalQuantity;
                $overallTotalAmount  += $totalAmount;

                // Add order item.
                $itemData = array(
                    'order_id'          => $currentTransaction['order'],
                    'product_id'        => $productItem->product_id,
                    'sku'               => $productItem->product->sku,
                    'name'              => $productItem->product->name,
                    'quantity_type_id'  => $productItem->quantity_type_id,
                    'total_quantity'    => $totalQuantity,
                    'amount'            => $productItem->srp,
                    'total_amount'      => $totalAmount
                );
                $orderItem = OrderItem::create($itemData);

            }
        }

        // Update the order's total amount.
        $currentOrder = Order::find($currentTransaction['order']);
        $currentOrder->total_amount = $overallTotalAmount;
        $currentOrder->save();

        return redirect()->route('order.completed');
    }

    /**
     * Once order is completed.
     */
    public function completed(Request $request)
    {
        if (!session('current_transaction')) {

            return redirect()->route('home');
        
        } else {

            $currentTransaction = session('current_transaction');
            $orderId = $currentTransaction['current_transaction']['order'];
            
            // Get order.
            $order = Order::with('orderItems')->where('id', $orderId)->firstOrFail();
            
            // Mail customer.
            Mail::to($order->email)->queue(new \App\Mail\User\OrderCreated($order));

            // Mail admin.
            // Mail::to('support@bearcatserver.com')->bcc(['bearcatserver@gmail.com'])->queue(new \App\Mail\Admin\OrderCreated($order));
            Mail::to('provisioning@caribbeancellars.com')->bcc(['wayne@limcvi.com', 'jemuel.inlinebits@gmail.com'])->queue(new \App\Mail\Admin\OrderCreated($order));
            
            // Clear the order session.
            $request->session()->forget('orders');
            $request->session()->forget('current_transaction');
            
            return view('home.order.completed', compact('order'));
        }
    }
}