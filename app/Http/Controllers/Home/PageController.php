<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Mail;
use Validator;

use App\Models\Page;

class PageController extends Controller
{
    /**
     * Contact us.
     */
    public function contact()
    {
        return view('home.page.contact');
    }

    /**
     * Contact us.
     */
    public function contactSubmit(Request $request)
    {   
        $data = $request->all();
        // dd($data);

        // Validate Recaptcha.
        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => ['required', new \App\Rules\ReCaptcha],
        ]);
        
        if ($validator->fails()) {
            return redirect()->route('order.shipping')->withErrors($validator)->withInput();
        }

        // Mail admin.
        // Mail::to('corders@caribbeancellars.com')->bcc(['wayne@limcvi.com', 'jemuel.inlinebits@gmail.com'])->queue(new \App\Mail\Admin\Contact($data));
        Mail::to('provisioning@caribbeancellars.com')->bcc(['wayne@limcvi.com', 'jemuel.inlinebits@gmail.com'])->queue(new \App\Mail\Admin\Contact($data));
        
        return redirect()->route('contact')->with('fm_success', 'We received your message. Thank you!');
    }

    /**
     * Verify age.
     */
    public function verify()
    {
        return view('home.page.verify');
    }

    /**
     * Verify age.
     */
    public function verifySubmit(Request $request)
    {
        $name = "verify_age";
        $value = "true";
        setcookie($name, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
        return redirect()->route('home');
    }

    /**
     * About us.
     */
    public function aboutUs()
    {
        return view('home.page.about_us');
    }

    /**
     * FAQ.
     */
    public function faq()
    {
        $page = Page::findOrFail(1);
        return view('home.page.faq', compact('page'));
    }

}