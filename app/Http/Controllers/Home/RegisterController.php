<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\User\RegisterForm;

use App\Models\User;
use App\Models\UserInfo;
use App\Models\UserMembership;

use Auth;
use Mail;

class RegisterController extends Controller
{
    
    /**
     * Register a user.
     */
    public function register()
    {
        // Redirect if Auth is true.
        if (Auth::check()) {
            return redirect()->route('user.dashboard');
        }
        
        return view('home.register.register', []);
    }

    /**
     * Submit Register.
     */
    public function registerSubmit(RegisterForm $request) 
    {
        $data = $request->all();
        
        // Set the role and status.
        // Since not yet activated, set it to null.
        $data['role_id']   = null;
        $data['status_id'] = 3;

        // Add user.
        $user   = new User();
        $userId = $user->addUser($data);
        
        // Add user info.
        $data['website']  = null;
        $data['state_id'] = null;
        $userInfo = new UserInfo();
        $userInfo->addUserInfo($data, $userId);

        // Set the email token.
        $emailToken = str_random(75);
        
        // Update user email token.
        $user              = User::find($userId);
        $user->email_token = $emailToken;
        $user->save();

        // Set the id and email token for email template.
        $data['email_token'] = $emailToken;
        $data['user_id']     = $userId; 

        // Get the email.
        $email = $data['email'];
        
        // Send email to user.
        Mail::to($email)->queue(new \App\Mail\User\Register($data));

        // Notify the admin.
        // Mail::to('corders@caribbeancellars.com')->bcc(['jemuel.inlinebits@gmail.com'])->queue(new \App\Mail\Admin\Register($data));
        Mail::to('provisioning@caribbeancellars.com')->bcc(['jemuel.inlinebits@gmail.com'])->queue(new \App\Mail\Admin\Register($data));

        return redirect()->route('register')->with('fm_success', 'Successfully Registered. Please check your email for confirmation.');
        
    }

    /**
     * Activate user using email token.
     */
    public function activate($emailToken = '') 
    {   
        
        if ($emailToken != null) {

            $user   = User::where(['email_token' => $emailToken, 'status_id' => 3])->firstOrFail();
            $userId = $user->id;
            $email  = $user->email;

            // Update the status and role of the user, also make email token value null.
            $user              = User::find($userId);
            $user->role_id     = 5;
            $user->status_id   = 1;
            $user->email_token = null;
            $user->save();
            
            // Send welcome email.
            // Mail::to($email)->queue(new \App\Mail\User\Welcome());
            
            return redirect()->route('login')->with('fm_success', 'Your account is activated. You may now login!');
            
        } else {
            
            return redirect()->route('register');
        }
    }
}