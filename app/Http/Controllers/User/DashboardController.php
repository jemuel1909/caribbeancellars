<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Carbon\Carbon;
use App\Models\Order;

class DashboardController extends Controller
{
    /**
     * Display the user dashboard.
     */
    public function index()
    {   
        $userId = Auth::user()->id;

        $focus_date = 'Past 6 Months';
        $orders = Order::where([
            ['user_id', '=', $userId],
            ['status_id', '!=', '0'],
            ['created_at', '>=', Carbon::now()->subMonths(6)]
        ])
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('home.user.dashboard.index', [
            'orders' => $orders,
            'focusDate' => $focus_date,
            'focusStatus' => ''
        ]);
    }
}