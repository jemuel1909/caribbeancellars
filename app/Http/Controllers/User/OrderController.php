<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Carbon\Carbon;
use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display the list of orders placed by the user.
     */
    public function index(Request $request)
    {   
        $userId = Auth::user()->id;

        $status = $request->status_condition;
        if ($status == 'open') {
            $status_condition = ['status_id', '=', '3'];
        } elseif ($status == 'cancelled') {
            $status_condition = ['status_id', '=', '5'];
        } else {
            $status_condition = ['status_id', '!=', '0'];
        }
       
        switch ($request->date_range) {
            case 'last-30-days':
                $array_condition = [
                    ['user_id', '=', $userId],
                    $status_condition,
                    ['created_at', '>=', Carbon::now()->subDays(30)],
                ];
                $focus_date = 'Last 30 Days';
                break;

            case '2017':
                $array_condition = [
                    ['user_id', '=', $userId],
                    $status_condition,
                    ['created_at', '>=', Carbon::parse('first day of January 2017')],
                    ['created_at', '<', Carbon::parse('first day of January 2018')]
                ];
                $focus_date = $request->date_range;
                break;

            case '2016':
                $array_condition = [
                    ['user_id', '=', $userId],
                    $status_condition,
                    ['created_at', '>=', Carbon::parse('first day of January 2016')],
                    ['created_at', '<', Carbon::parse('first day of January 2017')]
                ];
                $focus_date = $request->date_range;
                break;

            default:
                $array_condition = [
                    ['user_id', '=', $userId],
                    $status_condition,
                    ['created_at', '>=', Carbon::now()->subMonths(6)]
                ];
                $focus_date = 'Past 6 Months';
        }

        $orders = Order::where($array_condition)
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('home.user.dashboard.index', [
            'orders' => $orders,
            'focusDate' => $focus_date,
            'focusStatus' => $status
        ]);
    }

    /**
     * Display the order with order items.
     */
    public function show($orderId = 0)
    {   
        $userId = Auth::user()->id;
        
        $order = Order::with('orderItems')->where(['user_id' => $userId, 'id' => $orderId])->firstOrFail();

        $focusDate = '';
        $focusStatus = '';

        return view('home.user.order.show', compact('order', 'focusDate', 'focusStatus'));
    }
}