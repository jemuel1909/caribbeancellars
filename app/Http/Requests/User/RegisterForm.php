<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'      => 'required|string|email|max:255|unique:users|confirmed',
            'password'   => 'required|string|min:6|confirmed',
            'first_name' => 'required|string|min:1',
            'last_name'  => 'required|string|min:1',
        ];
    }
}