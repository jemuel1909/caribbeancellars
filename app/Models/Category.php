<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     */
    protected $dates = ['deleted_at'];

    /**
     * Scope a query to only include active.
     */
    public function scopeActive($query)
    {
        return $query->where('status_id', 1);
    }

    /**
     * Scope a query to sort by sort no.
     */
    public function scopeSortNo($query)
    {
        return $query->orderBy('sort_no', 'ASC');
    }
    
    /**
     * Get the membership of the membership option.
     */
    public function subCategories()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

    /**
     * Add category.
     */
    public function addCategory($data)
    {
        $category            = new Category();
        $category->name      = $data['name'];
        $category->slug      = str_slug($data['name']);
        $category->parent_id = $data['parent_id'];
        $category->type      = $data['type'];
        $category->status_id = $data['status_id'];
        $category->save();

        return $category->id;
    }

    /**
     * Edit category.
     */
    public function editCategory($data, $categoryId)
    {
        $category            = Category::findOrFail($categoryId);
        $category->name      = $data['name'];
        $category->slug      = $data['slug'];
        $category->parent_id = $data['parent_id'];
        $category->type      = $data['type'];
        $category->status_id = $data['status_id'];
        
        return $category->save();
    }

}