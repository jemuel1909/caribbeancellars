<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'status_id',
        'ordered_at',
        'tracking_id',
        'total_amount',
        'first_name',
        'last_name',
        'email',
        'address',
        'city',
        'phone_no',
        'zip_code',
        'delivery_location_id',
        'delivery_location',
        'delivery_date',
        'delivery_time',
        'notes'
    ];
    
    // /**
    //  * Set delivered at.
    //  */
    // public function setDeliveredAtAttribute($value)
    // {
    //     if ($value != null) {
    //         $this->attributes['delivered_at'] = date('Y-m-d h:i:s', strtotime($value));
    //     } else {
    //         $this->attributes['delivered_at'] = null;
    //     }
    // }
    
    /**
     * Get the order record associated with the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    /**
     * Get the order items of the order.
     */
    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    /**
     * Get the ordered at.
     */
    public function getOrderedAtAttribute($value)
    {
        return date('M-d-Y', strtotime($value));
    }
}