<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'sku',
        'name',
        'quantity_type_id',
        'total_quantity',
        'amount',
        'total_amount'
    ];
    
    /**
     * Get the quantity type of order item.
     */
    public function quantityType()
    {
        return $this->belongsTo('App\Models\QuantityType');
    }

    /**
     * Get the order id of an order item.
     */
    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }

}
