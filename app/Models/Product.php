<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['sku', 'name', 'slug', 'status_id'];

    /**
     * Scope a query to only include active.
     */
    public function scopeActive($query)
    {
        return $query->where('status_id', 1);
    }

    /**
     * Get the product quantity types of the product.
     */
    public function productItems()
    {
        return $this->hasMany('App\Models\ProductItem');
    }

    /**
     * Get the categories.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'product_categories');
    }

    /**
     * Add product.
     */
    public function addProduct($data)
    {
        $product            = new Product();
        $product->sku       = $data['sku'];
        $product->name      = $data['name'];
        $product->slug      = $data['slug'];
        $product->status_id = $data['status_id'];

        return $product->save();
    }

    /**
     * Edit product.
     */
    public function editProduct($data, $productId)
    {
        $product            = Product::findOrFail($productId);
        $product->sku       = $data['sku'];
        $product->name      = $data['name'];
        $product->slug      = $data['slug'];
        $product->status_id = $data['status_id'];
        
        return $product->save();
    }
    
    /**
     * Add or edit product.
     */
    public function addOrEditProduct($data)
    {
        $product = Product::updateOrCreate(
            [
                'sku' => $data['sku'], 
            ],
            [
                'name' => $data['name'],
                'status_id' => $data['status_id']
            ]
        );   
        
        return $product->id;
    }
}