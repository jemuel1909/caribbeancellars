<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['product_id', 'category_id'];

    /**
     * Get product.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Get category.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    
}