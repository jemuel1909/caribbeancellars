<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
    
    /**
     * Mass assignment.
     */
    protected $fillable = ['srp'];

    /**
     * Set base salary.
     */
    public function setSrpAttribute($value)
    {
        $srp = isset($value) ? filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) : null;
        $this->attributes['srp'] = ($srp != '') ? $srp : 0;
    }

    /**
     * Get the quantity type of product item.
     */
    public function quantityType()
    {
        return $this->belongsTo('App\Models\QuantityType');
    }

    /**
     * Get the product of a product item.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

}
