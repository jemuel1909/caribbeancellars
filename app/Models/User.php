<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the user's full name.
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Get the role of user.
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * Get the user info of user.
     */
    public function userInfo()
    {
        return $this->hasOne('App\Models\UserInfo');
    }

    /**
     * Get the status of user.
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    /**
     * Add user.
     */
    public function addUser($data)
    {
        $user             = new User();
        $user->username   = $data['email'];
        $user->email      = $data['email'];
        $user->password   = bcrypt($data['password']);
        $user->first_name = $data['first_name'];
        $user->last_name  = $data['last_name'];
        $user->role_id    = $data['role_id'];
        $user->status_id  = $data['status_id'];

        $user->save();
        return $user->id;
    }

    /**
     * Edit user.
     */
    public function editUser($data, $userId)
    {
        $user             = User::find($userId);
        $user->first_name = $data['first_name'];
        $user->last_name  = $data['last_name'];
        // $user->status_id  = $data['status_id'];
        
        return $user->save();
    }

}
