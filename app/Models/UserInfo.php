<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserInfo extends Model
{
    
    /**
     * Add user info.
     */
    public function addUserInfo(array $data, int $userId)
    {
        $userInfo           = new UserInfo;
        $userInfo->user_id  = $userId;
        $userInfo->address  = $data['address'];
        $userInfo->city     = $data['city'];
        $userInfo->state_id = $data['state_id'];
        $userInfo->phone_no = $data['phone_no'];
        $userInfo->zip_code = $data['zip_code'];
        // $userInfo->website  = $data['website'];
        
        $userInfo->save();
        $userInfoId = $userInfo->id;
        return $userInfoId;
    }
    
    /**
     * Edit user info.
     */
    public function editUserInfo(array $data, int $userInfoId)
    {
        $userInfo           = UserInfo::find($userInfoId);
        $userInfo->address  = $data['address'];
        $userInfo->city     = $data['city'];
        $userInfo->state_id = $data['state_id'];
        $userInfo->phone_no = $data['phone_no'];
        $userInfo->zip_code = $data['zip_code'];
        // $userInfo->website  = $data['website'];

        return $userInfo->save();
    }

}
