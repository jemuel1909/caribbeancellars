<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50)->unique();
            $table->string('first_name', 50);
            $table->string('middle_name', 50)->nullable();
            $table->string('last_name', 50);
            $table->string('email', 100)->unique();
            $table->string('password', 150);
            $table->tinyInteger('role_id')->unsigned()->nullable();
            $table->tinyInteger('status_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->string('email_token', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Create foreign key.
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');

        });
        
        Schema::enableForeignKeyConstraints();
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
