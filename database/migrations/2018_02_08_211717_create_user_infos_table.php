<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('address', 350)->nullable();
            $table->string('city', 150)->nullable();
            $table->integer('state_id')->unsigned()->nullable();
            $table->string('phone_no', 50)->nullable();
            $table->string('zip_code', 16)->nullable();
            $table->string('website', 250)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            // Create foreign key.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}