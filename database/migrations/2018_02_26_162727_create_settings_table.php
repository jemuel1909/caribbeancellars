<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 150);
            $table->text('description')->nullable();
            $table->string('name', 150);
            $table->string('email', 150)->nullable();
            $table->text('additional_emails')->nullable()->comment('Additional email, can be use as cc or bcc.');
            $table->boolean('has_pop_up')->default(false);
            $table->string('pop_up_url', 250)->nullable();

            $table->boolean('has_banner_top')->default(false);
            $table->string('banner_top_url', 250)->nullable();

            $table->boolean('has_banner_left')->default(false);
            $table->string('banner_left_url', 250)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
