<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable();
            $table->tinyInteger('quantity_type_id')->unsigned()->nullable();
            $table->double('srp', 10, 2)->unsigned()->nullable();
            $table->timestamps();

            // Unique key.
            $table->unique(['product_id', 'quantity_type_id']);

            // Foreign key.
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('quantity_type_id')->references('id')->on('quantity_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_items');
    }
}