<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// use DB;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->tinyInteger('status_id')->unsigned()->default(3)->comment('3 = pending, 4 = rejected, 5 = completed');
            $table->string('tracking_id', 25)->nullable();
            $table->double('total_amount', 10, 2)->default('0.00');
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('address', 350)->nullable();
            $table->string('city', 150)->nullable();
            $table->integer('state_id')->unsigned()->default(0);
            $table->string('phone_no', 20)->nullable();
            $table->string('zip_code', 16)->nullable();
            $table->integer('delivery_location_id')->unsigned()->nullable();
            $table->string('delivery_location', 150)->nullable();
            $table->date('delivery_date')->nullable();
            $table->time('delivery_time')->nullable();
            $table->text('notes')->nullable();
            $table->timestamp('ordered_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            // Create foreign key.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('delivery_location_id')->references('id')->on('delivery_locations')->onDelete('set null');
        });

        DB::statement("ALTER TABLE store_orders AUTO_INCREMENT = 15000;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}