<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('sku')->unsigned();
            $table->string('name', 250)->nullable();
            $table->tinyInteger('quantity_type_id')->unsigned()->nullable();
            $table->integer('total_quantity')->unsigned()->default(0);
            $table->double('amount', 10, 2)->unsigned()->default(0);
            $table->double('total_amount', 10, 2)->unsigned()->default(0);
            $table->timestamps();
            
            // Create foreign key.
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('quantity_type_id')->references('id')->on('quantity_types')->onDelete('set null');

        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
