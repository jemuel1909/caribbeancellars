<?php

use Illuminate\Database\Seeder;

use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_categories') {
                foreach($data['data'] as $value) {
                    $category = new Category();
                    $category->fill($value);
                    $category->save();
                }
                break;
            }
        }
    }
}