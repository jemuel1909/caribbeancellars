<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            StatusesTableSeeder::class,
            UsersTableSeeder::class,
            UserInfosTableSeeder::class,
            StatesTableSeeder::class,
            DeliveryLocationsTableSeeder::class,
            SettingsTableSeeder::class,
            CategoriesTableSeeder::class,
            QuantityTypesTableSeeder::class,
            PagesTableSeeder::class,
            ProductsTableSeeder::class,
            ProductItemsTableSeeder::class,
            ProductCategoriesTableSeeder::class,
            OrdersTableSeeder::class,
            OrderItemsTableSeeder::class,
        ]);
    }
}