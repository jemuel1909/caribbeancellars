<?php

use Illuminate\Database\Seeder;

use App\Models\DeliveryLocation;

class DeliveryLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_delivery_locations') {
                foreach($data['data'] as $value) {
                    $deliveryLocation = new DeliveryLocation();
                    $deliveryLocation->fill($value);
                    $deliveryLocation->save();
                }
                break;
            }
        }
    }
}