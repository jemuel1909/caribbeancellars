<?php

use Illuminate\Database\Seeder;

use App\Models\OrderItem;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_order_items') {
                foreach($data['data'] as $value) {
                    $orderItem = new OrderItem();
                    $orderItem->fill($value);
                    $orderItem->save();
                }
                break;
            }
        }
    }
}