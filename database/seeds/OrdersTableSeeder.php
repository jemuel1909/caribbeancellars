<?php

use Illuminate\Database\Seeder;

use App\Models\Order;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_orders') {
                foreach($data['data'] as $value) {
                    $order = new Order();
                    $order->fill($value);
                    $order->save();
                }
                break;
            }
        }
    }
}