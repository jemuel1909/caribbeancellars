<?php

use Illuminate\Database\Seeder;

use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_pages') {
                foreach($data['data'] as $value) {
                    $page = new Page();
                    $page->fill($value);
                    $page->save();
                }
                break;
            }
        }
    }
}