<?php

use Illuminate\Database\Seeder;

use App\Models\ProductCategory;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_product_categories') {
                foreach($data['data'] as $value) {
                    $productCategory = new ProductCategory();
                    $productCategory->fill($value);
                    $productCategory->save();
                }
                break;
            }
        }
    }
}
