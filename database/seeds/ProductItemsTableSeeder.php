<?php

use Illuminate\Database\Seeder;

use App\Models\ProductItem;

class ProductItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_product_items') {
                foreach($data['data'] as $value) {
                    $productItem = new ProductItem();
                    $productItem->fill($value);
                    $productItem->save();
                }
                break;
            }
        }
    }
}
