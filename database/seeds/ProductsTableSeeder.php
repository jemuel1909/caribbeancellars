<?php

use Illuminate\Database\Seeder;

use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_products') {
                foreach($data['data'] as $value) {
                    $product = new Product();
                    $product->fill($value);
                    $product->save();
                }
                break;
            }
        }
    }
}