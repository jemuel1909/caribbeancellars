<?php

use Illuminate\Database\Seeder;

use App\Models\QuantityType;

class QuantityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'name' => 'Case',
                'status_id' => 1,
            ),
            array(
                'id' => 2,
                'name' => 'Bottle',
                'status_id' => 1,
            ),
            array(
                'id' => 3,
                'name' => 'Box/Cnt',
                'status_id' => 1,
            ),
            array(
                'id' => 4,
                'name' => 'Piece',
                'status_id' => 1,
            ),
        );
    
        foreach ($data as $value) {
            $quantityType = new QuantityType();
            $quantityType->fill($value);
            $quantityType->save();
        }
    }
}