<?php

use Illuminate\Database\Seeder;

use App\Models\Role;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            array(
                'id'          => 1,
                'name'        => 'Administrator',
                'description' => 'Full access to create, edit, and update companies, and orders.Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.',
            ),
            array(
                'id'          => 5,
                'name'        => 'User',
                'description' => 'A standard user that can have a licence assigned to them. No administrative features.'
            )    
        );

        foreach($data as $value) {
            $role = new Role();
            $role->fill($value);
            $role->save();
        }

    }
}
