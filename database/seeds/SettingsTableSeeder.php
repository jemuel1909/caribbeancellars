<?php

use Illuminate\Database\Seeder;

use App\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_settings') {
                foreach($data['data'] as $value) {
                    $setting = new Setting();
                    $setting->fill($value);
                    $setting->save();
                }
                break;
            }
        }
    }
}