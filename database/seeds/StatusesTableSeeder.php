<?php

use Illuminate\Database\Seeder;

use App\Models\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'id'   => 1,
                'name' => 'Activated'
            ),
            array(
                'id'   => 2,
                'name' => 'Deactivated'
            ),
            array(
                'id'   => 3,
                'name' => 'Pending'
            ),
            array(
                'id'   => 4,
                'name' => 'Rejected'
            ),
            array(
                'id'   => 5,
                'name' => 'Completed'
            )
        );

        foreach($data as $value) {
            $status = new Status();
            $status->fill($value);
            $status->save();
        }
    }
}