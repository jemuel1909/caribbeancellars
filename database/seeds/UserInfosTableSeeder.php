<?php

use Illuminate\Database\Seeder;

use App\Models\UserInfo;

class UserInfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_user_infos') {
                foreach($data['data'] as $value) {
                    $userInfo = new UserInfo();
                    $userInfo->fill($value);
                    $userInfo->save();
                }
                break;
            }
        }
    }
}