<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(file_get_contents('storage/app/import/caribbeancellars.json'), true);
        foreach($json as $data) {
            if (isset($data['name']) && $data['name'] == 'store_users') {
                foreach($data['data'] as $value) {
                    $user = new User();
                    $user->fill($value);
                    $user->save();
                }
                break;
            }
        }
    }
}