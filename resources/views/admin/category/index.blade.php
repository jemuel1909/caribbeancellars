@extends('admin.layouts.admin')

@section('title', 'Products')

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration',
        'links' => [
            ['name' => 'Categories', 'route' => 'admin.categories', 'params' => []],
        ]
    ])

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-tag"></i>
                                </span>
                                <h3 class="m-portlet__head-text text-uppercase">
                                    Categories<small>List of all categories</small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ route('admin.categories.add') }}" class="btn btn-primary m-btn m-btn--air m-btn--custom">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>New Record</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item"></li>
                                <li class="m-portlet__nav-item">
                                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                            <i class="la la-ellipsis-h m--font-brand"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__section m-nav__section--first">
                                                                <span class="m-nav__section-text">Quick Actions</span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-delete"></i>
                                                                    <span class="m-nav__link-text">Delete Multiple Records</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <form method="post" action="#">
                        @csrf

                        <input type="hidden" value="{{ route('admin.categories') }}" id="url-categories">
                        <input type="hidden" value="{{ route('admin.categories.delete') }}" id="url-deletion">

                        <div class="m-portlet__body">

                            <!--begin: Search Form -->
                            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">
                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="m-input-icon m-input-icon--left">
                                                    <input type="text" class="form-control m-input" placeholder="Search..." id="m-general-search">
                                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                                        <span><i class="la la-search"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Search Form -->

                            <table class="m-datatable" id="html_table" width="100%">
                                <thead>
                                    <tr>
                                        <th title="ID">ID</th>
                                        <th title="Name">
                                            Name
                                        </th>
                                        <th title="Name">
                                            Parent
                                        </th>
                                        <th title="Name">
                                            Type
                                        </th>
                                        <th title="Date Modified">
                                            Date Modified
                                        </th>
                                        <th title="Action">
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>
                                                {{ $category->id }}
                                            </td>
                                            <td>
                                                {{ $category->name }}
                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                {{ $category->type }}
                                            </td>
                                            <td>
                                                {{ $category->updated_at }}
                                            </td>
                                            <td>
                                                <span style="overflow: visible; position: relative; width: 110px;">
                                                    <div class="dropdown ">
                                                        <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">
                                                            <i class="la la-ellipsis-h"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item deletion" href="javascript:;" data-id="{{ $category->id }}" data-name="{{ $category->name }}">
                                                            <i class="la la-trash"></i> Delete Record</a>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('admin.categories.edit', ['category_id' => $category->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></a>
                                                </span>
                                            </td>
                                        </tr>

                                        @if (isset($category->subCategories))

                                            @foreach ($category->subCategories as $subCategory)
                                                <tr>
                                                    <td>
                                                        {{ $subCategory->id }}
                                                    </td>
                                                    <td>
                                                        <span class="pl-3">{{ $subCategory->name }}</span>
                                                    </td>
                                                    <td>
                                                        {{ $category->name }}
                                                    </td>
                                                    <td>
                                                        {{ $subCategory->type }}
                                                    </td>
                                                    <td>
                                                        {{ $subCategory->updated_at }}
                                                    </td>
                                                    <td>
                                                        <span style="overflow: visible; position: relative; width: 110px;">
                                                            <div class="dropdown ">
                                                                <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">
                                                                    <i class="la la-ellipsis-h"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item deletion" href="javascript:;" data-id="{{ $subCategory->id }}" data-name="{{ $subCategory->name }}">
                                                                    <i class="la la-trash"></i> Delete Record</a>
                                                                </div>
                                                            </div>
                                                            <a href="{{ route('admin.categories.edit', ['category_id' => $subCategory->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></a>
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        @endif

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script>
        var Deletion = {
            init: function() {
                jQuery(document).on('click', '.deletion', function() {
                    url = $('#url-deletion').val();
                    _token = $('meta[name="csrf-token"]').attr('content');
                    id = $(this).data('id');
                    name = $(this).data('name');
                    console.log(url);
                    conf = confirm("Are you sure? Deleting " + name + " is cannot be undone. Data and Object will be deleted as well.");
                    if (conf) {
                        function createForm(action, method, input) {
                            'use strict';
                            var form;
                            form = $('<form />', {
                                action: action,
                                method: method,
                                style: 'display: none;'
                            });
                            if (typeof input !== 'undefined' && input !== null) {
                                $.each(input, function(name, value) {
                                    $('<input />', {
                                        type: 'hidden',
                                        name: name,
                                        value: value
                                    }).appendTo(form);
                                });
                            }
                            form.appendTo('body').submit();
                        }
                        createForm(url, 'POST', {
                            id: id,
                            _token: _token,
                            _method: 'DELETE'
                        });
                    } else {
                        return false;
                    }
                });
            }
        };

        jQuery(document).ready(function() {
            Deletion.init();
            $('.m-datatable').mDatatable({
                search: {
                    input: $("#generalSearch")
                },
                data: {
                    pageSize: 50,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
                }
            });
        });
    </script>

@endpush