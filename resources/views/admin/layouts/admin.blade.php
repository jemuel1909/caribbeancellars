<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>@yield('title') - Caribbean Cellars Admininstration</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
            WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
		<!--end::Web font -->

		<!--begin::Global Theme Styles -->
		<link href="{{ url('') }}/theme/admin/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="{{ url('') }}/theme/admin/assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
		<link href="{{ url('') }}/theme/admin/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="{{ url('') }}/theme/admin/assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--end::Page Vendors Styles -->
        <link rel="shortcut icon" href="{{ url('') }}/theme/admin/assets/demo/default/media/img/logo/favicon.ico" />
        
        @stack('css')

	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

            @include('admin.partials.header')
            
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				@include('admin.partials.aside_left')
                
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

                    @yield('content')

				</div>
			</div>
            <!-- end:: Body -->

            @include('admin.partials.footer')
            
		</div>
		<!-- end:: Page -->

		{{-- @include('admin.partials.quick_sidebar') --}}

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->

		{{-- @include('admin.partials.nav_sticky'); --}}

		<!--begin::Global Theme Bundle -->
		<script src="{{ url('') }}/theme/admin/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="{{ url('') }}/theme/admin/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->
        
        @stack('js')

	</body>

	<!-- end::Body -->
</html>