@extends('admin.layouts.admin')

@section('title', 'Orders')

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Orders', 'route' => 'admin.orders', 'params' => []],
            ['name' => $order->id, 'route' => 'admin.orders', 'params' => []],
        ]
    ])

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')

                <div class="m-portlet m-portlet--last m-portlet--responsive-mobile" id="main_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-trophy"></i>
                                </span>
                                <h3 class="m-portlet__head-text text-uppercase">
                                    Orders<small>List of all orders</small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ route('admin.orders.add') }}" class="btn btn-primary m-btn m-btn--air m-btn--custom">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>New Record</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item"></li>
                                <li class="m-portlet__nav-item">
                                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                            <i class="la la-ellipsis-h m--font-brand"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__section m-nav__section--first">
                                                                <span class="m-nav__section-text">Quick Actions</span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="javascript:;" class="m-nav__link" id="delete">
                                                                    <i class="m-nav__link-icon flaticon-delete"></i>
                                                                    <span class="m-nav__link-text">Delete Records</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="javascipt:;" class="m-nav__link" id="activate">
                                                                    <i class="m-nav__link-icon fa fa-eye"></i>
                                                                    <span class="m-nav__link-text">Activate Records</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="javacript:;" class="m-nav__link" id="deactivate">
                                                                    <i class="m-nav__link-icon fa fa-eye-slash"></i>
                                                                    <span class="m-nav__link-text">Deactivate Records</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="m-portlet__body">

                        <form method="post" id="form">

                            @csrf

                            <input type="hidden" value="{{ route('admin.orders') }}" id="url-orders">
                            <input type="hidden" value="{{ route('admin.orders.delete') }}" id="url-delete">
                            
                            {{-- <input type="hidden" value="{{ route('admin.countries.activate') }}" id="url-activate">
                            <input type="hidden" value="{{ route('admin.countries.deactivate') }}" id="url-deactivate"> --}}

                            <!--begin: Search Form -->
                            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">
                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label>Status:</label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <select class="form-control m-bootstrap-select" id="m-form-status">
                                                            <option value="">All</option>
                                                            <option value="1">Activated</option>
                                                            <option value="2">Deactivated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="m-input-icon m-input-icon--left">
                                                    <input type="text" class="form-control m-input" placeholder="Search..." id="m-general-search">
                                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                                        <span><i class="la la-search"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Search Form -->

                            <!--begin: Datatable -->
                            <div id="m-datatable-order"></div>
                            <!--end: Datatable -->

                        </form>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection

@push('js')

    <script>

        var Datatable = {
            init: function() {
                var t;
                t = $('#m-datatable-order').mDatatable({
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: $('#url-orders').val() + '/?dt=true',
                                method: 'GET',
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    params: {},
                                map: function(t) {
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data), e
                                }
                            }
                        },
                        pageSize: 20,
                        serverPaging: false,
                        serverFiltering: false,
                        serverSorting: false,
                    },
                    layout: {
                        scroll: false,
                        footer: false,
                    },
                    sortable: true,
                    pagination: true,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100, 200, 500]
                            }
                        }
                    },
                    search: {
                        input: $("#m-general-search")
                    },
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        textAlign: "left",
                        selector: { class: "m-checkbox--solid m-checkbox--brand"}
                    }, 
                    {
                        field: "order_id",
                        title: "Order #",
                        sortable: true,
                        template: function(t) {
                            return t.id
                        }
                    }, {
                        field: "first_name",
                        title: "First Name",
                        sortable: true,
                    }, {
                        field: "last_name",
                        title: "Last Name",
                        sortable: true,
                    }, {
                        field: "email",
                        title: "Email",
                        sortable: true,
                    }, {
                        field: "total_amount",
                        title: "Total Amount",
                        textAlign: "right",
                        sortable: true,
                        template: function(t) {
                            return '$' + t.total_amount
                        }
                    }, {
                        field: "status_id",
                        title: "Status",
                        template: function(t) {
                            var e = {
                                1: {
                                    title: "Activated",
                                    class: "m-badge--brand"
                                },
                                2: {
                                    title: "Deactivated",
                                    class: "m-badge--metal"
                                },
                                3: {
                                    title: "Pending",
                                    class: "m-badge--primary"
                                },
                                4: {
                                    title: "Rejected",
                                    class: "m-badge--danger"
                                },
                                5: {
                                    title: "Completed",
                                    class: "m-badge--accent"
                                },
                            };
                            t.status_id = (t.status_id == null) ? 2 : t.status_id;
                            return '<span class="m-badge ' + e[t.status_id].class + ' m-badge--wide">' + e[t.status_id].title + "</span>"
                        }
                    }, {
                        field: "ordered_at",
                        title: "Ordered At",
                        filterable: false,
                    }, {
                        field: "Actions",
                        width: 50,
                        title: "Actions",
                        sortable: false,
                        overflow: "visible",
                        textAlign: "right",
                        template: function(t, e, a) {
                            return '<a href="./orders/edit/'+ t.id +'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></a>'
                        }
                    }]
                }), $('#m-form-status').on('change', function() {
                    t.search($(this).val(), 'status_id')
                }), $('#m-form-status').selectpicker()
            }
        };

        var Delete = {
            init: function() {
                $("#delete").on("click", function(e){
                    e.preventDefault();
                    $('input[type="checkbox"]').attr('name','ids[]');
                    conf = confirm("Are you sure? Deleting record(s) cannot be undone. Data and Object will be deleted as well.");
                    if (conf) {
                        $('#form').attr('action', $('#url-delete').val()).submit();
                    }
                });
            }
        }

        var Activate = {
            init: function() {
                $("#activate").on("click", function(e){
                    e.preventDefault();
                    $('input[type="checkbox"]').attr('name','ids[]');
                    conf = confirm("Are you sure you want to activate the record(s).");
                    if (conf) {
                        $('#form').attr('action', $('#url-activate').val()).submit();
                    }
                });
            }
        }

        var Deactivate = {
            init: function() {
                $("#deactivate").on("click", function(e){
                    e.preventDefault();
                    $('input[type="checkbox"]').attr('name','ids[]');
                    conf = confirm("Are you sure you want to deactivate the record(s)");
                    if (conf) {
                        $('#form').attr('action', $('#url-deactivate').val()).submit();
                    }
                });
            }
        }

        jQuery(document).ready(function() {
            Datatable.init();
            Delete.init();
            Activate.init();
            Deactivate.init();
        });

    </script>

@endpush