@extends('admin.layouts.admin')

@section('title', "Order - {$order->id}")

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Orders', 'route' => 'admin.orders', 'params' => []],
            ['name' => $order->id, 'route' => 'admin.orders', 'params' => []],
        ]
    ])

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')

                <div class="m-portlet">
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="m-invoice-2">
                            <div class="m-invoice__wrapper">
                                <div class="m-invoice__head" style="background-image: url(../../assets/app/media/img//logos/bg-6.jpg);">
                                    <div class="m-invoice__container m-invoice__container--centered">
                                        <div class="m-invoice__logo">
                                            <a href="#">
                                                <h1>ORDER - {{ $order->id }}</h1>
                                            </a>
                                            <a href="#">
                                                <img src="{{ url('') }}/theme/admin/assets/img/logo/logo.png">
                                            </a>
                                        </div>
                                        <span class="m-invoice__desc">
                                            <span>P.O Box 3063, Lower Estate, Tortola</span>
                                            <span>Tel: 284-393-4470</span>
                                            <span>Fax: 284-393-4471</span>
                                        </span>
                                        <div class="m-invoice__items">
                                            <div class="m-invoice__item">
                                                <span class="m-invoice__subtitle text-uppercase">Name:</span>
                                                <span class="m-invoice__text">{{ $order->first_name }} {{ $order->last_name }}</span>
                                            </div>
                                            <div class="m-invoice__item">
                                                <span class="m-invoice__subtitle text-uppercase">Email:</span>
                                                <span class="m-invoice__text">{{ $order->email }}</span>
                                            </div>
                                            <div class="m-invoice__item">
                                                <span class="m-invoice__subtitle text-uppercase">Phone:</span>
                                                <span class="m-invoice__text">{{ $order->phone_no }}</span>
                                            </div>
                                        </div>
                                        <div class="m-invoice__items">
                                            <div class="m-invoice__item">
                                                <span class="m-invoice__subtitle text-uppercase">Delivery Location:</span>
                                                <span class="m-invoice__text">{{ $order->delivery_location }}</span>
                                            </div>
                                            @if (isset($order->delivery_date))
                                                <div class="m-invoice__item">
                                                    <span class="m-invoice__subtitle text-uppercase">Delivery Date:</span>
                                                    <span class="m-invoice__text">{{ date('m/d/Y', strtotime($order->delivery_date)) }}</span>
                                                </div>
                                            @endif
                                            @if (isset($order->delivery_time))
                                                <div class="m-invoice__item">
                                                    <span class="m-invoice__subtitle text-uppercase">Phone</span>
                                                    <span class="m-invoice__text">{{ date('h:i A', strtotime($order->delivery_time)) }}</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-invoice__body m-invoice__body--centered">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Item #</th>
                                                    <th class="text-left">Product</th>
                                                    <th>Unit</th>
                                                    <th>Quantity</th>
                                                    <th>SRP</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($order->orderItems->sortBy('sku') as $orderItem)
                                                    <tr>
                                                        <td>
                                                            {{ $orderItem->sku }}
                                                        </td>
                                                        <td class="text-left">
                                                            {{ $orderItem->name }}
                                                        </td>
                                                        <td>
                                                            {{ $orderItem->quantityType->name }}
                                                        </td>
                                                        <td>
                                                            {{ $orderItem->total_quantity }}
                                                        </td>
                                                        <td>
                                                            {{ number_format($orderItem->amount, 2) }}
                                                        </td>
                                                        <td class="m--font-danger">{{ number_format($orderItem->total_amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="m-invoice__footer">
                                    <div class="m-invoice__table  m-invoice__table--centered table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>ORDER NOTE</th>
                                                    <th>TOTAL AMOUNT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{!! $order->notes !!}</td>
                                                    <td class="m--font-danger">${{ number_format($orderItem->total_amount, 2) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>    
    </div>

@endsection

@push('js')

    <script>

      

    </script>

@endpush