<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true"
        data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
                <a href="{{ url('admin') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                            {{--  <span class="m-menu__link-badge">
                                <span class="m-badge m-badge--danger">
                                    2
                                </span>
                            </span>  --}}
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Components
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <li class="m-menu__item  m-menu__item" aria-haspopup="true">
                <a href="{{ route('admin.orders') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-cart"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Orders
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item" aria-haspopup="true">
                <a href="{{ route('admin.products') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-gift"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Products
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item" aria-haspopup="true">
                <a href="{{ route('admin.categories') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-tabs"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Categories
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item" aria-haspopup="true">
                <a href="{{ route('admin.quantity_type') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-interface-8"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quantity
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item" aria-haspopup="true">
                <a href="{{ route('admin.user') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                User
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item" aria-haspopup="true">
                <a href="{{ route('admin.settings') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-settings"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Settings
                            </span>
                        </span>
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->