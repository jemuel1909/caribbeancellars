
@extends('admin.layouts.admin')

@section('title', 'Edit Product')

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Product', 'route' => 'admin.products', 'params' => []],
            ['name' => 'Edit', 'route' => 'admin.products.edit', 'params' => ['product_id' => $product->id]],
        ]
    ])

    <div class="m-content">
        
        @include('admin.partials.alert')

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-tag"></i>
                        </span>
                        <h3 class="m-portlet__head-text text-uppercase">
                            Edit<small>Edit product</small>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">                        
                        <li class="m-portlet__nav-item">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                    <i class="la la-ellipsis-h m--font-brand"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
                                                        <span class="m-nav__section-text">Quick Actions</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-delete"></i>
                                                            <span class="m-nav__link-text">Delete Record</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--begin::Form-->

            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ route('admin.products.edit_submit', ['product_id' => $product->id]) }}">
                @csrf
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Product No.</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <input type="text" name="sku" value="{{ $product->sku }}" class="form-control m-input" placeholder="Enter product no.">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Name</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <input type="text" name="name" value="{{ $product->name }}" class="form-control m-input" placeholder="Enter product name">
                        </div>
                    </div>
                    
                    @foreach ($quantityTypes as $quantityType)
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 col-form-label">
                                Per {{ $quantityType->name }}: $
                            </label>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-3">

                                        @if ($product->productItems->isNotEmpty())

                                            @foreach($product->productItems as $productItem)

                                                @if ($productItem->quantity_type_id == $quantityType->id)
                                                    <input type="text" class="form-control m-input" placeholder="0.00" name="srp" value="{{ $productItem->srp }}">
                                                @endif

                                            @endforeach

                                        @else
                                            <input type="text" class="form-control m-input" placeholder="0.00" name="srp" value="">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Status</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <select name="status_id" class="form-control m-input m-bootstrap-select">
                                @foreach($statuses as $value)
                                    <option value="{{ $value->id }}" {{ ($value->id == $product->status_id) ? ' selected' : null }}>{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-12 m--align-right">
                                <button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">Save</button>
                                <span class="m--margin-left-10">or <a href="{{ route('admin.products') }}" class="m-link m--font-bold">Cancel</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>

@endsection

@push('js')




@endpush