
@extends('admin.layouts.admin')

@section('title', 'Edit Product')

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Product', 'route' => 'admin.products', 'params' => []],
            ['name' => 'Import', 'route' => 'admin.products.import', 'params' => []],
        ]
    ])

    <div class="m-content">
        
        @include('admin.partials.alert')

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-tag"></i>
                        </span>
                        <h3 class="m-portlet__head-text text-uppercase">
                            Import<small>import product</small>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.products.search_index') }}" class="btn btn-danger m-btn m-btn--air m-btn--custom">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Re-Index Products</span>
                                </span> 
                            </a>
                        </li>
                        <li class="m-portlet__nav-item">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                    <i class="la la-ellipsis-h m--font-brand"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
                                                        <span class="m-nav__section-text">Quick Actions</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-delete"></i>
                                                            <span class="m-nav__link-text">Delete Record</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--begin::Form-->

            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ route('admin.products.import_submit') }}" enctype="multipart/form-data">
                @csrf
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">CSV file</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <input type="file" name="csv" class="form-control m-input" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-12 m--align-right">
                                <button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">Save</button>
                                <span class="m--margin-left-10">or <a href="{{ route('admin.products') }}" class="m-link m--font-bold">Cancel</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>

@endsection

@push('js')

@endpush