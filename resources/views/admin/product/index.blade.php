@extends('admin.layouts.admin')

@section('title', 'Products')

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Products', 'route' => 'admin.products', 'params' => []],
        ]
    ])
    
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-tag"></i>
                                </span>
                                <h3 class="m-portlet__head-text text-uppercase">
                                    Products<small>List of all products</small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ route('admin.products.import') }}" class="btn btn-danger m-btn m-btn--air m-btn--custom">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Import Products</span>
                                        </span> 
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="{{ route('admin.products.add') }}" class="btn btn-primary m-btn m-btn--air m-btn--custom">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>New Record</span>
                                        </span> 
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item"></li>
                                <li class="m-portlet__nav-item">
                                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                            <i class="la la-ellipsis-h m--font-brand"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__section m-nav__section--first">
                                                                <span class="m-nav__section-text">Quick Actions</span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="javascript:;" class="m-nav__link" id="delete-multiple">
                                                                    <i class="m-nav__link-icon flaticon-delete"></i>
                                                                    <span class="m-nav__link-text">Delete Multiple Records</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <!--begin::Form-->
                    <form id="form" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" value="{{ route('admin.products') }}" id="url-products">
                        <input type="hidden" value="{{ route('admin.products.delete') }}" id="url-deletion">

                        <div class="m-portlet__body">
                            
                            <!--begin: Search Form -->
                            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">
                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label">
                                                        <label>Status:</label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <select class="form-control m-bootstrap-select" id="m-form-status">
                                                            <option value="">All</option>
                                                            <option value="1">Activated</option>
                                                            <option value="2">Deactivated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <div class="m-input-icon m-input-icon--left">
                                                    <input type="text" class="form-control m-input" placeholder="Search..." id="m-general-search">
                                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                                        <span><i class="la la-search"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Search Form -->

                            <!--begin: Datatable -->
                            <div class="m-datatable-product"></div>
                            <!--end: Datatable -->
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>    
    </div>

@endsection

@push('js')

    <script>
        var Datatable = {
            init: function() {
                var t;
                t = $('.m-datatable-product').mDatatable({
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: $('#url-products').val() + '?dt=true',
                                method: 'GET',
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    params: {},
                                map: function(t) {
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data), e
                                }
                            }
                        },
                        pageSize: 10,
                        serverPaging: false,
                        serverFiltering: false,
                        serverSorting: false,
                    },
                    layout: {
                        scroll: !1,
                        footer: !1
                    },
                    sortable: !0,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    search: {
                        input: $("#m-general-search")
                    },
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        textAlign: "left",
                        selector: { class: "m-checkbox--solid m-checkbox--brand"}
                    }, {
                        field: "sku",
                        title: "Item #",
                        sortable: true,
                        width: 45,
                    }, {
                        field: "name",
                        title: "Name",
                        sortable: true,
                    }, {
                        field: "status_id",
                        title: "Status",
                        width: 100,
                        template: function(t) {
                            var e = {
                                1: {
                                    title: "Activated",
                                    class: "m-badge--brand"
                                },
                                2: {
                                    title: "Deactivated",
                                    class: "m-badge--metal"
                                }
                            };
                            t.status_id = (t.status_id == null) ? 2 : t.status_id;
                            return '<span class="m-badge ' + e[t.status_id].class + ' m-badge--wide">' + e[t.status_id].title + "</span>"
                        }
                    }, {
                        field: "updated_at",
                        title: "Modified",
                        filterable: false,
                        width: 120,
                    }, {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: !1,
                        overflow: "visible",
                        template: function(t, e, a) {
                            return '<div class="dropdown ' + (a.getPageSize() - e <= 4 ? "dropup" : "") + '">'
                                    + '<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"><i class="la la-ellipsis-h"></i></a>'
                                    + '<div class="dropdown-menu dropdown-menu-right">'
                                        + '<a class="dropdown-item deletion" href="javascript:;" data-id="'+t.id+'" data-name="'+t.name+'"><i class="la la-trash"></i> Delete Record</a></div></div>'
                                    + '<a href="./products/edit/'+ t.id +'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></a>'
                        }
                    }]
                }), $('#m-form-status').on('change', function() {
                    t.search($(this).val(), 'status_id')
                }), $('#m-form-league').on('change', function() {
                    t.search($(this).val(), "league_id")
                }), $('#m-form-status, #m-form-league').selectpicker()
            }
        };

        var Deletion = {
            init: function() {
                jQuery(document).on('click', '.deletion', function() {
                    url = $('#url-deletion').val();
                    _token = $('meta[name="csrf-token"]').attr('content');
                    id = $(this).data('id');
                    name = $(this).data('name');
                    console.log(url);
                    conf = confirm("Are you sure? Deleting " + name + " is cannot be undone. Data and Object will be deleted as well.");
                    if (conf) {
                        function createForm(action, method, input) {
                            'use strict';
                            var form;
                            form = $('<form />', {
                                action: action,
                                method: method,
                                style: 'display: none;'
                            });
                            if (typeof input !== 'undefined' && input !== null) {
                                $.each(input, function (name, value) {
                                    $('<input />', {
                                        type: 'hidden',
                                        name: name,
                                        value: value
                                    }).appendTo(form);
                                });
                            }
                            form.appendTo('body').submit();
                        }
                        createForm(url, 'POST', {
                            id: id,
                            _token: _token,
                            _method: 'DELETE'
                        });
                    } else {
                        return false;
                    }
                });
            }
        };

        var DeleteMultiple = {
            init: function() {
                $("#delete-multiple").on("click", function(e){
                    e.preventDefault();
                    $('input[type="checkbox"]').attr('name','id[]');
                    conf = confirm("Are you sure? Deleting record(s) cannot be undone. Data and Object will be deleted as well.");
                    if (conf) {
                        $('#form').attr('action', $('#url-deletion').val()).submit();
                    };
                });
            }
        };
        
        jQuery(document).ready(function() {
            Datatable.init();
            Deletion.init();
            DeleteMultiple.init();
        });

    </script>

@endpush