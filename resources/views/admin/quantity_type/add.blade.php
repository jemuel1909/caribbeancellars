
@extends('admin.layouts.admin')

@section('css')
@endsection

@section('content')


    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ route('admin.dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="{{ route('admin.product') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Product
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Add
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')
                
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Add Product
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" name="product" method="post" action="{{ route('admin.product.add_submit') }}" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Product No:
                                            </label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control m-input" placeholder="Enter name" name="sku" value="" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Name:
                                            </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control m-input" placeholder="Enter name" name="name" value="" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Price Bottle: $
                                            </label>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-1">
                                                        <span class="m-switch m-switch--sm m-switch--icon">
                                                            <label>
                                                                <input type="checkbox" name="is_per_bottle" value="1">
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="text" class="form-control m-input" placeholder="0.00" name="srp_per_bottle" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Price Case: $
                                            </label>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-1">
                                                        <span class="m-switch m-switch--sm m-switch--icon">
                                                            <label>
                                                                <input type="checkbox" name="is_per_case" value="1">
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="text" class="form-control m-input" placeholder="0.00" name="srp_per_case" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Status:
                                            </label>
                                            <div class="col-lg-2">
                                                <select class="form-control m-input" name="status_id" required>
                                                    <option value="1">Activated</option>
                                                    <option value="2">Deactivated</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>

                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                    
                </div>
                <!--end::Portlet-->

            </div>
        </div>    
    </div>

@endsection

@section('js')
    <script>
        var Treeview = function () {

            var category = function () {
                $('#m_tree_category').jstree({
                    'plugins': ["wholerow", "checkbox", "types"],
                    'core': {
                        "themes" : {
                            "responsive": false
                        },    
                        'data': [{
                                "text": "Same but with checkboxes",
                                "children": [{
                                    "text": "initially selected",
                                    "state": {
                                        "selected": true
                                    }
                                }, {
                                    "text": "custom icon",
                                    "icon": "fa fa-warning m--font-danger"
                                }, {
                                    "text": "initially open",
                                    "icon" : "fa fa-folder m--font-default",
                                    "state": {
                                        "opened": true
                                    },
                                    "children": ["Another node"]
                                }, {
                                    "text": "custom icon",
                                    "icon": "fa fa-warning m--font-waring"
                                }, {
                                    "text": "disabled node",
                                    "icon": "fa fa-check m--font-success",
                                    "state": {
                                        "disabled": true
                                    }
                                }]
                            },
                            "And wholerow selection"
                        ]
                    },
                    "types" : {
                        "default" : {
                            "icon" : "fa fa-folder m--font-warning"
                        },
                        "file" : {
                            "icon" : "fa fa-file  m--font-warning"
                        }
                    },
                });
            }

        
            return {
                //main function to initiate the module
                init: function () {
                    category();
                }
            };
        }();

        jQuery(document).ready(function() {    
            Treeview.init();
        });
    </script>

@endsection