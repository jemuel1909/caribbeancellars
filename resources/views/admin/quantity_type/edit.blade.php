
@extends('admin.layouts.admin')

@section('css')
@endsection

@section('content')


    @include('admin.partials.breadcrumb')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')
                
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Product
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" name="product" method="post" action="{{ route('admin.product.edit_submit', ['product_id' => $product->id]) }}" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Product No:
                                            </label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control m-input" placeholder="Enter name" name="sku" value="{{ $product->sku }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Name:
                                            </label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control m-input" placeholder="Enter name" name="name" value="{{ $product->name }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Price Bottle: $
                                            </label>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-1">
                                                        <span class="m-switch m-switch--sm m-switch--icon">
                                                            <label>
                                                                <input type="checkbox" name="is_per_bottle" value="1" {{ ($product->is_per_bottle) ? ' checked="cheched"' : '' }}>
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="text" class="form-control m-input" placeholder="0.00" name="srp_per_bottle" value="{{ $product->srp_per_bottle }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Price Case: $
                                            </label>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-1">
                                                        <span class="m-switch m-switch--sm m-switch--icon">
                                                            <label>
                                                                <input type="checkbox" name="is_per_case" value="1" {{ ($product->is_per_case) ? ' checked="cheched"' : '' }}>
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="text" class="form-control m-input" placeholder="0.00" name="srp_per_case" value="{{ $product->srp_per_case }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Status:
                                            </label>
                                            <div class="col-lg-2">
                                                <select class="form-control m-input" name="status_id" required>
                                                    <option value="1" {{ (1 == $product->status_id) ? 'selected' : '' }}>Activated</option>
                                                    <option value="2" {{ (2 == $product->status_id) ? 'selected' : '' }}>Deactivated</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                    
                </div>
                <!--end::Portlet-->

            </div>
        </div>    
    </div>

@endsection

@section('js')
    
@endsection