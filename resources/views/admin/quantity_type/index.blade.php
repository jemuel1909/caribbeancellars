
@extends('admin.layouts.admin')

@section('css')
@endsection

@section('content')

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Quantity Type
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                View
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')
                
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Quantity Type
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="{{ route('admin.product.add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>
                                                Add Quantity Type
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <!--begin: Datatable -->
                        <table class="m-datatable" id="html_table" width="100%">
                            <thead>
                                <tr>
                                    <th title="ID">ID</th>
                                    <th title="Name">
                                        Name
                                    </th>
                                    <th title="Date Modified">
                                        Date Modified
                                    </th>
                                    <th title="Action">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($quantityTypes as $quantityType)
                                    <tr>
                                        <td>
                                            {{ $quantityType->id }}
                                        </td>
                                        <td>
                                            {{ $quantityType->name }}
                                        </td>
                                        <td>
                                            {{ $quantityType->created_at }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.quantity_type.edit', ['quantity_type_id' => $quantityType->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                                <i class="la la-edit"></i>
                                            </a>
                                            
                                            <a href="#!" data-url="{{ route('admin.quantity_type.delete', ['quantity_type_id' => $quantityType->id]) }}" data-name="{{ $quantityType->name }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete" title="delete">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable -->
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection

@section('js')

    <!--begin::Page Resources -->
    <script>
        $(document).ready(function() {
            $('.m-datatable').mDatatable({
                search : {input:$("#generalSearch")},
                data : {
                    pageSize : 30,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
                }
            });

            $(document).on('click', '.delete', function() {
                url  = $(this).data('url');
                name = $(this).data('name');
                conf = confirm("Are you sure? Deleting " + name + " is cannot be undone. Data and Object will be deleted as well.");
                if (conf) {
                    window.location = url;
                } else {
                    return false;
                }
            });
        });
    </script>
    <!--end::Page Resources -->

@endsection