
@extends('admin.layouts.admin')

@section('title', 'Edit Settings')

@push('css')

@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Settings', 'route' => 'admin.products', 'params' => []],
        ]
    ])

    <div class="m-content">
        
        @include('admin.partials.alert')

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-tag"></i>
                        </span>
                        <h3 class="m-portlet__head-text text-uppercase">
                            Edit<small>Edit Settings</small>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">                        
                        <li class="m-portlet__nav-item">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                    <i class="la la-ellipsis-h m--font-brand"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
                                                        <span class="m-nav__section-text">Quick Actions</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-delete"></i>
                                                            <span class="m-nav__link-text">Delete Record</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--begin::Form-->

            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ route('admin.settings.edit_submit') }}" enctype="multipart/form-data">
                @csrf
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Title</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <input type="text" name="title" value="{{ $setting->title }}" class="form-control m-input" placeholder="Enter site title.">
                            <span class="m-form__help">
                                Site name or a title.
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Header Description</label>
                        <div class="col-lg-8">
                            <textarea class="form-control m-input summernote" name="description" row="3">{{ $setting->description }}</textarea>
                            <span class="m-form__help">
                                Header Description
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Header Image</label>
                        <div class="col-lg-4">
                            <input type="file" name="header_image" class="form-control m-input" placeholder="">
                            <span class="m-form__help">
                                Header Image
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Email</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <input type="email" name="email" value="{{ $setting->email }}" class="form-control m-input" placeholder="Enter email" required>
                            <span class="m-form__help">
                                Email address who will be received order and contact us notification.
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Additional Emails</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <textarea class="form-control m-input" name="additional_emails" row="3">{{ $setting->additional_emails }}</textarea>
                            <span class="m-form__help">
                                Any additional emails you want to receive the alert email, in addition to the main store email. (comma separated).
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Pricelist</label>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <input type="file" name="pricelist" class="form-control m-input" placeholder="">
                            <span class="m-form__help">
                                Upload pricelist PDF.
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">FAQ</label>
                        <div class="col-lg-8">
                            <textarea class="form-control m-input summernote" name="page[description]" row="3">{{ $page->description }}</textarea>
                            <span class="m-form__help">
                                FAQ content
                            </span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Pop Up Image</label>
                        <div class="col-lg-3">
                            <div class="row mb-3">
                                <div class="col-lg-12">
                                    <input type="file" name="pop_up_image" class="form-control m-input" placeholder="">
                                    <span class="m-form__help">
                                        Homepage Marketing Popup Image
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" name="pop_up_url" value="{{ $setting->pop_up_url }}" class="form-control m-input" placeholder="Enter url">
                                    <span class="m-form__help">
                                        Enter url link e.g. https://shop.caribbeancellars.com/search/q/becks
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <span class="m-switch m-switch--icon">
                                <label>
                                    <input type="checkbox" name="has_pop_up" {{ ($setting->has_pop_up) ? 'checked' : null }}  value="1">
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-2">
                            <img class="w-50" src="{{ url('') }}/upload/popup/popup.jpg" onerror="this.onerror=null;this.src='{{ url('') }}/no-image.svg';">
                        </div>                        
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Banner (Top)</label>
                        <div class="col-lg-3">
                            <div class="row mb-3">
                                <div class="col-lg-12">
                                    <input type="file" name="banner_top_image" class="form-control m-input" placeholder="">
                                    <span class="m-form__help">
                                        Banner / Flyer at the top of the page, recommended size is wide 728x90
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" name="banner_top_url" value="{{ $setting->banner_top_url }}" class="form-control m-input" placeholder="Enter url">
                                    <span class="m-form__help">
                                        Enter url link. e.g. https://shop.caribbeancellars.com/search/q/becks
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <span class="m-switch m-switch--icon">
                                <label>
                                    <input type="checkbox" name="has_banner_top" {{ ($setting->has_banner_top) ? 'checked' : null }}  value="1">
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-5">
                            <img class="w-50" src="{{ url('') }}/upload/banner/top.jpg" onerror="this.onerror=null;this.src='{{ url('') }}/no-image.svg';">
                        </div>                        
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Banner (Left)</label>
                        <div class="col-lg-3">
                            <div class="row mb-3">
                                <div class="col-lg-12">
                                    <input type="file" name="banner_left_image" class="form-control m-input" placeholder="">
                                    <span class="m-form__help">
                                        Banner / Flyer at the left of the page, recommended size is wide skycrapper 160x600
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" name="banner_left_url" value="{{ $setting->banner_left_url }}" class="form-control m-input" placeholder="Enter url">
                                    <span class="m-form__help">
                                        Enter url link e.g. https://shop.caribbeancellars.com/search/q/becks
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <span class="m-switch m-switch--icon">
                                <label>
                                    <input type="checkbox" name="has_banner_left" {{ ($setting->has_banner_left) ? 'checked' : null }}  value="1">
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-2">
                            <img class="w-50" src="{{ url('') }}/upload/banner/left.jpg" onerror="this.onerror=null;this.src='{{ url('') }}/no-image.svg';">
                        </div>                        
                    </div>

                </div>
                
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-12 m--align-right">
                                <button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">Save</button>
                                <span class="m--margin-left-10">or <a href="{{ route('admin.settings') }}" class="m-link m--font-bold">Cancel</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>

@endsection

@push('js')

    <script>
            
        var BootstrapSelect = {
            init: function() {
                $(".m-selectpicker").selectpicker()
            }
        };

        var SummerNote = {
            init: function() {
                $(".summernote").summernote({
                    height: 250
                })
            }
        };

        jQuery(document).ready(function() {
            BootstrapSelect.init();
            SummerNote.init()
        });

    </script>

@endpush