
@extends('admin.layouts.admin')

@section('css')
@endsection

@section('content')

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('admin/dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                User
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Add
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')
                
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Add User
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" name="user" method="post" action="{{ url('') }}/admin/user/add-submit" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Username:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter username" name="username" value="{{ old('username') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Email:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="email" class="form-control m-input" placeholder="Enter email" name="email" value="{{ old('email') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Retype Email:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="email" class="form-control m-input" placeholder="Enter email" name="email_confirmation" value="" required>
                                                <span class="m-form__help">
                                                    Please retype the email.
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Password:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control m-input" placeholder="Enter password" name="password" value="" required>
                                                <span class="m-form__help">
                                                    Atleast 6 characters.
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Retype Password:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control m-input" placeholder="Re-enter password" name="password_confirmation" value="" required>
                                                <span class="m-form__help">
                                                    Please retype the password.
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Role:
                                            </label>
                                            <div class="col-lg-3">
                                                <select class="form-control m-input" name="role_id" required>
                                                    <option value="1" {{ (1 == old('role_id')) ? 'selected' : '' }}>Admin</option>
                                                    <option value="5" {{ (5 == old('role_id')) ? 'selected' : '' }}>User</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Status:
                                            </label>
                                            <div class="col-lg-3">
                                                <select class="form-control m-input" name="status_id" required>
                                                    <option value="1" {{ (1 == old('status_id')) ? 'selected' : '' }}>Activated</option>
                                                    <option value="2" {{ (2 == old('status_id')) ? 'selected' : '' }}>Deactivated</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="m-form__seperator m-form__seperator--dashed"></div>
                            <div class="m-form__section m-form__section">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                First Name:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter first name" name="first_name" value="{{ old('first_name') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Last Name:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter last name" name="last_name" value="{{ old('last_name') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Address:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter address" name="address" value="{{ old('address') }}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                City:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter city" name="city" value="{{ old('city') }}">
                                                <span class="m-form__help">
                                                    City/County/Town.
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                State:
                                            </label>
                                            <div class="col-lg-6">
                                                <select class="form-control m-input" name="state_id">
                                                    <option value="">Select State</option>
                                                    @foreach($states as $state)
                                                    <option value="{{ $state->state_id }}" {{ ($state->state_id == old('state_id')) ? 'selected' : '' }}>{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Zip Code:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter zip code" name="zip_code" value="{{ old('zip_code') }}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Phone:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter phone" name="phone_no" value="{{ old('phone_no') }}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Website:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="url" class="form-control m-input" placeholder="Enter website" name="website" value="{{ old('website') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Add User
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->

                </div>
                <!--end::Portlet-->

            </div>
        </div>    
    </div>

@endsection

@section('js')

@endsection