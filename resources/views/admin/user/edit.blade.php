
@extends('admin.layouts.admin')

@section('css')
@endsection

@section('content')

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('admin/dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="{{ url('admin/user') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                User
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                {{ $user->username }}
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')
                
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Password
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" name="user" method="post" action="{{ url('') }}/admin/user/edit-password-submit/{{ $user->user_id }}" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Username:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter username" value="{{ $user->username }}" disabled>
                                                <span class="m-form__help">
                                                    Username can't be edited.
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Password:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control m-input" placeholder="Enter password" name="password" value="">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Retype Password:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control m-input" placeholder="Re-enter password"  name="password_confirmation" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                    
                </div>
                <!--end::Portlet-->

                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Info
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" name="user_info" method="post" action="{{ route('admin.user.edit_submit', ['user_id' => $user->id]) }}" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Status:
                                            </label>
                                            <div class="col-lg-3">
                                                
                                                <select class="form-control m-input" name="status_id" required>
                                                    <option value="1" {{ (1 == $user->status_id) ? 'selected' : '' }}>Activated</option>
                                                    <option value="2" {{ (2 == $user->status_id) ? 'selected' : '' }}>Deactivated</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Email:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter email" name="email" value="{{ $user->email }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                First Name:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter first name" name="first_name" value="{{ $user->first_name }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Last Name:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter last name" name="last_name" value="{{ $user->last_name }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Address:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter address" name="address" value="{{ $userInfo->address }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                City:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter city" name="city" value="{{ $userInfo->city }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                State:
                                            </label>
                                            <div class="col-lg-3">
                                                <select class="form-control m-input" name="state_id" required>
                                                    <option value="">Select State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" {{ ($state->id == $userInfo->state_id) ? 'selected' : '' }}>{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Zip Code:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter zip code" name="zip_code" value="{{ $userInfo->zip_code }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Phone:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control m-input" placeholder="Enter phone" name="phone_no" value="{{ $userInfo->phone_no }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-lg-3 col-form-label">
                                                Website:
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="url" class="form-control m-input" placeholder="Enter website" name="website" value="{{ $userInfo->website }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                    
                </div>
                <!--end::Portlet-->

            </div>
        </div>    
    </div>

@endsection

@section('js')

@endsection