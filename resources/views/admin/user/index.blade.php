
@extends('admin.layouts.admin')

@push('css')
@endpush

@section('content')

    @include('admin.partials.breadcrumb', [
        'title' => 'Administration', 
        'links' => [
            ['name' => 'Users', 'route' => 'admin.user', 'params' => []],
        ]
    ])

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                @include('admin.partials.alert')
                
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    User
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="{{ route('admin.user.add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>
                                                Add User
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <!--begin: Datatable -->
                        <table class="m-datatable" id="html_table" width="100%">
                            <thead>
                                <tr>
                                    <th title="ID">ID</th>
                                    <th title="Username">Username</th>
                                    <th title="First Name">
                                        First Name
                                    </th>
                                    <th title="Last Name">
                                        Last Name
                                    </th>
                                    <th title="Role">
                                        Role
                                    </th>
                                    <th title="Status">
                                        Status
                                    </th>
                                    <th title="Date Modified">
                                        Date Modified
                                    </th>
                                    <th title="Action">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($users as $user)
                                <tr>
                                    <td>
                                        {{ $user->id }}
                                    </td>
                                    <td>
                                        {{ $user->username }}
                                    </td>
                                    <td>
                                        {{ $user->first_name }}
                                    </td>
                                    <td>
                                        {{ $user->last_name }}
                                    </td>
                                    <td>
                                        
                                        @if ($user->role_id == 1)
                                            admin
                                        @else
                                            user
                                        @endif

                                    </td>
                                    <td>
                                    
                                        @if ($user->status_id == 1)
                                            <span style="width: 132px;"><span class="m-badge m-badge--brand m-badge--wide">Activated</span></span>
                                        @else
                                            <span style="width: 132px;"><span class="m-badge m-badge--secondary m-badge--wide">Deactivated</span></span>
                                        @endif

                                    </td>
                                    <td>
                                        {{ $user->created_at }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.user.edit', ['user_id' => $user->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="#!" data-url="{{ url('') }}/admin/user/delete/{{ $user->id }}" data-name="{{ $user->username }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete" title="delete">
                                            <i class="la la-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable -->
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection

@push('js')

    <!--begin::Page Resources -->
    <script>
        $(document).ready(function() {
            $('.m-datatable').mDatatable({
                search : {input:$("#generalSearch")},
                data : {
                    pageSize : 30,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
                }
            });

            $(document).on('click', '.delete', function() {
                url  = $(this).data('url');
                name = $(this).data('name');
                conf = confirm("Are you sure? Deleting " + name + " is cannot be undone. Data and Object will be deleted as well.");
                if (conf) {
                    window.location = url;
                } else {
                    return false;
                }
            });
        });
    </script>
    <!--end::Page Resources -->

@endpush