
@extends('email.layouts.email')

@section('title', 'Registration')

@section('content')

    <div style="background-color:#f8f8f8;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #fff;" class="block-grid">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#fff;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:#f8f8f8;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:#fff;"><![endif]-->

                <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:600px; padding-right: 30px; padding-left: 30px; padding-top:40px; padding-bottom:40px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                <div class="num12 col" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:40px; padding-bottom:40px; padding-right: 30px; padding-left: 30px;">
                            <!--<![endif]-->

                            
                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;"><![endif]-->
                                <div style="color:#555555;line-height:180%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;">
                                    <div style="font-size:12px;line-height:22px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 12px;line-height: 22px"><span style="font-size: 18px; line-height: 32px;">Hi Admin</span>,<br>
                                            <span style="font-size: 14px; line-height: 25px;">
                                            You have new message! From contact us form.
                                        </p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>

                            <hr style="border: 1px solid #efefef">
                            <div class="" style="color:#555555; line-height:180%; font-size: 14px; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 5px 10px;">
                                <table width="100%" style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 15px;">
                                    <tr>
                                        <td width="30%">
                                            <strong>Name:</strong> 
                                        </td>
                                        <td width="70%">
                                            {{ $data['name'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Email:</strong>
                                        </td>
                                        <td>
                                            {{ $data['email'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Subject:</strong>
                                        </td>
                                        <td>
                                            {{ $data['subject'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Phone:</strong>
                                        </td>
                                        <td>
                                            {{ $data['phone'] }}
                                        </td>
                                    </tr>    
                                    <tr>
                                        <td>
                                            <strong>Message:</strong>
                                        </td>
                                        <td>
                                            {{ $data['message'] }}
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <!--[if (!mso)&(!IE)]><!-->
                        </div>
                        <!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
@endsection