
@extends('email.layouts.order')

@section('title', 'New Order Created')

@section('content')

    <div style="background-color:#f8f8f8;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 800px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #fff;" class="block-grid">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#fff;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:#f8f8f8;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 800px;"><tr class="layout-full-width" style="background-color:#fff;"><![endif]-->

                <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:800px; padding-right: 30px; padding-left: 30px; padding-top:40px; padding-bottom:40px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                <div class="num12 col" style="min-width: 320px;max-width: 800px;display: table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:40px; padding-bottom:40px; padding-right: 30px; padding-left: 30px;">
                            <!--<![endif]-->

                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;"><![endif]-->
                                <div style="color:#555555;line-height:180%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;">
                                    <div style="font-size:12px;line-height:22px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 12px;line-height: 22px"><span style="font-size: 18px; line-height: 32px;">Hi Admin</span>,<br>
                                        <span style="font-size: 14px; line-height: 25px;">
                                        A new order was placed in your store.<br>
                                        Order #: <span style="color: #862a41; font-weight: bold;">{{ $order->id }}</span></span></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>

                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;"><![endif]-->
                                <div style="color:#555555; line-height:180%; font-size: 12px; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 5px 10px;">
                                    <hr style="border: 1px solid #efefef">
                                    <table width="100%" style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 15px;">
                                        <tr>
                                            <td width="30%">
                                                <strong>First Name:</strong> 
                                            </td>
                                            <td width="70%">
                                                {{ $order->first_name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Last Name:</strong>
                                            </td>
                                            <td>
                                                {{ $order->last_name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Email:</strong>
                                            </td>
                                            <td>
                                                {{ $order->email }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Phone:</strong>
                                            </td>
                                            <td>
                                                {{ $order->phone_no }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Delivery Location:</strong>
                                            </td>
                                            <td>
                                                {{ $order->delivery_location }}
                                            </td>
                                        </tr>
                                        @if (isset($order->delivery_date))

                                            <tr>
                                                <td>
                                                    <strong>Delivery Date:</strong><small>mm/dd/yyyy</small>
                                                </td>
                                                <td>
                                                    {{ date('m/d/Y', strtotime($order->delivery_date)) }}
                                                </td>
                                            </tr>
                                        
                                        @endif

                                        @if (isset($order->delivery_time))

                                            <tr>
                                                <td>
                                                    <strong>Delivery Time:</strong>
                                                </td>
                                                <td>
                                                    {{ date('h:i A', strtotime($order->delivery_time)) }}
                                                </td>
                                            </tr>
                                        
                                        @endif
                                       
                                    </table>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>
                            
                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;"><![endif]-->
                                <div style="color:#555555; line-height:180%; font-size: 12px; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 5px 10px;">
                                    <hr style="border: 1px solid #efefef">
                                    <table width="100%" style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 15px;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: left; color: #862a41; font-weight: bold; width:15%">Item #</th>
                                                <th style="text-align: left; color: #862a41; font-weight: bold; width:30%">Product</th>
                                                <th style="text-align: left; color: #862a41; font-weight: bold; width:10%">Unit</th>
                                                <th style="text-align: right; color: #862a41; font-weight: bold; width:10%">Quantity</th>
                                                <th style="text-align: right; color: #862a41; font-weight: bold; width:15%">SRP</th>
                                                <th style="text-align: right; color: #862a41; font-weight: bold; width:20%">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($order->orderItems->sortBy('sku') as $orderItem)
                                                <tr>
                                                    <td>
                                                        <small>{{ $orderItem->sku }}</small>
                                                    </td>
                                                    <td>
                                                        {{ $orderItem->name }}<br/>
                                                    </td>
                                                    <td>
                                                        <small>{{ $orderItem->quantityType->name }}</small>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <small>{{ $orderItem->total_quantity }}</small>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        ${{ number_format($orderItem->amount, 2) }}
                                                    </td>
                                                    <td style="text-align: right;">${{ number_format($orderItem->total_amount, 2) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" style="font-weight:bold; color: #862a41; text-align: right; border-top: 2px dotted #efefef; padding-top:5px">Total Amount</td>
                                                <td style="font-weight:bold; color:  #862a41; text-align: right; border-top: 2px dotted #efefef; padding-top:5px">${{ number_format($order->total_amount, 2) }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <hr style="border: 1px solid #efefef">
                                    <div style="color:#555555;">
                                        @if ($order->notes != '')
                                            <p><strong>Order note: </strong></p>
                                            <div style="padding: 5px 10px; border-left: 2px solid #862a41;">
                                                {!! $order->notes !!}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>
                            
                            <div class="">
                                <div class="button-container left" style="padding-right: 0px; padding-left: 0px; padding-top:25px; padding-bottom:25px;">
                                    <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:25px;" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ route('admin.orders.edit', ['order_id' => $order->id]) }}" style="height:36pt; v-text-anchor:middle; width:156pt;" arcsize="9%" strokecolor="#26B4FF" fillcolor="#26B4FF"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:16px;"><![endif]-->
                                    <a href="{{ route('admin.orders.edit', ['order_id' => $order->id]) }}" target="_blank" style="display: block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #ffffff; background-color: #26B4FF; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 209px; width: 169px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 8px; padding-right: 20px; padding-bottom: 8px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;mso-border-alt: none">
                                        <span style="font-size:16px;line-height:32px;"><span style="font-size: 14px; line-height: 28px;" data-mce-style="font-size: 14px;">View Order</span></span>
                                    </a>
                                    <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                                </div>

                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 15px;"><![endif]-->
                                <div style="color:#555555; line-height:100%; font-size: 12px; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 5px 10px;">
                                    <p style="color:#862a41">
                                        <small>
                                            <strong>
                                            * Prices are subjected to change without notice.
                                            <br>
                                            For all future orders, the process will begin five (5) days prior to delivery date to ensure stock availability. Out of stocks will be communicated before processing.
                                            </strong>
                                        </small>
                                    </p>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                                                            
                            </div>

                            <!--[if (!mso)&(!IE)]><!-->
                        </div>
                        <!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
@endsection