<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 800px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f2f2f2;" class="block-grid">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#f2f2f2;">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 800px;"><tr class="layout-full-width" style="background-color:#f2f2f2;"><![endif]-->

            <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:800px; padding-right: 30px; padding-left: 30px; padding-top:25px; padding-bottom:25px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <div class="num12 col" style="min-width: 320px;max-width: 800px;display: table-cell;vertical-align: top;">
                <div style="background-color: transparent; width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 30px; padding-left: 30px;">
                        <!--<![endif]-->

                        <div class="">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;"><![endif]-->
                            <div style="color:#bbbbbb;line-height:150%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">
                                <div style="font-size:12px;line-height:18px;color:#bbbbbb;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                    <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center"><span style="font-size: 12px; line-height: 18px;">{{ env('APP_NAME') }}&#160;©</span></p>
                                    <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center"><span style="font-size: 12px; line-height: 18px;"><a style="color:#bbbbbb;color:#bbbbbb;text-decoration: underline;" href="{{ env('APP_URL') }}" target="_blank" rel="noopener">{{ env('APP_URL') }}</a></span></p>
                                </div>
                            </div>
                            <!--[if mso]></td></tr></table><![endif]-->
                        </div>
                        
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>