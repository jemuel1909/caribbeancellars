<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 800px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f8f8f8;" class="block-grid">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#f8f8f8;">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 800px;"><tr class="layout-full-width" style="background-color:#f8f8f8;"><![endif]-->
                
            <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:800px; padding-right: 0px; padding-left: 0px; padding-top:50px; padding-bottom:30px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 2px solid #26B4FF; border-right: 0px solid transparent;" valign="top"><![endif]-->
            <div class="num12 col" style="min-width: 320px;max-width: 800px;display: table-cell;vertical-align: top;">
                <div style="background-color: transparent; width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 2px solid #26B4FF; border-right: 0px solid transparent; padding-top:50px; padding-bottom:30px; padding-right: 0px; padding-left: 0px;">
                        <!--<![endif]-->

                        <div align="left" class="img-container left autowidth" style="padding-right: 0px;  padding-left: 0px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px;line-height:0px;"><td style="padding-right: 0px; padding-left: 0px;" align="left"><![endif]-->
                            <img class="left autowidth" align="left" border="0" src="{{ url('') }}/theme/home/assets/img/logo/logo.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 158px" width="158">
                            <!--[if mso]></td></tr></table><![endif]-->
                        </div>

                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>