
@extends('home.layouts.home')

@section('title', 'Home')

@push('css')

    <!-- CSS Implementing Plugins -->
    {{-- <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/animate.css"> --}}
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/custombox/custombox.min.css">

@endpush

@section('content')

    <!-- Promo Block -->
    <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options='{direction: "fromtop", animation_duration: 25, direction: "reverse"}'>
        <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center g-bg-cover g-bg-black-opacity-0_1--after" style="height: 100%; background-image: url({{ url('') }}/theme/home/assets/img/banner/banner.jpg);"></div>
        <div class="container g-color-white g-pt-40 g-pb-20">
            
            {!! $setting->description !!}
            
            {{-- <div class="g-mb-20">
                <span class="d-block g-color-white-opacity-0_8 g-font-weight-300 g-font-size-20">From our Cellars</span>
                <h3 class="g-color-white g-font-size-50 g-font-size-60--md g-line-height-1_2 mb-0">To Yours</h3>
                <p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Shop Now!</p>
            </div> --}}

            <div class="d-flex justify-content-end">
                <ul class="u-list-inline g-bg-gray-dark-v1 g-font-weight-300 g-rounded-50 g-py-5 g-px-20">
                    <li class="list-inline-item g-mr-5">
                        <a class="u-link-v5 g-color-white g-color-primary--hover" href="#!">Home</a>
                        <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
                    </li>
                    <li class="list-inline-item g-mr-5 g-color-primary g-font-weight-400">
                        <span>Shop</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End Promo Block -->

    <!-- Products -->
    <div class="container">
        <div class="row">

            @include('home.partials.left_sidebar', ['categories' => $categories, 'categoryId' => 0, 'categoryParentId' => 0])
            
            <!-- Content -->
            <div class="col-md-9 order-md-2">

                {{-- Banner Left --}}
                @if ($setting->has_banner_top)
                    <div class="g-mt-30 text-center">
                        <a href="{{ $setting->banner_top_url ?? 'javascript:;' }}">
                            <img class="img-fluid" src="{{ url('') }}/upload/banner/top.jpg" alt="Image Description">
                        </a>
                    </div>
                @endif

                <div class="row justify-content-end g-pt-50 g-mb-20 g-mb-0--md">  
                    <div class="col-md-7 g-mb-10">
                        @include('home.partials.search')
                    </div>
                </div>
                <div class="g-pl-15--lg">
                    <div class="row justify-content-end">
                        <div class="col-md-7 text-right">
                            <span class="g-font-weight-400 font-italic">*Prices are subject to change.</span>
                        </div>
                    </div>
                    
                    {{-- Product --}}
                    <div class="table-responsive g-pt-10 g-pb-50">
                        <input type="hidden" value="{{ $setting->has_pop_up }}" id="has-popup">
                        <input type="hidden" value="{{ (isset($_COOKIE['popup_product'])) ? true : false }}" id="popup-product-status">

                        <table class="table table-bordered u-table--v2">
                            <thead class="text-uppercase g-letter-spacing-1">
                                <tr>
                                    {{-- <th class="g-font-weight-300 g-color-black">#</th> --}}
                                    <th class="g-font-weight-300 g-color-black">Product</th>
                                    <th class="g-font-weight-300 g-color-black">Unit</th>
                                    <th class="g-font-weight-300 g-color-black">Quantity</th>
                                    <th class="g-font-weight-300 g-color-black">Total Amount</th>
                                    <th class="g-font-weight-300 g-color-black"></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($products as $product)
                                    <tr>
                                        {{-- <td class="align-middle">
                                            {{ $product->sku }}
                                        </td> --}}
                                        <td class="align-middle">
                                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
                                                <li>#{{ $product->sku }}</li>
                                            </ul>
                                            {{ $product->name }}
                                        </td>
                                        <td class="align-middle">
                                            <select id="srp_{{ $product->id }}" class="js-custom-select w-150 u-select-v2 u-shadow-v32 g-brd-none g-color-black g-color-primary--hover g-bg-white text-left" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" onchange="totalAmount('{{ $product->id }}')">
                                                {{-- <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active" value="0" data-srp="0">- Select -</option> --}}
                                                
                                                @foreach($product->productItems as $productItem)

                                                    <option class="g-brd-secondary-light-v2 g-color-black g-color-white--hover g-bg-white g-bg-primary--hover g-brd-1" value="{{ $productItem->quantity_type_id }}" data-srp="{{ $productItem->srp }}">{{ $productItem->quantityType->name }} : ${{ @number_format($productItem->srp, 2) }}</option>
                                                    
                                                @endforeach
                                            
                                            </select>
                                        </td>
                                        <td class="align-middle">
                                            <div class="form-group">
                                                <div class="input-group u-quantity-v1 g-width-120 g-brd-primary--focus">
                                                    <div class="js-minus input-group-prepend g-width-25 g-color-gray g-bg-grey-light-v3" onclick="totalAmount('{{ $product->id }}', -1)">
                                                        <span class="input-group-text rounded-0 w-100"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <input class="form-control text-center rounded-0 g-pa-5" type="text" value="0" id="quantity_{{ $product->id }}" onchange="totalAmount('{{ $product->id }}')">
                                                    <div class="js-plus input-group-append g-width-25 g-color-gray g-bg-grey-light-v3" onclick="totalAmount('{{ $product->id }}', 1)">
                                                        <span class="input-group-text rounded-0 w-100"><i class="fa fa-plus"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle">
                                            <span id="total_amount_{{ $product->id }}">$0.00</span>
                                        </td>

                                        <td class="align-middle text-center">
                                            <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-10 g-px-20" href="#!" onclick="putOrder({{ $product->id }})">
                                                Add To Cart: <i class="ml-2 icon-finance-100 u-line-icon-pro"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <nav class="g-mb-50" aria-label="Page Navigation">
                        {{ $products->links('home.partials.paginator') }}
                    </nav>
 
                </div>
            </div>
            <!-- End Content -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="modal-type-aftersometime" class="js-autonomous-popup text-left g-max-width-600 g-bg-white g-overflow-y-auto g-pa-20" style="display:none;" data-modal-type="aftersometime" data-effect="fadein">
                    <button type="button" class="close" onclick="Custombox.modal.close();">
                        <i class="hs-icon hs-icon-close"></i>
                    </button>
                    <p class="mt-5">
                        <a href="{{ $setting->pop_up_url ?? 'javascript:;' }}">
                            <img src="{{ url('') }}/upload/popup/popup.jpg" class="mx-auto d-block w-100">
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Products -->
   
    {{-- @include('home.partials.call_to_action') --}}

@endsection

@push('js')

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widget.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.jquery.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/appear.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/custombox/custombox.min.js"></script>
    
    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.scrollbar.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.rating.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.slider.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.go-to.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.count-qty.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.select.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.modal-window.js"></script>
    
    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            getTally();

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of HSScrollBar component
            $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of rating
            $.HSCore.helpers.HSRating.init();
            
            // initialization of custom select
            $.HSCore.components.HSSelect.init('.js-custom-select');

            // initialization of range slider
            $.HSCore.components.HSSlider.init('#rangeSlider1');
            
            // initialization of forms
            $.HSCore.components.HSCountQty.init('.js-quantity');
            
        });
    </script>
    <script>
    
        function totalAmount(productId, cnt)
        {   
            // name = $(this).data('name');
            var quantity = $('#quantity_' + productId).val();
            var quantity = parseInt(quantity) + parseInt(cnt);
            
            if (quantity >= 0) {
                quantityTypeId = $('#srp_' + productId).val();
                srp = $('#srp_' + productId).find(':selected').data('srp');
                
                // alert(srp);
                total = quantity * srp;
                $('#total_amount_' + productId).html('$' + total.toFixed(2));
                $('#quantity_' + productId).val(quantity);
            }
        }
        
        function putOrder(productId)
        {   
            quantity = $('#quantity_' + productId).val();
            quantityTypeId = $('#srp_' + productId).val();
            
            if (quantityTypeId != 0 && quantity != 0) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                
                $.post('{{ route('order.put') }}', { product_id: productId, quantity_type_id: quantityTypeId, quantity: quantity })
                .done(function(data) {
                    
                    alert('Product has been added to cart.');
                    
                    total = 0;
                    $('#total_amount_' + productId).html('$' + total.toFixed(2));
                    $('#quantity_' + productId).val(0);

                    getTally();
                });
            } else {
                alert('Select Unit and quantity');
            }
        }

        function getTally()
        {
            $.getJSON('{{ route('order.tally') }}', function() {})
            .done(function(data) {
                var html = '';
                var totalAmount = 0;
                var cartItemCount = 0;
                $.each(data, function(key, value) {
                    html += '<div class="u-basket__product g-brd-none g-px-20">' +
                                '<div class="row no-gutters g-pb-5">' +
                                    '<div class="col-11">' +
                                        '<small class="g-color-gray-dark-v4 g-font-size-11 g-line-height-1_6 mb-0">#' + value.sku +' | Per ' + value.quantity_name +'</small>' +
                                        '<h6 class="g-font-weight-400 g-font-size-default">' +
                                            '<a class="g-color-black g-color-primary--hover g-text-underline--none--hover" href="#!">' + value.name + '</a>' +
                                        '</h6>' +
                                        '<small class="g-color-primary g-font-size-12">' + value.total_quantity + ' x $' + value.srp + '</small>' +
                                    '</div>' +
                                    '<button type="button" class="u-basket__product-remove" onclick="removeItem('+value.product_id+');">×</button>' +
                                '</div>' +
                            '</div>';
                    totalAmount += parseFloat(value.total_amount);
                    cartItemCount += value.total_quantity;
                });
                
                $('#mCSB_2_container').html(html);
                $('#total_amount').html('$' + totalAmount.toFixed(2));
                $('.total_amount').html('$' + totalAmount.toFixed(2));
                $('#cart-itm-cnt').html(cartItemCount);
            })
        }

        function removeItem(productId)
        {
            if (confirm("Are you sure you want to remove this item?")) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                
                $.post('{{ route('order.remove') }}', { product_id: productId })
                .done(function(data) {
                    getTally();
                    $("#p-"+productId).remove();
                });
            }
        }
    </script>

    <script>
        $(document).on('ready', function () {
            // initialization of popups
            if ($('#popup-product-status').val() != 1 && $('#has-popup').val() == 1) {
                // alert('test');
                $.HSCore.components.HSModalWindow.init('.js-autonomous-popup', {
                    autonomous: true
                });
            } 
        });
    </script>

@endpush