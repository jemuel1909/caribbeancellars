<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title -->
    <title>@yield('title') - Shop - Caribbean Cellars</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/icon-line/css/simple-line-icons.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/icon-line-pro/style.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/animate.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css">
    {{-- <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/slick-carousel/slick/slick.css"> --}}

    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.css">
    {{-- <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/vendor/jquery-ui/themes/base/jquery-ui.min.css"> --}}

    @stack('css')

    <!-- CSS Unify Theme -->
    {{-- <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/css/styles.e-commerce.css"> --}}

    <!-- CSS Unify -->
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/css/unify-core.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/css/unify-components.css">
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/css/unify-globals.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ url('') }}/theme/home/assets/css/custom.css">

</head>

<body>
    <main>
       
        @include('home.partials.header')
        
        @yield('content')

        @include('home.partials.footer')

        <!-- Go To Top -->
        <a class="js-go-to u-go-to-v2" href="#!" data-type="fixed" data-position='{"bottom": 15, "right": 15}' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
            <i class="hs-icon hs-icon-arrow-top"></i>
        </a>
        <!-- End Go To Top -->

    </main>

    <div class="u-outer-spaces-helper"></div>

    <!-- JS Global Compulsory -->
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery/jquery.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/popper.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/bootstrap/bootstrap.min.js"></script>
    
    <!-- JS Customization -->
    <script src="{{ url('') }}/theme/home/assets/js/custom.js"></script>
    
    @stack('js')
    
    <script type="text/javascript">!function(){var b=function(){window.__AudioEyeSiteHash = "71acfeef4dbb9f35f0afd90d77095397"; var a=document.createElement("script");a.src="https://wsmcdn.audioeye.com/aem.js";a.type="text/javascript";a.setAttribute("async","");document.getElementsByTagName("body")[0].appendChild(a)};"complete"!==document.readyState?window.addEventListener?window.addEventListener("load",b):window.attachEvent&&window.attachEvent("onload",b):b()}();</script>
    
</body>

</html>