@extends('home.layouts.home')

@section('title', 'Forgot Password')

@section('content')
    

    @include('home.partials.breadcrumb', [
        'links' => [
            ['name' => 'Forgot Password', 'route' => 'login', 'params' => []],
        ]
    ])

    <!-- Login -->
    <section class="container g-pt-100 g-pb-20">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5 order-lg-2 g-mb-80">
                <div class="g-brd-around g-brd-gray-light-v3 g-bg-white rounded g-px-30 g-py-50 mb-4">
                    <header class="text-center mb-4">
                        <h1 class="h5 g-color-black g-font-weight-400">Enter your email to reset your password:</h1>
                    </header>
                        
                    @include('home.partials.alert')
                    
                    <!-- Form -->
                    <form class="g-py-15" method="post" action="{{ route('forgot_password') }}">
                        @csrf
                        <div class="mb-4">
                            <div class="input-group g-rounded-left-3">
                                <span class="input-group-prepend g-width-45">
                                    <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                        <i class="icon-finance-067 u-line-icon-pro"></i>
                                    </span>
                                </span>
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15"
                                    type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="mb-5">
                            <button class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Send Email</button>
                        </div>

                    </form>
                    <!-- End Form -->
                </div>

                <div class="text-center">
                    <p class="g-color-gray-dark-v5 mb-0">Already have an account?
                        <a class="g-font-weight-600" href="{{ route('login') }}">signin</a>
                    </p>
                </div>
            </div>

          
        </div>
    </section>
    <!-- End Login -->

    {{-- @include('home.partials.call_to_action') --}}

@endsection

@push('js')
    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });
            
            getTally();

        });
    
        function totalAmount(productId, cnt = 0)
        {   
            quantity = $('#quantity_' + productId).val();
            quantity = parseInt(quantity) + parseInt(cnt);
            
            quantityTypeId = $('#srp_' + productId).val();
            srp = $('#srp_' + productId).find(':selected').data('srp');
            
            total = quantity * srp;
            $('#total_amount_' + productId).html('$' + total.toFixed(2));
        }

        function getTally()
        {
            $.getJSON('{{ route('order.tally') }}', function() {})
            .done(function(data) {
                var html = '';
                var totalAmount = 0;
                var cartItemCount = 0;
                $.each(data, function(key, value) {
                    totalAmount += parseFloat(value.total_amount);
                    cartItemCount += value.total_quantity;
                });
                
                $('.total_amount').html('$' + totalAmount.toFixed(2));
                $('#cart-itm-cnt').html(cartItemCount);
            })
        }

    </script>

@endpush
