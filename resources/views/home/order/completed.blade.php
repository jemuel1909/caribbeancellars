@extends('home.layouts.home')

@section('title', 'Checkout Completed')

@push('css')

@endpush

@section('content')

    @include('home.partials.breadcrumb', [
        'title' => 'Order: Completed',
        'links' => [
            ['name' => 'Completed', 'route' => 'order.completed', 'params' => []],
        ]
    ])
    
    <!-- Page Content -->
    <div class="container g-pt-100 g-pb-70">
        <div class="u-shadow-v19 g-max-width-645 g-brd-around g-brd-gray-light-v4 text-center rounded mx-auto g-pa-30 g-pa-50--md">
            <span class="u-icon-v3 u-icon-size--lg g-color-white g-bg-primary rounded-circle g-pa-15 mb-5">
                <svg width="30" height="30" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 497.5 497.5" style="enable-background:new 0 0 497.5 497.5;" xml:space="preserve">
                <g>
                    <path d="M487.75,78.125c-13-13-33-13-46,0l-272,272l-114-113c-13-13-33-13-46,0s-13,33,0,46l137,136
                    c6,6,15,10,23,10s17-4,23-10l295-294C500.75,112.125,500.75,91.125,487.75,78.125z" fill="#fff"/>
                </g>
                </svg>
            </span>
            <div class="mb-5">
                <h2 class="mb-4">Your Order is Completed!</h2>
            </div>
            <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" href="{{ route('home') }}">Continue Shopping</a>
        </div>
    </div>
    <!-- End Page Content -->

@endsection

@push('js')
    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });    
        });
    
    </script>
@endpush