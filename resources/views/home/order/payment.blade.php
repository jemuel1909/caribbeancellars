@extends('home.layouts.home')

@section('title', 'Payment & Review')

@push('css')

@endpush

@section('content')
    
    @include('home.partials.breadcrumb', [
        'title' => 'Order: Review & Send',
        'links' => [
            ['name' => 'Review & Send', 'route' => 'order.payment', 'params' => []],
        ]
    ])

    <div class="container g-pt-20 g-pb-20">
        <div class="g-mb-75">
            <!-- Step Titles -->
            <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
                <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-bg-primary g-color-white rounded-circle mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Cart Summary</h4>
                </li>
                <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-bg-primary g-color-white rounded-circle mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Customer Information</h4>
                </li>
                <li class="col-3 list-inline-item">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-bg-primary g-color-white rounded-circle mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Review &amp; Send Order</h4>
                </li>
            </ul>
            <!-- End Step Titles -->
        </div>

        <div id="stepFormSteps">
            
            <!-- Payment & Review -->
            <div id="step3">
                <div class="row">
                    <div class="col-md-8 g-mb-100">
                        <!-- Checkout Form -->
                        <form class="js-validate js-step-form" data-progress-id="#stepFormProgress" data-steps-id="#stepFormSteps" method="post" action="{{ route('order.checkout') }}">

                            @csrf

                            <!-- Alert -->
                            <div class="alert g-brd-around g-brd-gray-dark-v5 rounded-0 g-pa-0 mb-4" role="alert">
                                <div class="media">
                                    <div class="d-flex g-brd-right g-brd-gray-dark-v5 g-pa-15">
                                        <span class="u-icon-v1 u-icon-size--xs">
                                            <i class="fa fa-check fa-lg g-font-primary"></i>
                                        </span>
                                    </div>
                                    <div class="media-body g-pa-15">
                                        <p class="g-color-dark g-font-weight-600 m-0 text-uppercase">Is your customer information correct?</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Alert -->

                            <!-- Shipping Details -->
                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-15 g-pl-70 mb-5">
                                
                                @if (isset($user->id))
                                    <li class="g-my-3">{{ $user->first_name }} {{ $user->last_name }} <a href="{{ route('order.shipping') }}"><i class="fa fa-pencil"></i></a></li>
                                    <li class="g-my-3">{{ $user->email }}</li>
                                @else
                                    <li class="g-my-3">{{ $shippingInfo['first_name'] }} {{ $shippingInfo['last_name'] }} <a href="{{ route('order.shipping') }}"><i class="fa fa-pencil"></i></a></li>
                                    <li class="g-my-3">{{ $shippingInfo['email'] }}</li>
                                @endif

                                <li class="g-my-3">{{ $shippingInfo['phone_no'] }}</li>
                                <li class="g-my-3">{{ $shippingInfo['delivery_location'] }}</li>
                                
                                @if (isset($shippingInfo['delivery_date']))
                                    <li class="g-my-3">Delivery Date: {{ date('m/d/Y', strtotime($shippingInfo['delivery_date'])) }}</li>
                                @endif

                                @if (isset($shippingInfo['delivery_time']))
                                    <li class="g-my-3">Delivery Time: {{ date('h:i A', strtotime($shippingInfo['delivery_time'])) }}</li>
                                @endif

                            </ul>
                            <!-- End Shipping Details -->

                            <div class="g-pb-30 g-mb-30">
                                <div class="text-right">
                                    <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit">Confirm &amp; Submit Order</button>
                                </div>
                            </div>
                        </form>
                        <!-- End Checkout Form -->
                    </div>

                    <div class="col-md-4 g-mb-100">

                        <!-- Order Summary -->
                        <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                            <div class="g-mb-15">
                                <h4 class="h6 text-uppercase mb-3">Order summary</h4>
                            </div>

                            <!-- Accordion -->
                            <div id="accordion-basket" class="mb-4" role="tablist" aria-multiselectable="true">
                                <div id="accordion-basket-heading-03" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                                    <h5 class="g-font-weight-400 g-font-size-default mb-0">
                                        <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-basket-body-03" data-toggle="collapse" data-parent="#accordion-basket" aria-expanded="false" aria-controls="accordion-basket-body-03"><span id="cart-item-count">1 item</span> in cart
                                            <span class="ml-3 fa fa-angle-down"></span></a>
                                    </h5>
                                </div>

                                <div id="accordion-basket-body-03" class="collapse g-brd-bottom g-brd-gray-light-v3 " role="tabpanel" aria-labelledby="accordion-basket-heading-03">
                                    <div class="g-py-15">
                                        <ul class="list-unstyled mb-3" id="cart-items-basket">
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Accordion -->

                            <div class="d-flex justify-content-between mb-3">
                                <span class="g-color-black">Cart Subtotal</span>
                                <span class="g-color-black g-font-weight-300" id="subtotal_amount">$00.00</span>
                            </div>
                            
                            <div class="d-flex justify-content-between mb-3">
                                <span class="g-color-black">Order Total</span>
                                <span class="g-color-black g-font-weight-300" id="total_amount">$00.00</span>
                            </div>

                        </div>
                        <!-- End Order Summary -->

                    </div>
                </div>
            </div>
            <!-- End Payment & Review -->
            
        </div>
    </div>

@endsection

@push('js')
    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.jquery.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/image-select/src/ImageSelect.jquery.js"></script>


    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.scrollbar.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.select.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.count-qty.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.validation.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.go-to.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });
            
            getTally();

        });
    
        function totalAmount(productId, cnt = 0)
        {   
            quantity = $('#quantity_' + productId).val();
            quantity = parseInt(quantity) + parseInt(cnt);
            
            quantityTypeId = $('#srp_' + productId).val();
            srp = $('#srp_' + productId).find(':selected').data('srp');
            
            total = quantity * srp;
            $('#total_amount_' + productId).html('$' + total.toFixed(2));
        }

        function getTally()
        {
            $.getJSON('{{ route('order.tally') }}', function() {})
            .done(function(data) {
                var html = '';
                var totalAmount = 0;
                var cartItemCount = 0;
                $.each(data, function(key, value) {
                    html += '<li class="d-flex justify-content-start">' +
                                '<div class="d-block">' +
                                    '<h4 class="h6 g-color-black">' + value.name + '</h4>' +
                                    '<small class="g-color-black g-font-size-12">' + value.total_quantity + ' x $' + value.srp + '</small> ' +
                                    '<span class="d-block g-color-black g-font-weight-400">$ ' + (parseFloat(value.total_quantity * value.srp)).toFixed(2) + '</span>' +
                                '</div>' +
                            '</li>';
                    totalAmount += parseFloat(value.total_amount);
                    cartItemCount += value.total_quantity;
                });
                
                $('#cart-items-basket').html(html);
                if (cartItemCount > 1) {
                    $('#cart-item-count').html( cartItemCount + ' items' );
                } else {
                    $('#cart-item-count').html( cartItemCount + ' item' );
                }
                
                $('#subtotal_amount').html('$ ' + totalAmount.toFixed(2));
                $('#total_amount').html('$ ' + totalAmount.toFixed(2));
                $('.total_amount').html('$' + totalAmount.toFixed(2));
                $('#cart-itm-cnt').html(cartItemCount);
            })
        }

    </script>
@endpush