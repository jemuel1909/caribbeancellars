@extends('home.layouts.home')

@section('title', 'Customer Information - Shippping Address')

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <style>
        .form-control[readonly] {
            background-color: rgb(255 255 255 / 10%);
            opacity: 1;
        }
    </style>
@endpush

@section('content')
    
    @include('home.partials.breadcrumb', [
        'title' => 'Order: Address',
        'links' => [
            ['name' => 'Address', 'route' => 'order.shipping', 'params' => []],
        ]
    ])
    
    <!-- Checkout Form -->
    <div class="container g-pt-20 g-pb-20">
        
        <div class="g-mb-75">
            <!-- Step Titles -->
            <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
                <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-bg-primary g-color-white rounded-circle mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Cart Summary</h4>
                </li>

                <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-bg-primary g-color-white rounded-circle mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Customer Information</h4>
                </li>

                <li class="col-3 list-inline-item">
                    <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                        <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                        <i class="fa fa-check g-show-check"></i>
                    </span>
                    <h4 class="g-font-size-16 text-uppercase mb-0">Review &amp; Send Order</h4>
                </li>
            </ul>
            <!-- End Step Titles -->
        </div>
        
        <div class="row">
            <div class="col-lg-9 col-md-8 g-mb-30">
                <div id="accordion-steps" role="tablist" aria-multiselectable="true">

                    <!-- Retailer's Shipping Information Details Card -->
                    <div class="card g-mb-5">
                        <div id="accordion-steps-heading-02" class="card-header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover g-brd-around g-brd-gray-light-v4 g-pa-10-15" href="#accordion-steps-body-02" aria-expanded="false" aria-controls="accordion-steps-body-02" data-toggle="collapse" data-parent="#accordion-steps">
                                    <span class="u-accordion__control-icon g-mr-10">
                                        <i class="fa fa-angle-down"></i>
                                        <i class="fa fa-angle-up"></i>
                                    </span>
                                    Customer Information
                                </a>
                            </h5>
                        </div>

                        <div id="accordion-steps-body-02" class="collapse show" role="tabpanel" aria-labelledby="accordion-steps-heading-02" data-parent="#accordion-steps">
                            <div class="u-accordion__body g-color-gray-dark-v5">
                                <div class="container">

                                    @include('home.partials.alert')

                                    <!-- Form -->
                                    <form class="g-py-15" method="post" action="{{ route('order.payment') }}">
                                        @csrf
                                        
                                        @if (!isset($user->id))                                         
                                            
                                            <div class="row">
                                                <div class="col-sm-6 g-mb-20">
                                                    <div class="form-group">
                                                        <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="first-name">First Name</label>
                                                        <input id="first-name" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-3 g-py-15" name="first_name" type="text" placeholder="First Name" value="{{ $shippingInfo['first_name'] ?? null }}" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 g-mb-20">
                                                    <div class="form-group">
                                                        <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="last-name">Last Name</label>
                                                        <input id="last-name" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-3 g-py-15" name="last_name" type="text" placeholder="Last Name" value="{{ $shippingInfo['last_name'] ?? null }}" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 g-mb-20">
                                                    <div class="form-group">
                                                        <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="email-address">Email Address</label>
                                                        <input id="email-address" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-3 g-py-15" name="email" type="email" placeholder="your.email@mail.com" value="{{ $shippingInfo['email'] ?? null }}" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>

                                        @endif

                                        <div class="row">
                                            <div class="col-sm-6 g-mb-20">
                                                <div class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="phone-no">Phone No.</label>
                                                    <input id="phone-no" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-3 g-py-15" name="phone_no" type="text" placeholder="(XXX) XXX-XXXX" value="{{ $shippingInfo['phone_no'] ?? null }}" data-mask="(999) 999-9999" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 g-mb-20">
                                                <div class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="delivery_location_id">Delivery Location</label>
                                                    
                                                    <select class="form-control form-control-lg rounded-3" name="delivery_location_id" required data-msg="This field is mandatory" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1">
                                                        
                                                        <option value="">-Select-</option>
                                                        @foreach($deliveryLocations as $value)

                                                            <option value="{{ $value->id }}" {{ (($shippingInfo['delivery_location_id'] ?? null) == $value->id) ? 'selected' : null }}>{{ $value->name }}</option>

                                                        @endforeach

                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 g-mb-20">
                                                <div class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="date-input">Delivery Date (optional)</label>
                                                    <input class="form-control form-control-md rounded-3" name="delivery_date" type="date" value="{{ $shippingInfo['delivery_date'] ?? null }}" id="date-input" placeholder="mm/dd/yyyy">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 g-mb-20">
                                                <div class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="time-input">Delivery Time (optional)</label>
                                                    <input class="form-control form-control-md rounded-3" name="delivery_time" type="time" value="{{ $shippingInfo['delivery_time'] ?? null }}" id="time-input">
                                                </div>
                                            </div>
                                        </div>
    
                                        <hr class="g-mb-10">
                                        
                                        <div class="row">
                                            <div class="col-sm-12 g-mb-20">
                                                <div class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13" for="notes">Notes</label>
                                                    <textarea id="notes" name="notes" class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-py-13 g-px-15" rows="7">{{ $shippingInfo['notes'] ?? null }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 g-mb-20">
                                                <div class="form-group">
                                                    @if(env('GOOGLE_RECAPTCHA_SITE_KEY'))
                                                        <div class="g-recaptcha"
                                                             data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}">
                                                        </div>
                                                   @endif
                                                </div>
                                            </div>                                            
                                        </div>

                                        <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit" data-next-step="#step3">Submit</button>
                                    
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Accordion -->
                
            </div>

            <div class="col-lg-3 col-md-4 g-mb-30">
                <!-- Order Summary -->
                <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                    <h4 class="h6 text-uppercase mb-3">Order summary</h4>

                    <!-- Accordion -->
                    <div id="accordion-basket" class="mb-4" role="tablist" aria-multiselectable="true">
                        <div id="accordion-basket-heading-03" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                            <h5 class="g-font-weight-400 g-font-size-default mb-0">
                                <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-basket-body-03" data-toggle="collapse" data-parent="#accordion-basket" aria-expanded="false" aria-controls="accordion-basket-body-03"><span id="cart-item-count">1 item</span> in cart
                                    <span class="ml-3 fa fa-angle-down"></span></a>
                            </h5>
                        </div>

                        <div id="accordion-basket-body-03" class="collapse" role="tabpanel" aria-labelledby="accordion-basket-heading-03">
                            <div class="g-py-15">
                                <ul class="list-unstyled mb-3" id="cart-items-basket"></ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Accordion -->

                    <div class="d-flex justify-content-between mb-2">
                        <span class="g-color-black">Subtotal</span>
                        <span class="g-color-black g-font-weight-300" id="subtotal_amount">$00.00</span>
                    </div>

                    <div class="d-flex justify-content-between">
                        <span class="g-color-black">Order Total</span>
                        <span class="g-color-black g-font-weight-300" id="total_amount">$00.00</span>
                    </div>
                </div>
                <!-- End Order Summary -->
            </div>
        </div>
        
    </div>
    <!-- End Checkout Form -->


@endsection


@push('js')

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.jquery.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/image-select/src/ImageSelect.jquery.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery.maskedinput/src/jquery.maskedinput.js"></script>

    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.scrollbar.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.select.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.count-qty.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.validation.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.go-to.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.masked-input.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of HSScrollBar component
            $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of form validation
            $.HSCore.components.HSValidation.init('.js-validate');

            // initialization of custom select
            $.HSCore.components.HSSelect.init('.js-custom-select');

            // initialization of quantity counter
            $.HSCore.components.HSCountQty.init('.js-quantity');

            // initialization of forms
            $.HSCore.components.HSMaskedInput.init('[data-mask]');

            getTally();

            $('#date-input').flatpickr(
                {
                    "disable": [
                        function(date) {
                            return (date.getDay() === 0);
                        }
                    ],
                    dateFormat: "m/d/y",
                }
            );
        });

        function totalAmount(productId, cnt = 0)
        {   
            quantity = $('#quantity_' + productId).val();
            quantity = parseInt(quantity) + parseInt(cnt);
            
            quantityTypeId = $('#srp_' + productId).val();
            srp = $('#srp_' + productId).find(':selected').data('srp');
            
            total = quantity * srp;
            $('#total_amount_' + productId).html('$' + total.toFixed(2));
        }
        
        function getTally()
        {
            $.getJSON('{{ route('order.tally') }}', function() {})
            .done(function(data) {
                var html = '';
                var totalAmount = 0;
                var cartItemCount = 0;
                $.each(data, function(key, value) {
                    html += '<li class="d-flex justify-content-start">' +
                                '<div class="d-block">' +
                                    '<h4 class="h6 g-color-black">' + value.name + '</h4>' +
                                    '<small class="g-color-black g-font-size-12">' + value.total_quantity + ' x $' + value.srp + '</small> ' +
                                    '<span class="d-block g-color-black g-font-weight-400">$ ' + (parseFloat(value.total_quantity * value.srp)).toFixed(2) + '</span>' +
                                '</div>' +
                            '</li>';
                    totalAmount += parseFloat(value.total_amount);
                    cartItemCount += value.total_quantity;
                });
                
                $('#cart-items-basket').html(html);
                if (cartItemCount > 1) {
                    $('#cart-item-count').html( cartItemCount + ' items' );
                } else {
                    $('#cart-item-count').html( cartItemCount + ' item' );
                }
                
                $('#subtotal_amount').html('$ ' + totalAmount.toFixed(2));
                $('#total_amount').html('$ ' + totalAmount.toFixed(2));
                $('.total_amount').html('$' + totalAmount.toFixed(2));
                $('#cart-itm-cnt').html(cartItemCount);
            })
        }
        
    </script>
@endpush