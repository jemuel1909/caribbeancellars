@extends('home.layouts.home')

@section('title', 'Order Summary')

@push('css')

@endpush

@section('content')
    
    @include('home.partials.breadcrumb', [
        'title' => 'Order: Summary',
        'links' => [
            ['name' => 'Summary', 'route' => 'order.summary', 'params' => []],
        ]
    ])

    @if($orders != null)
        <!-- Checkout Form -->
        <form id="formCartUpdate" class="g-py-15" method="post" action="{{ route('order.update') }}">
            @csrf
            <div class="container g-pt-20 g-pb-20">
                <div class="g-mb-75">
                    <!-- Step Titles -->
                    <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
                        <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-bg-primary g-color-white rounded-circle mx-auto mb-3">
                                <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                                <i class="fa fa-check g-show-check"></i>
                            </span>
                            <h4 class="g-font-size-16 text-uppercase mb-0">Cart Summary</h4>
                        </li>

                        <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                                <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                                <i class="fa fa-check g-show-check"></i>
                            </span>
                            <h4 class="g-font-size-16 text-uppercase mb-0">Customer Information</h4>
                        </li>

                        <li class="col-3 list-inline-item">
                            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                                <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                                <i class="fa fa-check g-show-check"></i>
                            </span>
                            <h4 class="g-font-size-16 text-uppercase mb-0">Review &amp; Send Order</h4>
                        </li>
                    </ul>
                    <!-- End Step Titles -->
                </div>

                <div id="stepFormSteps">
                    <!-- Shopping Cart -->
                    <div id="step1" class="active">
                        <div class="row">
                            <div class="col-lg-9 col-md-8 g-mb-75">
                                

                                <div class="alert alert-secondary mb-5 g-font-size-12" role="alert">
                                
                                    * Prices are subjected to change without notice.
                                    <br>
                                    For all future orders, the process will begin five (5) days prior to delivery date to ensure stock availability. Out of stocks will be communicated before processing.
                                    
                                </div>

                                
                                @include('home.partials.alert')
                                
                                <!-- Products Block -->
                                <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                                    <table class="text-center w-100">
                                        <thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
                                            <tr>
                                                <th class="g-font-weight-400 g-width-80 g-pb-20 text-left">Item #</th>
                                                <th class="g-font-weight-400 g-text-left g-pb-20 text-left">Product</th>
                                                <th class="g-font-weight-400 g-width-100 g-pb-20">Unit</th>
                                                <th class="g-font-weight-400 g-width-100 g-pb-20">Price</th>
                                                <th class="g-font-weight-400 g-width-50 g-pb-20">Qty</th>
                                                <th class="g-font-weight-400 g-width-150 g-pb-20">Total</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody id="items">
                                            
                                            @foreach($orders as $product)
                                                
                                                <!-- Item-->
                                                <tr class="g-brd-bottom g-brd-gray-light-v3" id="p-{{ $product['product_id'] }}">
                                                    
                                                    <td class="g-color-gray-dark-v2 g-font-size-13 text-left">{{ $product['sku'] }}</td>
                                                    
                                                    <td class="g-color-gray-dark-v2 g-font-size-13 text-left">
                                                        
                                                        <h4 class="h6 g-color-black g-py-20">{{ $product['name'] }}</h4>
                                                        
                                                    </td>
                                                    <td class="g-color-gray-dark-v2 g-font-size-13">{{ $product['quantity_name'] }}</td>
                                                    <td class="g-color-gray-dark-v2 g-font-size-13">$ {{ $product['srp'] }}</td>
                                                    <td>
                                                        <div class="js-quantity input-group u-quantity-v1 g-width-80 g-brd-primary--focus">
                                                            <input class="js-result form-control text-center g-font-size-13 rounded-0 g-pa-0 product-quantity" type="text" name="prod[{{ $product['product_id'] }}][{{ $product['quantity_type_id'] }}]" value="{{ $product['total_quantity'] }}">

                                                            <div class="input-group-addon d-flex align-items-center g-width-30 g-brd-gray-light-v2 g-bg-white g-font-size-12 rounded-0 g-px-5 g-py-6">
                                                                <i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up"></i>
                                                                <i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down"></i>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-right g-color-black">
                                                        <span class="g-color-gray-dark-v2 g-font-size-13 mr-4">$ {{ @number_format($product['total_amount'], 2) }}</span>
                                                        <span class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer" onclick="removeItem('{{ $product['product_id'] }}');">
                                                            <i class="mt-auto fa fa-trash"></i>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <!-- End Item-->
                                                
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- End Products Block -->

                              

                            </div>

                            <div class="col-lg-3 col-md-4 g-mb-75">
                                <!-- Summary -->
                                <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                                    <h4 class="h6 text-uppercase mb-3">Summary</h4>

                                    <div class="d-flex justify-content-between mb-2">
                                        <span class="g-color-black">Subtotal</span>
                                        <span class="g-color-black g-font-weight-300" id="subtotal_amount">$ 00.00</span>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <span class="g-color-black">Order Total</span>
                                        <span class="g-color-black g-font-weight-300" id="total_amount">$ 00.00</span>
                                    </div>
                                </div>
                                <!-- End Summary -->

                                <button class="btn btn-block u-btn-outline-black g-brd-gray-light-v1 g-bg-black--hover g-font-size-13 text-uppercase g-py-15 mb-4" type="submit">Update Shopping Cart</button>
                                <a class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4" href="{{ route('order.shipping') }}" data-next-step="#step2">Proceed to Checkout</a>

                            </div>
                        </div>
                    </div>
                    <!-- End Shopping Cart -->
                </div>

            </div>
        </form>
        <!-- End Checkout Form -->
    @else

        <div class="container text-center g-py-100">
            <div class="mb-5">
                <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">
                <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
                </span>
                <h2 class="g-mb-30">Your Cart is Currently Empty</h2>
                {{-- <p>Before proceed to checkout you must add some products to your shopping cart.<br>You will find a lot of interesting products on our "Shop" page.</p> --}}
            </div>
            <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" href="{{ route('home') }}">Start Shopping</a>
        </div>

    @endif

    {{-- @include('home.partials.call_to_action') --}}

@endsection

@push('js')

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.jquery.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/image-select/src/ImageSelect.jquery.js"></script>


    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.scrollbar.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.select.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.count-qty.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.validation.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.go-to.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of HSScrollBar component
            $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of form validation
            $.HSCore.components.HSValidation.init('.js-validate');

            // initialization of custom select
            $.HSCore.components.HSSelect.init('.js-custom-select');

            // initialization of quantity counter
            $.HSCore.components.HSCountQty.init('.js-quantity');

            getTally();

            $("form#formCartUpdate").submit(function(){

                var withoutZero = true;
                $(".product-quantity").each(function( index ) {
                    if ($(this).val() == 0) {
                        withoutZero = false;
                    }
                });
                if (withoutZero == false) {
                    var msg = "Some of the item/s in your cart is/are set to '0'.\n Are you sure you want to continue?";
                } else {
                    var msg = "Are you sure you want to update the no. of items in your cart?";
                }
                if (confirm(msg)) {
                    return true;
                } else {
                    return false;
                }

            });

        });

        function getTally()
        {
            $.getJSON('{{ route('order.tally') }}', function() {})
            .done(function(data) {
                var totalAmount = 0;
                var cartItemCount = 0;
                $.each(data, function(key, value) {
                    totalAmount += parseFloat(value.total_amount);
                    cartItemCount += value.total_quantity;
                });
                
                $('#subtotal_amount').html('$ ' + totalAmount.toFixed(2));
                $('#total_amount').html('$ ' + totalAmount.toFixed(2));
                $('.total_amount').html('$' + totalAmount.toFixed(2));
                $('#cart-itm-cnt').html(cartItemCount);
            })
        }

        function removeItem(productId)
        {   
            if (confirm("Are you sure you want to remove this item?")) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                
                $.post('{{ route('order.remove') }}', { product_id: productId })
                .done(function(data) {
                    getTally();
                    $("#p-"+productId).remove();
                });
            }
        }
    
    </script>
@endpush