@extends('home.layouts.home')

@section('title', 'About Us')

@push('css')

@endpush

@section('content')

    <!-- Promo Block -->
    <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options='{direction: "fromtop", animation_duration: 25, direction: "reverse"}'>
        <!-- Parallax Image -->
        <div class="divimage dzsparallaxer--target w-100 g-bg-cover g-bg-pos-center g-bg-black-opacity-0_2--after" style="height: 140%; background-image: url({{ url('') }}/theme/home/assets/img/banner/banner-about.jpg);"></div>
        <!-- End Parallax Image -->

        <!-- Promo Block Content -->
        <div class="container g-color-white text-center g-py-50">
            <span class="d-block g-font-weight-300 g-font-size-25 mb-3">Discover more about us</span>
            {{-- <h3 class="h1 text-uppercase mb-0">Caribbean no 1 Brand</h3> --}}
        </div>
        <!-- Promo Block Content -->
    </section>
    <!-- End Promo Block -->

    <section class="container g-pt-100 g-pb-50">
        <div class="row justify-content-center g-mb-60">
            <div class="col-lg-7">
                <!-- Heading -->
                <div class="text-center">
                    <h2 class="h3 g-color-black text-uppercase mb-2">Overview</h2>
                    <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
                    {{-- <p class="mb-0">In 1983, John E. Anderson opened A. H. Riise (BVI) Ltd on Tortola. In 1999 the company purchased Fort Wines and Spirits. The companies were merged and named Caribbean Cellars Ltd. representing leading international brands in both a wholesale and retail environment. The team provides full service distribution throughout the British Virgin Islands.</p> --}}
                </div>
                <!-- End Heading -->
            </div>
        </div>

        <div class="row">

            <div class="col-lg-6">
                <div class="mb-5">
                    <p>Caribbean Cellars, Ltd: is an importer, wholesaler and retailer of category-leading beverage distributor with over 30 years trading history in the British Virgin Islands (BVI). It is the leading beverage distribution company in the BVI.</p>
                    <p>Our new 15,000 square foot warehouse facility located in Lower Estate, Road Town, Tortola, with its temperature controlled wine room ensures the quality of our products whether delivered wholesale or purchased onsite by the public and the trade. Our on-premise domination in the market is heavily supported by our strong off trade network, web-orders, Wine Club and Corporate relationships.</p>
                    <p>Caribbean Cellars, Ltd.’s 900 sq ft retail store located on Wickham’s Cay II serves the largest marina in the BVI, home of Sunsail, Moorings and Foot Loose Charters. It also services all shoppers – local and expat – as we are committed to providing our customers with an exceptional shopping experience.</p>
                    <p class="g-mb-30">Save time and stock your yacht with your favorite Wines, Beers and Spirits in advance of your arrival using our online store for ALL your Yacht and Villa Provisioning needs. Caribbean Cellars, Ltd., is the Number 1 Provisioning Service in the BVI, and provides FREE delivery to ALL Mariners, Charter Companies and Docks on island.</p>
                    
                    <div class="mb-4">
                        <h2 class="h5 g-color-gray-dark-v2 g-font-weight-600">Mailing Address:</h2>
                        <p class="g-color-gray-dark-v4 g-font-size-16">P.O Box 3063
                            <br>Lower Estate
                            <br>Road Town, Tortola
                        </p>
                    </div>
                    <div class="mb-3">
                        <h2 class="h5 g-color-gray-dark-v2 g-font-weight-600">Call us:</h2>
                        <p class="g-color-gray-dark-v4">Tel: <span class="g-color-gray-dark-v2">284-393-4470</span>
                            <br>Fax: <span class="g-color-gray-dark-v2">284-393-4471</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 g-mb-50">
                
                <img class="img-fluid" src="http://caribbeancellars.com/wp-content/uploads/2018/11/ccl-about.jpg">
                
                <div id="GMapRoadmap" class="js-g-map embed-responsive embed-responsive-21by9 mt-3" data-lat="18.424899" data-lng="-64.624383" data-pin="true"></div>
            </div>
        </div>
    </section>
{{-- 
    <!-- Team Blocks -->
    <section class="g-bg-gray-light-v5">
        
        <div class="container g-pt-100 g-pb-70">
        
            <div class="row justify-content-center g-mb-60">
                <div class="col-lg-7">
                    <!-- Heading -->
                    <div class="text-center">
                        <h2 class="h3 g-color-black text-uppercase mb-2">Organization Listing</h2>
                        <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
                        <p class="mb-0">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>
                    </div>
                    <!-- End Heading -->
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Kevin Fahie</h4>
                        <p class="g-font-size-13">General Manager</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Cherica Thomas</h4>
                        <p class="g-font-size-13">Account Manager/Head of Retail</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Kishama Robinson</h4>
                        <p class="g-font-size-13">Marketing Manager</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Roosevelt Smith</h4>
                        <p class="g-font-size-13">Sales Manager</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Ron Allen</h4>
                        <p class="g-font-size-13">Account Supervisor</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Danel Dailey</h4>
                        <p class="g-font-size-13">Customer Service Supervisor</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Keshma Cameron</h4>
                        <p class="g-font-size-13">Retail Manager</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 g-mb-30">
                    <div class="text-center u-block-hover">
                        <!-- Figure -->
                        <figure class="u-block-hover g-mb-25">
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ url('') }}/theme/home/assets/img-temp/300x400/img1.png" alt="Image Description">
                            <!-- End Figure Image -->
                        </figure>
                        <!-- End Figure -->

                        <!-- Figure Info -->
                        <h4 class="h5 g-color-black g-mb-5">Ranalie Cain</h4>
                        <p class="g-font-size-13">Warehouse Manager</p>
                        <!-- End Figure Info-->
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End Team Blocks --> --}}


    {{-- @include('home.partials.call_to_action') --}}

@endsection

@push('js') 

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });    
        });
    </script>

    <!-- JS Implementing Plugins -->
    <script  src="{{ url('') }}/theme/home/assets/vendor/gmaps/gmaps.min.js"></script>

    <!-- JS Unify -->
    <script  src="{{ url('') }}/theme/home/assets/js/components/gmap/hs.map.js"></script>

    <!-- JS Plugins Init. -->
    <script >
    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }
    </script>

    <script  src="//maps.googleapis.com/maps/api/js?key=AIzaSyCpqOJ8suilw7IyGoiTZdkkv3qnXwD4W4I&amp;callback=initMap" async="" defer=""></script>
    
@endpush