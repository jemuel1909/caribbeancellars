@extends('home.layouts.home')

@section('title', 'Contact Us')

@push('css')

@endpush

@section('content')

    @include('home.partials.breadcrumb', [
        'title' => 'Contact Us',
        'links' => [
            ['name' => 'Contact Us', 'route' => 'contact', 'params' => []],
        ]
    ])

    <section class="container g-pt-100 g-pb-40">
        <div class="row justify-content-between">
            <div class="col-md-7 g-mb-60">

                @include('home.partials.alert')

                <!-- Contact Form -->
                <form method="post" action="{{ route('contact_submit') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 form-group g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-size-13">Name:</label>
                            <input name="name" class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="text" required>
                        </div>

                        <div class="col-md-6 form-group g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-size-13">Email:</label>
                            <input name="email" class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="email" required>
                        </div>

                        <div class="col-md-6 form-group g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-size-13">Subject:</label>
                            <input name="subject" class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="text">
                        </div>

                        <div class="col-md-6 form-group g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-size-13">Phone:</label>
                            <input name="phone" class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="tel">
                        </div>

                        <div class="col-md-12 form-group g-mb-40">
                            <label class="g-color-gray-dark-v2 g-font-size-13">Message:</label>
                            <textarea name="message" class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-py-13 g-px-15" rows="7" required></textarea>
                        </div>

                        <div class="col-md-6 form-group g-mb-20">
                            @if(env('GOOGLE_RECAPTCHA_SITE_KEY'))
                                <div class="g-recaptcha"
                                    data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}">
                                </div>
                            @endif
                        </div>
                    </div>

                    <button class="btn u-btn-primary rounded-3 g-py-12 g-px-20" type="submit" role="button">Send Enquiry</button>
                </form>
                <!-- End Contact Form -->
            </div>

            <div class="col-md-4">
                <h1 class="h2 g-font-weight-300 mb-5">Caribbean Cellars: B.V.I.</h1>
                <div class="mb-4">
                    <h2 class="h5 g-color-gray-dark-v2 g-font-weight-600">Mailing Address:</h2>
                    <p class="g-color-gray-dark-v4 g-font-size-16">P.O Box 3063
                        <br>Lower Estate
                        <br>Road Town, Tortola
                    </p>
                </div>

                <div class="mb-3">
                    <h2 class="h5 g-color-gray-dark-v2 g-font-weight-600">Call us:</h2>
                    <p class="g-color-gray-dark-v4">Tel: <span class="g-color-gray-dark-v2">284-393-4470</span>
                        <br>Fax: <span class="g-color-gray-dark-v2">284-393-4471</span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    {{-- @include('home.partials.call_to_action') --}}

@endsection

@push('js') 

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });    
        });
    </script>
@endpush