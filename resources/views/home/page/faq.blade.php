@extends('home.layouts.home')

@section('title', 'FAQ')

@push('css')

@endpush

@section('content')

    <!-- Promo Block -->
    <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options='{direction: "fromtop", animation_duration: 25, direction: "reverse"}'>
        <!-- Parallax Image -->
        <div class="divimage dzsparallaxer--target w-100 g-bg-cover g-bg-pos-center g-bg-black-opacity-0_2--after" style="height: 140%; background-image: url({{ url('') }}/theme/home/assets/img/banner/banner-faq.jpg);"></div>
        <!-- End Parallax Image -->

        <!-- Promo Block Content -->
        <div class="container g-color-white text-center g-py-50">
            {{-- <span class="d-block g-font-weight-300 g-font-size-25 mb-3">Discover more about us</span> --}}
            <h3 class="h1 text-uppercase mb-0">FAQ</h3>
        </div>
        <!-- Promo Block Content -->
    </section>
    <!-- End Promo Block -->

    {!! $page->description !!}

    {{-- <section class="g-pt-100 g-pb-100 g-bg-secondary">
        <div class="container">
            <div class="col-md-12">
                <div id="accordion-07" class="u-accordion u-accordion-color-primary" role="tablist" aria-multiselectable="true">
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-01" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-01" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="true" aria-controls="accordion-07-body-01">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        When will my order be processed?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-01" class="collapse show" role="tabpanel" aria-labelledby="accordion-07-heading-01" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                For all future orders, the process will begin five (5) days prior to delivery date to ensure stock availability. Out of stock items will be communicated before processing.
                            </div>
                        </div>
                    </div>

                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-02" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-02" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-02">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Can I pick up my order?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-02" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-02" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Yes, orders can be picked up from 8:30am-5:00pm, we are located in Lower Estate, Road Town, Tortola, BVI, behind the Elmore Stoutt High School.
                            </div>
                        </div>
                    </div>

                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-03" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-03" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        What is the drinking age in the British Virgin Islands?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-03" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-03" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                The legal minimum age for purchasing liquor or drinking alcohol in bars or restaurants is <strong>18</strong>.  Alcohol beverages can be sold any day of the week, including Sunday. You can have an open container on the beach, but you can be fined if you litter.
                            </div>
                        </div>
                    </div>

                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-04" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-04" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Can I open a charge account?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-04" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-04" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Yes, a charge account can be opened but <strong>ONLY</strong> to <strong>CUSTOMERS</strong> residing in the territory.
                            </div>
                        </div>
                    </div>

                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-05" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-05" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        When will payment be required?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-05" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-05" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Payment will be required on or before the day of delivery.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-06" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-06" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Can I pay the driver directly?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-06" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-06" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Yes, our drivers are allowed to collect funds from our customers.  Please be reminded that we only accept USD.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-07" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-07" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Can I return unused items?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-07" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-07" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Only items purchased on Consignment are allowed to be returned at a minimum of two (2) days after an event. This option is only offered to customers residing in the territory.
                            </div>
                        </div>
                    </div>

                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-08" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-08" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Do you offer free delivery?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-08" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-08" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Yes all deliveries are free of charge
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-09" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-09" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        How far in advance shall I place an order?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-09" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-09" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Orders can be placed can at any time, but will be processed 5 days before delivery date.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-10" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-10" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Do you offer same day delivery?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-10" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-10" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Items ordered before 10 am will be delivered on the same day.  Anything ordered after 10 am will be delivered on the next business day.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-11" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-11" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Which forms of payment do you accept?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-11" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-11" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                The following are the accepted forms of payment
                                <ul>
                                    <li>Cash USD</li>
                                    <li>Credit Card
                                        <ul>
                                            <li>Visa</li>
                                            <li>MasterCard</li>
                                            <li>American Express</li>
                                            <li>Discover</li>
                                        </ul>
                                    </li>
                                    <li>Company Check</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-12" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-12" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Are there any additional taxes or fees?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-12" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-12" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                There are no sales tax in the British Virgin Islands.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-13" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-13" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        When can I cancel my order?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-13" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-13" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Orders can be canceled 24hrs before the delivery date.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-14" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-14" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Are there any penalties for cancelling an order?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-14" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-14" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                There are no penalties for canceling your order.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-15" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-15" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Is there a minimum dollar amount for a delivery?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-15" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-15" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                There are no minimum delivery dollar amount or quantity.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-16" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-16" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Do we deliver to other islands besides Tortola?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-16" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-16" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                Orders can be delivered to any island in the BVI as long as there is a ferry or barge service to that island.
                            </div>
                        </div>
                    </div>
                    <div class="card rounded-0 g-brd-none">
                        <div id="accordion-07-heading-17" class="u-accordion__header g-pa-0" role="tab">
                            <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700 g-pa-20a mb-0">
                                <a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-17" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-03">
                                    <span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v4 g-color-primary text-center g-pa-20">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="g-pa-20">
                                        Do orders have to place by case?
                                    </span>
                                </a>
                            </h5>
                        </div>
                        <div id="accordion-07-body-17" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-17" data-parent="#accordion-07">
                            <div class="u-accordion__body g-bg-gray-light-v5 g-px-50 g-py-30">
                                The dropdown arrow in unit column indicates whether cases and units can be ordered. Beer, water, Non-alcoholic and Glassware are typically cases only. However, mixers and all other categories can be ordered by both cases and bottles.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section> --}}

@endsection

@push('js')

<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- JS Implementing Plugins -->
<script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

<!-- JS Unify -->
<script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
<script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
<script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
<script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>

<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function() {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of HSDropdown component
        $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
            afterOpen: function() {
                $(this).find('input[type="search"]').focus();
            }
        });
    });
</script>
@endpush