
@extends('home.layouts.home_no_header_no_footer')

@section('title', 'Age Verification')

@push('css')

@endpush

@section('content')
    
    <section class="container g-pt-100 g-pb-20">
        <div class="row justify-content-center">
            
            <div class="col-md-7 col-lg-7 order-lg-2 g-mb-80">
                <div class="g-brd-around g-brd-gray-light-v3 g-bg-white rounded g-px-30 g-py-50 mb-4">
                    <header class="text-center mb-4">           
                        <img src="{{ url('') }}/theme/home/assets/img/logo/logo.png" alt="Shop Caribbean Cellars">
                        <h1 class="h1 g-color-dark g-font-weight-400 g-mt-20">Are you of legal drinking age?</h1>
                        <span class="h5 g-color-dark g-font-weight-400">This website facilitates the sale of alcoholic beverages.</span>
                    </header>
                         
                    @include('home.partials.alert')

                    <!-- Form -->
                    <form class="g-py-10" method="post" action="{{ route('verify_submit') }}">
                        @csrf
                        <div class="mb-3 text-center">
                            <button class="btn btn-md u-btn-primary g-mr-10 g-mb-15 text-uppercase g-py-12 g-px-25" type="submit">Yes</button>
                            <a href="javascript:;" class="btn btn-md u-btn-darkgray g-mr-10 g-mb-15 text-uppercase g-py-12 g-px-25">No</a>
                        </div>
                        <div class="row text-center mb-5">
                            <div class="col align-self-center">
                                <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-0">
                                    <input name="remember" value="1" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </span>
                                    Remember me
                                </label>
                            </div>
                        </div>

                    </form>
                    <!-- End Form -->
                </div>

            </div>

        </div>
    </section>
    
@endsection

@push('js')
    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
            
            // initialization of HSMegaMenu component
              $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function() {
                    $(this).find('input[type="search"]').focus();
                }
            });
        });

    </script>

@endpush