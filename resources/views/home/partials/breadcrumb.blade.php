<!-- Breadcrumbs -->
<section class="g-bg-gray-light-v5 g-py-20">
    <div class="container">
        <div class="d-sm-flex text-center">
            <div class="align-self-center">
                <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md">{{ $title ?? 'Home' }}</h2>
            </div>
            <div class="align-self-center ml-auto">
                <ul class="u-list-inline">
                    <li class="list-inline-item g-mr-5">
                        <a class="u-link-v5 g-color-main g-color-primary--hover" href="{{ route('home') }}">Shop</a>
                        <i class="g-color-gray-light-v2 g-ml-5">/</i>
                    </li>
                    @if(isset($links))
                        @foreach($links as $link)

                            <li class="list-inline-item g-mr-5">
                                
                                <a class="u-link-v5 g-color-main g-color-primary--hover" href="{{ route($link['route'], $link['params']) }}">{{ $link['name'] }}</a>
                                <i class="g-color-gray-light-v2 g-ml-5">/</i>

                            </li>

                        @endforeach
                    @endif

                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End Breadcrumbs -->