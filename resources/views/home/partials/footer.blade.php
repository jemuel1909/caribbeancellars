<!-- Footer -->
<footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8">
    <!-- Copyright -->
    <div class="container g-pt-30 g-pb-10">
        <div class="row justify-content-between align-items-center">
            <div class="col-md-6 g-mb-20">
                <p class="g-font-size-13 mb-0">{{ date('Y') }} &copy; Caribbean Cellars. All Rights Reserved.</p>
            </div>
        </div>
    </div>
    <!-- End Copyright -->
</footer>
<!-- End Footer -->