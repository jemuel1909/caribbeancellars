<!-- Header -->
<header id="js-header" class="u-header u-header--static u-shadow-v19">
    
    <!-- Top Bar -->
    <div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-bg-black g-transition-0_3">
        <div class="container">
            <div class="row justify-content-between align-items-center g-mx-0--lg">
                
                <div class="col-sm-auto g-pos-rel g-py-14">
                  
                </div>

                <div class="col-sm-auto g-pr-15 g-pr-0--sm">
                    
                    <div class="u-basket d-inline-block g-z-index-3">
                        <div class="g-py-10 g-px-6">
                            <a href="#!" id="basket-bar-invoker" class="u-icon-v1 g-color-white-opacity-0_8 g-color-primary--hover g-font-size-17 g-text-underline--none--hover" aria-controls="basket-bar" aria-haspopup="true" aria-expanded="false" data-dropdown-event="hover" data-dropdown-target="#basket-bar" data-dropdown-type="css-animation" data-dropdown-duration="300" data-dropdown-hide-on-scroll="false" data-dropdown-animation-in="fadeIn" data-dropdown-animation-out="fadeOut">
                                <span class="u-badge-v1--sm g-color-white g-bg-primary g-font-size-11 g-line-height-1_4 g-rounded-50x g-pa-4" style="top: 7px !important; right: 3px !important;" id="cart-itm-cnt">0</span>
                                <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
                            </a>
                        </div>
                        
                        <!-- Basket Content-->
                        <div id="basket-bar" class="u-basket__bar u-dropdown--css-animation u-dropdown--hidden g-text-transform-none g-bg-white g-brd-around g-brd-gray-light-v4" aria-labelledby="basket-bar-invoker">
                            <div class="g-brd-bottom g-brd-gray-light-v4 g-pa-15 g-mb-10">
                                <span class="d-block h6 text-center text-uppercase mb-0">Shopping Cart</span>
                            </div>

                            <div class="clearfix g-px-15">
                                <div class="row align-items-center text-center g-brd-y g-brd-gray-light-v4 g-font-size-default">
                                    <div class="col g-brd-right g-brd-gray-light-v4">
                                        <strong class="d-block g-py-10 text-uppercase g-color-main g-font-weight-500 g-py-10">Total</strong>
                                    </div>
                                    <div class="col">
                                        <strong class="d-block g-py-10 g-color-main g-font-weight-500 g-py-10 total_amount">$0.00</strong>
                                    </div>
                                </div>
                            </div>

                            <div class="g-pa-20">
                                <div class="text-center g-mb-15">
                                    <a class="text-uppercase g-color-primary g-color-main--hover g-font-weight-400 g-font-size-13 g-text-underline--none--hover" href="{{ route('order.summary') }}">
                                        View Cart
                                        <i class="ml-2 icon-finance-100 u-line-icon-pro"></i>
                                    </a>
                                </div>
                                <a class="btn btn-block u-btn-black g-brd-primary--hover g-bg-primary--hover g-font-size-12 text-uppercase rounded g-py-10" href="{{ route('order.summary') }}">Proceed to Checkout</a>
                            </div>
                        </div>
                        <!-- End Basket Content -->
                    </div>
                    
                    <!-- Search -->
                    <div class="d-inline-block g-valign-middle">
                        <div class="col-sm-auto g-pos-rel g-py-14">
                            <!-- List -->
                            <ul class="list-inline g-overflow-hidden g-pt-1 g-mx-minus-4 mb-0">
                                
                                <!-- Account -->
                                <li class="list-inline-item">
                                    <a id="account-dropdown-invoker-2" class="g-color-white-opacity-0_6 g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="#!" aria-controls="account-dropdown-2" aria-haspopup="true" aria-expanded="false" data-dropdown-event="hover" data-dropdown-target="#account-dropdown-2" data-dropdown-type="css-animation" data-dropdown-duration="300" data-dropdown-hide-on-scroll="false" data-dropdown-animation-in="fadeIn" data-dropdown-animation-out="fadeOut">
                                        Account
                                    </a>
                                    <ul id="account-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-2 u-dropdown--css-animation u-dropdown--hidden" aria-labelledby="account-dropdown-invoker-2" style="animation-duration: 300ms; right: -100px;">
                                        
                                        @if (!isset(Auth::user()->id)) 
                                        
                                            <li>
                                                <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route('login') }}">
                                                    Login
                                                </a>
                                            </li>
                                            {{-- <li>
                                                <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route('register') }}">
                                                    Signup
                                                </a>
                                            </li> --}}
                                        
                                        @else
                                        
                                            <li>
                                                <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route('user.dashboard') }}">
                                                    Dashboard
                                                </a>
                                            </li>
                                            
                                            @if (Auth::user()->role_id == 1)
                                                <li>
                                                    <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route('admin.dashboard') }}">
                                                        Admin Dashboard
                                                    </a>
                                                </li>
                                            @endif

                                            <li>
                                                <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route('logout') }}">
                                                    Logout
                                                </a>
                                            </li>
                                        
                                        @endif
                                        
                                    </ul>
                                </li>
                                <!-- End Account -->
                            </ul>
                            <!-- End List -->
                        </div>
                    </div>
                    <!-- End Search -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Bar -->

    <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-2">
        <nav class="js-mega-menu navbar navbar-expand-lg">
            <div class="container">
                <!-- Responsive Toggle Button -->
                <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button"
                    aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse"
                    data-target="#navBar">
                    <span class="hamburger hamburger--slider g-pr-0">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </span>
                </button>
                <!-- End Responsive Toggle Button -->

                <!-- Logo -->
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="{{ url('') }}/theme/home/assets/img/logo/logo.png" alt="Shop Caribbean Cellars">
                </a>
                <!-- End Logo -->
                
                <!-- Navigation -->
                <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg" id="navBar">
                    <ul class="navbar-nav text-uppercase g-pos-rel g-font-weight-600 ml-auto">
                        <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                            <a href="{{ route('home') }}" class="nav-link g-py-7 g-px-0">Home</a>
                        </li>
                        {{-- <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                            <a href="{{ route('home') }}" class="nav-link g-py-7 g-px-0">About</a>
                        </li>
                        <li class="nav-item hs-has-sub-menu  g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut">
                            <a id="nav-link--features" class="nav-link g-py-7 g-px-0" href="#!" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--features">Portfolio</a>
                            <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-18 g-mt-8--lg--scrolling"
                                id="nav-submenu--features" aria-labelledby="nav-link--features">
                                
                                <li class="dropdown-item">
                                    <a class="nav-link" href="{{ route('category', ['slug' => 'beer']) }}">Beer</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link" href="{{ route('category', ['slug' => 'wine']) }}">Wine</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link" href="{{ route('category', ['slug' => 'spirits']) }}">Spirits</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link" href="{{ route('category', ['slug' => 'tobacco']) }}">Tobacco</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link" href="{{ route('category', ['slug' => 'non-alcholic']) }}">Non Alcholic</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link" href="{{ route('category', ['slug' => 'misc']) }}">Misc</a>
                                </li>                                
                            </ul>
                        </li>--}}
                        <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                            <a href="{{ route('faq') }}" class="nav-link g-py-7 g-px-0">FAQ</a>
                        </li>
                        <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                            <a href="{{ url('') }}/upload/pdf/pricelist.pdf" class="nav-link g-py-7 g-px-0" target="blank">Pricelist</a>
                        </li>
                        <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                            <a href="{{ route('about_us') }}" class="nav-link g-py-7 g-px-0">About Us</a>
                        </li>
                        <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                            <a href="{{ route('contact') }}" class="nav-link g-py-7 g-px-0">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <!-- End Navigation -->

            </div>
        </nav>
    </div>
</header>
<!-- End Header -->