<!-- Filters -->
<div class="col-md-3 order-md-1 g-brd-right--lg g-brd-gray-light-v4 g-pt-40">
    <div class="g-pr-15--lg g-pt-60">
        
        <!-- Cart Summary -->
        <div class="g-mb-30">
            <div class="g-brd-bottom g-brd-gray-light-v4 g-pb-10 g-mb-10">
                <span class="d-block h5 mb-0">Shopping Cart</span>
            </div>
            <div class="js-scrollbar g-height-200 mCustomScrollbar _mCS_2 mCS-autoHide" style="position: relative; overflow: visible;">
                <div id="mCSB_2" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" style="max-height: none;" tabindex="0">
                    <div id="mCSB_2_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
                        <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">
                            <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
                        </span>
                    </div>
                </div>

                <div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-minimal-dark mCSB_scrollTools_vertical" style="display: block;"><div class="mCSB_draggerContainer">
                    <div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 50px; display: block; height: 91px; max-height: 166px; top: 0px;">
                        <div class="mCSB_dragger_bar" style="line-height: 50px;"></div></div><div class="mCSB_draggerRail"></div>
                    </div>
                </div>
            </div>

            <div class="clearfix g-px-15">
                <div class="row align-items-center text-center g-brd-y g-brd-gray-light-v4 g-font-size-default">
                    <div class="col g-brd-right g-brd-gray-light-v4">
                        <strong class="d-block g-py-10 text-uppercase g-color-main g-font-weight-500 g-py-10">Total</strong>
                    </div>
                    <div class="col">
                        <strong class="d-block g-py-10 g-color-main g-font-weight-500 g-py-10" id="total_amount">$0.00</strong>
                    </div>
                </div>
            </div>

            <div class="g-pa-20">
                <a class="btn btn-block u-btn-black g-brd-primary--hover g-bg-primary--hover g-font-size-12 text-uppercase rounded g-py-10" href="{{ route('order.summary') }}">Proceed to Checkout</a>
            </div>
            <hr>
        </div>

        <!-- Categories -->
        <div class="g-mb-30">
            <h3 class="h5 mb-3">Categories</h3>
            <ul class="list-unstyled">
                
                @foreach($categories as $value)
                    <li class="my-3">
                        <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="{{ route('category', ['slug' => $value->slug]) }}">{{ $value->name }}
                            <span class="float-right g-font-size-12">{{ $value->count }}</span>
                        </a>
                    </li>

                    @if ($categoryId == $value->id || $categoryParentId == $value->id)
                        
                        @foreach($value->subCategories as $value)
                            <li class="ml-3 my-2">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="{{ route('category', ['slug' => $value->slug]) }}">{{ $value->name }}
                                    <span class="float-right g-font-size-12">{{ $value->count }}</span>
                                </a>
                            </li>
                        @endforeach
                        

                        @if ($categoryId == 21 || $categoryParentId == 21)

                            <li class="ml-3 my-2">
                                <span class="g-font-size-10 g-font-weight-600 text-uppercase">Region</span>    
                            </li>

                            @foreach($categoryRegions as $value)
                                <li class="ml-3 my-2">
                                    <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="{{ route('category', ['slug' => $value->slug]) }}">{{ $value->name }}
                                        <span class="float-right g-font-size-12">{{ $value->count }}</span>
                                    </a>
                                </li>
                            @endforeach
                        
                        @endif


                    @endif

                  
                @endforeach

            </ul>
        </div>
        <!-- End Categories -->
        
        <hr>

        {{-- Banner Left --}}
        @if ($setting->has_banner_left)
            <div class="g-mb-30 text-center">
                <a href="{{ $setting->banner_left_url ?? 'javascript:;' }}">
                    <img class="img-fluid" src="{{ url('') }}/upload/banner/left.jpg" alt="Image Description">
                </a>
            </div>
        @endif
        
    </div>
</div>
<!-- End Filters -->