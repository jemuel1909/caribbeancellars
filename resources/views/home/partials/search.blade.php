 <!-- Search Form -->
 <form class="input-group g-pos-rel" method="post" action="{{ route('search_submit') }}">
    @csrf
    <span class="g-pos-abs g-top-0 g-left-0 g-z-index-3 g-px-13 g-py-10">
        <i class="g-color-gray-dark-v4 g-font-size-12 icon-education-045 u-line-icon-pro"></i>
    </span>
    <input name="search_query" class="form-control u-form-control g-brd-around g-brd-gray-light-v3 g-brd-primary--focus g-font-size-13 g-rounded-left-5 g-pl-35 g-pa-0" type="search" placeholder="Search products" max="50">
    <div class="input-group-append g-brd-none g-py-0">
        <button class="btn u-btn-black g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Search Products</button>
    </div>
</form>
<!-- End Search Form -->