
@extends('home.layouts.home')

@section('title', 'Register')

@push('css')

@endpush

@section('content')

    @include('home.partials.breadcrumb', [
        'title' => 'Register',
        'links' => [
            ['name' => 'Register', 'route' => 'register', 'params' => []],
        ]
    ])

    <!-- Signup -->
    <section class="container g-pt-100 g-pb-20">
        <div class="row justify-content-center">
            <div class="col-lg-7 order-lg-1 g-mb-80">
                <div class="g-brd-around g-brd-gray-light-v3 g-bg-white rounded g-px-30 g-py-50 mb-4">
                    <header class="text-center mb-4">
                        <h1 class="h4 g-color-black g-font-weight-400">Create New Account</h1>
                    </header>

                    <!-- Form -->
                    <form class="g-py-15" method="post" action="{{ route('register_submit') }}">
                        @csrf
                        
                        @include('home.partials.alert')

                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="email" value="{{ old('email') }}" type="text" placeholder="Email address">
                        </div>
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="email_confirmation" value="{{ old('email_confirmation') }}" type="text" placeholder="Email address confirmation">
                        </div>
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="password" type="password" placeholder="Password">
                        </div>
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="password_confirmation" type="password" placeholder="Confirm password">
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col g-mb-20">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="first_name" value="{{ old('first_name') }}" type="text" placeholder="First name">
                            </div>

                            <div class="col g-mb-20">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="last_name" value="{{ old('last_name') }}" type="text" placeholder="Last name">
                            </div>
                        </div>
                        
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="address" value="{{ old('address') }}" type="text" placeholder="Address">
                        </div>
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="city" value="{{ old('city') }}" type="text" placeholder="City">
                        </div>
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="zip_code" value="{{ old('zip_code') }}" type="text" placeholder="Zip Code">
                        </div>
                        <div class="g-mb-20">
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" name="phone_no" value="{{ old('phone_no') }}" type="text" placeholder="Phone No.">
                        </div>
                        
                        <div class="mb-1">
                            <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-2">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" required>
                                <span class="d-block u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                I accept the <a href="#!">Terms and Conditions</a>
                            </label>
                        </div>
                        <button class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Signup</button>
                    </form>
                    <!-- End Form -->
                </div>

                <div class="text-center">
                    <p class="g-color-gray-dark-v5 mb-0">Already have an account?
                        <a class="g-font-weight-600" href="{{ route('login') }}">signin</a>
                    </p>
                </div>
            </div>

         
        </div>
    </section>
    <!-- End Signup -->

    @include('home.partials.call_to_action')

@endsection

@push('js')

@endpush