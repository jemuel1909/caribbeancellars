
@extends('home.layouts.home')

@section('title', 'Dashboard')

@section('css')

@endsection

@section('content')

    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h1 class="h3 mb-0">Your Orders</h1>
                </div>
                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text" href="{{ route('home') }}">Home</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text" href="{{ route('user.dashboard') }}">Dashboard</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Your Orders</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container g-pt-70 g-pb-30">
        <div class="row">
            <!-- Profile Settings -->
            <div class="col-lg-3 g-mb-50">
                <aside class="g-brd-around g-brd-gray-light-v4 rounded g-px-20 g-py-30">
                    <!-- Profile Picture -->
                    <div class="text-center g-pos-rel g-mb-30">
                        <div class="g-width-100 g-height-100 mx-auto mb-3">
                            <img class="img-fluid rounded-circle" src="{{ url('') }}/theme/home/assets/img-temp/100x100/img1.jpg" alt="Image Decor">
                        </div>
                        <span class="d-block g-font-weight-500">{{ Auth::user()->full_name }}</span>
                        <span class="u-icon-v3 u-icon-size--xs g-color-white--hover g-bg-primary--hover rounded-circle g-pos-abs g-top-0 g-right-15 g-cursor-pointer"
                            title="Change Profile Picture" data-toggle="tooltip" data-placement="top">
                            <i class="icon-finance-067 u-line-icon-pro"></i>
                        </span>
                    </div>
                    <!-- End Profile Picture -->

                    <hr class="g-brd-gray-light-v4 g-my-30">

                    <!-- Profile Settings List -->
                    <ul class="list-unstyled mb-0">
                        @php
                            $activeClass = 'active u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover g-color-primary--parent-active g-bg-gray-light-v5--active';
                            $inactiveClass = 'u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover';
                        @endphp
                        
                        <li class="g-py-3">
                            <a class="d-block align-middle @if (!in_array($focusStatus, array('open', 'cancelled'))) {{ $activeClass }} @else {{ $inactiveClass }} @endif rounded g-pa-3" href="{{ route('user.dashboard') }}">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-114 u-line-icon-pro"></i>
                                </span>
                                Your Orders
                            </a>
                        </li>
                        <li class="g-py-3">
                            <a class="d-block align-middle @if ($focusStatus == 'open') {{ $activeClass }} @else {{ $inactiveClass }} @endif rounded g-pa-3" href="{{ url('user/order/open') }}">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-115 u-line-icon-pro"></i>
                                </span>
                                Open Orders
                            </a>
                        </li>
                        <li class="g-py-3">
                            <a class="d-block align-middle @if ($focusStatus == 'cancelled') {{ $activeClass }} @else {{ $inactiveClass }} @endif @endphp rounded g-pa-3" href="{{ url('user/order/cancelled') }}">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-113 u-line-icon-pro"></i>
                                </span>
                                Cancelled Orders
                            </a>
                        </li>
                        
                        {{-- <li class="g-py-3">
                            <a class="d-block align-middle u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover rounded g-pa-3" href="#">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-real-estate-027 u-line-icon-pro"></i>
                                </span>
                                Addresses
                            </a>
                        </li> --}}

                        <li class="g-py-3">
                            <a class="d-block align-middle u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover rounded g-pa-3" href="#">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-135 u-line-icon-pro"></i>
                                </span>
                                Login &amp; Security
                            </a>
                        </li>
                    </ul>
                    <!-- End Profile Settings List -->
                </aside>
            </div>
            <!-- End Profile Settings -->

            <!-- Orders -->
            <div class="col-lg-9 g-mb-50">
                @php    
                    $activeClass = 'g-brd-primary g-color-main g-color-black g-font-weight-600';
                    $inactiveClass = 'g-brd-transparent g-color-main g-color-gray-dark-v4 g-color-primary--hover';
                @endphp
                
                <!-- Links -->
                <ul class="list-inline g-brd-bottom--sm g-brd-gray-light-v3 mb-5">
                    <li class="list-inline-item g-pb-10 g-pr-10 g-mb-20 g-mb-0--sm">
                        <a class="g-brd-bottom g-brd-2 @if (!in_array($focusStatus, array('open', 'cancelled'))) {{ $activeClass }} @else {{ $inactiveClass }} @endif g-text-underline--none--hover g-px-10 g-pb-13"
                            href="{{ url('user/order/all') }}">Orders</a>
                    </li>
                    <li class="list-inline-item g-pb-10 g-px-10 g-mb-20 g-mb-0--sm">
                        <a class="g-brd-bottom g-brd-2 @if ($focusStatus == 'open') {{ $activeClass }} @else {{ $inactiveClass }} @endif g-text-underline--none--hover g-px-10 g-pb-13"
                            href="{{ url('user/order/open') }}">Open Orders</a>
                    </li>
                    <li class="list-inline-item g-pb-10 g-pl-10 g-mb-20 g-mb-0--sm">
                        <a class="g-brd-bottom g-brd-2 @if ($focusStatus == 'cancelled') {{ $activeClass }} @else {{ $inactiveClass }} @endif g-text-underline--none--hover g-px-10 g-pb-13"
                            href="{{ url('user/order/cancelled') }}">Cancelled Orders</a>
                    </li>
                </ul>
                <!-- End Links -->

                <div class="mb-5">
                    @php
                        if ($focusStatus == 'open') {
                            $labelOrder = ' open';
                        } elseif ($focusStatus == 'cancelled') {
                            $labelOrder = ' cancelled';
                        } else {
                            $labelOrder = '';
                            $focusStatus = 'all';
                        }

                        $order_count = count($orders);

                        if ($order_count > 1) {
                            $displayCount = $order_count . $labelOrder . ' orders';
                        } else {
                            $displayCount = $order_count . $labelOrder . ' order';
                        }
                    @endphp
                    <h3 class="h6 d-inline-block">{{ $displayCount }}
                        <span class="g-color-gray-dark-v4 g-font-weight-400">placed in</span>
                    </h3>

                    <!-- Secondary Button -->
                    <div class="d-inline-block btn-group u-shadow-v19 ml-2">
                        <button type="button" class="btn u-btn-black dropdown-toggle h6 align-middle g-brd-none g-color-black g-bg-gray-light-v5 g-bg-gray-light-v4--hover g-font-weight-300 g-font-size-12 g-py-10 g-ma-0"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ $focusDate }}
                        </button>
                        <div class="dropdown-menu rounded-0 g-font-size-12">
                            <a class="dropdown-item g-color-black g-font-weight-300 @if ($focusDate == 'Last 30 Days') {{ 'g-bg-gray-light-v5' }} @endif" href="{{ url('user/order/' . $focusStatus . '/last-30-days') }}">last 30 days</a>
                            <a class="dropdown-item g-color-black g-font-weight-300 @if ($focusDate == 'Past 6 Months') {{ 'g-bg-gray-light-v5' }} @endif" href="{{ url('user/order/' . $focusStatus . '/past-6-months') }}">past 6 months</a>
                        </div>
                    </div>
                    <!-- End Secondary Button -->
                </div>

                <!-- Orders Block -->
                <div class="table-responsive g-pt-20 g-pb-50">
                    <table class="table table-bordered u-table--v2">
                        <thead class="text-uppercase g-letter-spacing-1">
                            <tr>
                                <th class="g-font-weight-300 g-color-black">Order #</th>
                                <th class="g-font-weight-300 g-color-black">Order Placed</th>
                                <th class="g-font-weight-300 g-color-black">Tracking ID</th>
                                <th class="g-font-weight-300 g-color-black">Total Amount</th>
                                <th class="g-font-weight-300 g-color-black"></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach($orders as $order)
                                <tr>
                                    <td class="align-middle">
                                        {{ $order->id }}
                                    </td>
                                    <td class="align-middle">
                                        {{ $order->ordered_at }}
                                    </td>
                                    <td class="align-middle">
                                        {{ $order->tracking_id }}
                                    </td>
                                    <td class="align-middle">
                                        {{ number_format($order->total_amount, 2) }}
                                    </td>

                                    <td class="align-middle text-center">
                                        <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-10 g-px-20" href="{{ route('user.order.show', ['order_id' => $order->id])}}">
                                            View Details <i class="ml-2 icon-finance-002 u-line-icon-pro"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- End Orders Block -->

                <!-- Pagination -->
                <nav class="g-mb-100" aria-label="Page Navigation">
                    {{ $orders->links('home.partials.paginator') }}
                </nav>
                <!-- End Pagination -->
            </div>
            <!-- Orders -->
        </div>
    </div>

    <!-- Call to Action -->
    <div class="g-bg-primary">
        <div class="container g-py-20">
            <div class="row justify-content-center">
                <div class="col-md-4 mx-auto g-py-20">
                    <!-- Media -->
                    <div class="media g-px-50--lg">
                        <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
                        <div class="media-body">
                            <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Shipping</span>
                            <span class="d-block g-color-white-opacity-0_8">In 2-3 Days</span>
                        </div>
                    </div>
                    <!-- End Media -->
                </div>

                <div class="col-md-4 mx-auto g-brd-x--md g-brd-white-opacity-0_3 g-py-20">
                    <!-- Media -->
                    <div class="media g-px-50--lg">
                        <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
                        <div class="media-body">
                            <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Returns</span>
                            <span class="d-block g-color-white-opacity-0_8">No Questions Asked</span>
                        </div>
                    </div>
                    <!-- End Media -->
                </div>

                <div class="col-md-4 mx-auto g-py-20">
                    <!-- Media -->
                    <div class="media g-px-50--lg">
                        <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
                        <div class="media-body text-left">
                            <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free 24</span>
                            <span class="d-block g-color-white-opacity-0_8">Days Storage</span>
                        </div>
                    </div>
                    <!-- End Media -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Call to Action -->

@endsection

@push('js')

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widget.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.jquery.js"></script>
    
    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.scrollbar.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.rating.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.slider.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.go-to.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.count-qty.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.select.js"></script>
    
    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of HSScrollBar component
            $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of rating
            $.HSCore.helpers.HSRating.init();
            
            // initialization of custom select
            $.HSCore.components.HSSelect.init('.js-custom-select');

            // initialization of range slider
            $.HSCore.components.HSSlider.init('#rangeSlider1');
            
            // initialization of forms
            $.HSCore.components.HSCountQty.init('.js-quantity');

        });
    </script>

@endpush