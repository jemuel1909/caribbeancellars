
@extends('home.layouts.home')

@section('title', 'Dashboard')

@section('css')

@endsection

@section('content')

    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex text-center">
                <div class="align-self-center">
                    <h1 class="h3 mb-0">Your Orders</h1>
                </div>
                <div class="align-self-center ml-auto">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text" href="{{ route('home') }}">Home</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text" href="{{ route('user.dashboard') }}">Dashboard</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Your Orders</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container g-pt-70 g-pb-30">
        <div class="row">
            <!-- Profile Settings -->
            <div class="col-lg-3 g-mb-50">
                <aside class="g-brd-around g-brd-gray-light-v4 rounded g-px-20 g-py-30">
                    <!-- Profile Picture -->
                    <div class="text-center g-pos-rel g-mb-30">
                        <div class="g-width-100 g-height-100 mx-auto mb-3">
                            <img class="img-fluid rounded-circle" src="{{ url('') }}/theme/home/assets/img-temp/100x100/img1.jpg" alt="Image Decor">
                        </div>
                        <span class="d-block g-font-weight-500">{{ Auth::user()->full_name }}</span>
                        <span class="u-icon-v3 u-icon-size--xs g-color-white--hover g-bg-primary--hover rounded-circle g-pos-abs g-top-0 g-right-15 g-cursor-pointer"
                            title="Change Profile Picture" data-toggle="tooltip" data-placement="top">
                            <i class="icon-finance-067 u-line-icon-pro"></i>
                        </span>
                    </div>
                    <!-- End Profile Picture -->

                    <hr class="g-brd-gray-light-v4 g-my-30">

                    <!-- Profile Settings List -->
                    <ul class="list-unstyled mb-0">
                        @php
                            $activeClass = 'active u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover g-color-primary--parent-active g-bg-gray-light-v5--active';
                            $inactiveClass = 'u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover';
                        @endphp
                        
                        <li class="g-py-3">
                            <a class="d-block align-middle @if (!in_array($focusStatus, array('open', 'cancelled'))) {{ $activeClass }} @else {{ $inactiveClass }} @endif rounded g-pa-3" href="{{ route('user.dashboard') }}">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-114 u-line-icon-pro"></i>
                                </span>
                                Your Orders
                            </a>
                        </li>
                        <li class="g-py-3">
                            <a class="d-block align-middle @if ($focusStatus == 'open') {{ $activeClass }} @else {{ $inactiveClass }} @endif rounded g-pa-3" href="{{ url('user/order/open') }}">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-115 u-line-icon-pro"></i>
                                </span>
                                Open Orders
                            </a>
                        </li>
                        <li class="g-py-3">
                            <a class="d-block align-middle @if ($focusStatus == 'cancelled') {{ $activeClass }} @else {{ $inactiveClass }} @endif @endphp rounded g-pa-3" href="{{ url('user/order/cancelled') }}">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-113 u-line-icon-pro"></i>
                                </span>
                                Cancelled Orders
                            </a>
                        </li>
                        
                        {{-- <li class="g-py-3">
                            <a class="d-block align-middle u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover rounded g-pa-3" href="#">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-real-estate-027 u-line-icon-pro"></i>
                                </span>
                                Addresses
                            </a>
                        </li> --}}

                        <li class="g-py-3">
                            <a class="d-block align-middle u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover rounded g-pa-3" href="#">
                                <span class="u-icon-v1 g-color-gray-dark-v5 mr-2">
                                    <i class="icon-finance-135 u-line-icon-pro"></i>
                                </span>
                                Login &amp; Security
                            </a>
                        </li>
                    </ul>
                    <!-- End Profile Settings List -->
                </aside>
            </div>
            <!-- End Profile Settings -->

            <!-- Orders -->
            <div class="col-lg-9 g-mb-50">
                
                <!-- Order Block -->
                <div class="g-brd-around g-brd-gray-light-v4 rounded g-mb-30">
                    <header class="g-bg-gray-light-v5 g-pa-20">
                        <div class="row">
                            <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Order Placed</h4>
                                <span class="g-color-black g-font-weight-300 g-font-size-13">{{ $order->ordered_at }}</span>
                            </div>

                            <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Total</h4>
                                <span class="g-color-black g-font-weight-300 g-font-size-13">${{ number_format($order->total_amount, 2) }}</span>
                            </div>

                            <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Ship to</h4>
                                <span class="g-color-black g-font-weight-300 g-font-size-13">{{ $order->user->full_name }}</span>
                            </div>

                            <div class="col-sm-3 col-md-4 ml-auto text-sm-right">
                                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Order # {{ $order->id }}</h4>
                                <a class="g-font-weight-300 g-font-size-13" href="#!">Invoice</a>
                            </div>
                        </div>
                    </header>

                    <!-- Order Content -->
                    <div class="g-pa-20">
                        <div class="row">
                            <div class="col-md-9">
                                {{-- <div class="mb-4">
                                    <h3 class="h5 mb-1">Delivered Yesterday</h3>
                                    <p class="g-color-gray-dark-v4 g-font-size-13">Your package was delivered per the instructions.</p>
                                </div> --}}

                                @foreach($order->orderItems as $orderItem)

                                    <div class="row">
                                        {{--
                                        <div class="col-4 col-sm-3 g-mb-30">
                                            <!--
                                            <img class="img-fluid" src="{{ url('') }}/theme/home/assets/img-temp/150x150/img9.jpg" alt="Image Description">
                                            -->
                                        </div>
                                        --}}
                                        <div class="col-sm-12 g-mb-30">
                                            <h4 class="h6 g-font-weight-400">
                                                <a href="#!">{{ $orderItem->name }}</a>
                                            </h4>
                                            <span class="d-block g-color-gray-dark-v4 g-font-size-13">Type: {{ $orderItem->quantityType->name }}</span>
                                            <span class="d-block g-color-gray-dark-v4 g-font-size-13">Total Quantity: {{ $orderItem->total_quantity }}</span>
                                            <span class="d-block g-color-gray-dark-v4 g-font-size-13">Amount: {{ $orderItem->amount }}</span>
                                            <span class="d-block g-color-lightred mb-2">${{ number_format($orderItem->total_amount, 2) }}</span>
                                        </div>
                                    </div>
                                @endforeach

                                @if (trim($order->notes) != '')
                                    <h4 class="h6 mb-1">Your Note:</h4>
                                    <p class="g-color-gray-dark-v4 g-font-size-13">{{ $order->notes }}</p>
                                @endif
                            </div>

                            <div class="col-md-3">
                                <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="#!">Track Package</a>
                                {{-- <a class="btn btn-block g-brd-around g-brd-gray-light-v3 g-color-gray-dark-v3 g-bg-gray-light-v5 g-bg-gray-light-v4--hover g-font-size-12 text-uppercase g-py-12 g-px-25" href="#!">Return Item</a>
                                <a class="btn btn-block g-brd-around g-brd-gray-light-v3 g-color-gray-dark-v3 g-bg-gray-light-v5 g-bg-gray-light-v4--hover g-font-size-12 text-uppercase g-py-12 g-px-25" href="#!">Leave Package Feedback</a>
                                <a class="btn btn-block g-brd-around g-brd-gray-light-v3 g-color-gray-dark-v3 g-bg-gray-light-v5 g-bg-gray-light-v4--hover g-font-size-12 text-uppercase g-py-12 g-px-25" href="#!">Write a Product Review</a>
                                <a class="btn btn-block g-brd-around g-brd-gray-light-v3 g-color-gray-dark-v3 g-bg-gray-light-v5 g-bg-gray-light-v4--hover g-font-size-12 text-uppercase g-py-12 g-px-25" href="#!">Archive Order</a> --}}
                            </div>

                        </div>
                    </div>
                    <!-- End Order Content -->
                </div>
                <!-- End Order Block -->

            </div>
            <!-- Orders -->
        </div>
    </div>

    <!-- Call to Action -->
    <div class="g-bg-primary">
        <div class="container g-py-20">
            <div class="row justify-content-center">
                <div class="col-md-4 mx-auto g-py-20">
                    <!-- Media -->
                    <div class="media g-px-50--lg">
                        <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
                        <div class="media-body">
                            <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Shipping</span>
                            <span class="d-block g-color-white-opacity-0_8">In 2-3 Days</span>
                        </div>
                    </div>
                    <!-- End Media -->
                </div>

                <div class="col-md-4 mx-auto g-brd-x--md g-brd-white-opacity-0_3 g-py-20">
                    <!-- Media -->
                    <div class="media g-px-50--lg">
                        <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
                        <div class="media-body">
                            <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Returns</span>
                            <span class="d-block g-color-white-opacity-0_8">No Questions Asked</span>
                        </div>
                    </div>
                    <!-- End Media -->
                </div>

                <div class="col-md-4 mx-auto g-py-20">
                    <!-- Media -->
                    <div class="media g-px-50--lg">
                        <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
                        <div class="media-body text-left">
                            <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free 24</span>
                            <span class="d-block g-color-white-opacity-0_8">Days Storage</span>
                        </div>
                    </div>
                    <!-- End Media -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Call to Action -->

@endsection

@push('js')

    <!-- JS Implementing Plugins -->
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widget.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ url('') }}/theme/home/assets/vendor/chosen/chosen.jquery.js"></script>
    
    <!-- JS Unify -->
    <script src="{{ url('') }}/theme/home/assets/js/hs.core.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.header.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.dropdown.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.scrollbar.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/helpers/hs.rating.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.slider.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.go-to.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.count-qty.js"></script>
    <script src="{{ url('') }}/theme/home/assets/js/components/hs.select.js"></script>
    
    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of HSDropdown component
            $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of HSScrollBar component
            $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of rating
            $.HSCore.helpers.HSRating.init();
            
            // initialization of custom select
            $.HSCore.components.HSSelect.init('.js-custom-select');

            // initialization of range slider
            $.HSCore.components.HSSlider.init('#rangeSlider1');
            
            // initialization of forms
            $.HSCore.components.HSCountQty.init('.js-quantity');

        });
    </script>

@endpush