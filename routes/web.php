<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Test
Route::get('test', function(){


    // Get order.
    $order = \App\Models\Order::with('orderItems')->where('id', 15111)->firstOrFail();
    
    // return $order;
    // Mail customer.
    // return  new \App\Mail\Admin\OrderCreated($order);
    return  new \App\Mail\User\OrderCreated($order);

    // return Mail::to('jemuel1909@yahoo.com')->queue(new \App\Mail\User\OrderCreated($order));

});

/*
 * Home 
 * All information pages.
 * Controllers Within The "App\Http\Controllers\Home" Namespace
 */
Route::group(['namespace' => 'Home'], function() {
    
    // Home page.
    Route::get('/', 'IndexController@index')->name('home');
    
    // Category.
    Route::get('category/{slug}', 'CategoryController@index')->name('category');

    // Search Result.
    Route::post('search-submit', 'IndexController@searchSubmit')->name('search_submit');
    Route::get('search/q/{search_query}', 'IndexController@search')->name('search');
    
    // Authentication.
    Route::match(['get', 'post'], 'login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    
    // Forgot password. 
    Route::match(['get', 'post'], 'forgot-password', 'LoginController@forgotPassword')->name('forgot_password');
    Route::get('reset-password/{email_token}', 'LoginController@resetPassword')->name('reset_password');
    Route::post('reset-password-submit', 'LoginController@resetPasswordSubmit')->name('reset_password_submit');
    
    // Registration.
    Route::get('register', 'RegisterController@register')->name('register');
    Route::post('register-submit', 'RegisterController@registerSubmit')->name('register_submit');
    Route::get('activate/{email_token}', 'RegisterController@activate')->name('activate');
    
    // Order.
    Route::post('order/put', 'OrderController@put')->name('order.put');
    Route::post('order/remove', 'OrderController@remove')->name('order.remove');
    Route::post('order/update', 'OrderController@update')->name('order.update');
    Route::get('order/tally', 'OrderController@tally')->name('order.tally');
    Route::get('order/summary', 'OrderController@summary')->name('order.summary');
    Route::get('order/shipping', 'OrderController@shipping')->name('order.shipping');
    Route::match(['get', 'post'], 'order/payment', 'OrderController@payment')->name('order.payment');
    Route::post('order/checkout', 'OrderController@checkout')->name('order.checkout');
    Route::get('order/completed', 'OrderController@completed')->name('order.completed');

    // Pages.
    Route::get('contact', 'PageController@contact')->name('contact');
    Route::post('contact-submit', 'PageController@contactSubmit')->name('contact_submit');
    Route::get('verify', 'PageController@verify')->name('verify');
    Route::post('verify-submit', 'PageController@verifySubmit')->name('verify_submit');
    Route::get('about-us', 'PageController@aboutUs')->name('about_us');
    // Route::get('overview', 'PageController@overview')->name('overview');
    Route::get('faq', 'PageController@faq')->name('faq');

});

/*
 * Admin
 * Only allowed admin access.
 * Controllers Within The "App\Http\Controllers\Admin" Namespace
 */ 
Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin', 'namespace' => 'Admin'], function() {
    
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    
    // User.
    Route::get('user', 'UserController@index')->name('admin.user');
    Route::get('user/add', 'UserController@add')->name('admin.user.add');
    Route::post('user/add-submit', 'UserController@addSubmit');
    Route::get('user/edit/{user_id}', 'UserController@edit')->name('admin.user.edit');
    Route::post('user/edit-submit/{user_id}', 'UserController@editSubmit')->name('admin.user.edit_submit');
    Route::post('user/edit-password-submit/{user_id}', 'UserController@editPasswordSubmit');
    Route::get('user/delete/{user_id}', 'UserController@delete');

    // Setting.
    Route::get('settings', 'SettingController@edit')->name('admin.settings');
    Route::post('settings-submit', 'SettingController@editSubmit')->name('admin.settings.edit_submit');

    // Products.
    Route::get('products', 'ProductController@index')->name('admin.products');
    Route::get('products/add', 'ProductController@add')->name('admin.products.add');
    Route::post('products/add-submit', 'ProductController@addSubmit')->name('admin.products.add_submit');
    Route::get('products/edit/{product_id}', 'ProductController@edit')->name('admin.products.edit');
    Route::post('products/edit-submit/{product_id}', 'ProductController@editSubmit')->name('admin.products.edit_submit');
    Route::delete('products/delete', 'ProductController@delete')->name('admin.products.delete');
    Route::get('products/search-index', 'ProductController@searchIndex')->name('admin.products.search_index');
    Route::get('products/import', 'ProductController@import')->name('admin.products.import');
    Route::post('products/import-submit', 'ProductController@importSubmit')->name('admin.products.import_submit');

    // Quantity Type.
    Route::get('quantity-type', 'QuantityTypeController@index')->name('admin.quantity_type');
    Route::get('quantity-type/add', 'QuantityTypeController@add')->name('admin.quantity_type.add');
    Route::post('quantity-type/add-submit', 'QuantityTypeController@addSubmit')->name('admin.quantity_type.add_submit');
    Route::get('quantity-type/edit/{quantity_type_id}', 'QuantityTypeController@edit')->name('admin.quantity_type.edit');
    Route::post('quantity-type/edit-submit/{quantity_type_id}', 'QuantityTypeController@editSubmit')->name('admin.quantity_type.edit_submit');
    Route::get('quantity-type/delete/{quantity_type_id}', 'QuantityTypeController@delete')->name('admin.quantity_type.delete');

    // Categories.
    Route::get('categories', 'CategoryController@index')->name('admin.categories');
    Route::get('categories/add', 'CategoryController@add')->name('admin.categories.add');
    Route::post('categories/add-submit', 'CategoryController@addSubmit')->name('admin.categories.add_submit');
    Route::get('categories/edit/{product_id}', 'CategoryController@edit')->name('admin.categories.edit');
    Route::post('categories/edit-submit/{product_id}', 'CategoryController@editSubmit')->name('admin.categories.edit_submit');
    Route::delete('categories/delete', 'CategoryController@delete')->name('admin.categories.delete');

    // Orders.
    Route::get('orders', 'OrderController@index')->name('admin.orders');
    Route::get('orders/view/{order_id}', 'OrderController@view')->name('admin.orders.view');
    Route::get('orders/add', 'OrderController@add')->name('admin.orders.add');
    Route::post('orders/add-submit', 'OrderController@addSubmit')->name('admin.orders.add_submit');
    Route::get('orders/edit/{order_id}', 'OrderController@edit')->name('admin.orders.edit');
    Route::post('orders/edit-submit', 'OrderController@editSubmit')->name('admin.orders.edit_submit');
    Route::get('orders/delete', 'OrderController@delete')->name('admin.orders.delete');

});

/*
 * User
 * Only allowed user/admin access.
 * Controllers Within The "App\Http\Controllers\User" Namespace
 */ 
Route::group(['prefix' => 'user', 'middleware' => 'auth.user', 'namespace' => 'User'], function() {

    Route::get('/', 'DashboardController@index')->name('user.dashboard');
    Route::get('order/view/{order_id}', 'OrderController@show')->name('user.order.show');
    Route::get('order/{status_condition?}/{date_range?}', 'OrderController@index')->name('user.order');
});